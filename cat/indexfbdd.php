<?xml version="1.0" encoding="utf-8"?>
            <rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
                <channel>
                    <atom:link href="https://hostie-whmcs.themewant.com/index.php?rp=/announcements/rss" rel="self" type="application/rss+xml" />
                    <title><![CDATA[ReacThemes]]></title>
                    <description><![CDATA[ReacThemes Announcements Feed]]></description>
                    <link>https://hostie-whmcs.themewant.com/index.php?rp=/announcements</link>
                    
<item>
    <title><![CDATA[Optimized and Super-Fast Hosting]]></title>
    <link>https://hostie-whmcs.themewant.com/index.php?rp=/announcements/2</link>
    <guid>https://hostie-whmcs.themewant.com/index.php?rp=/announcements/2</guid>
    <pubDate>Thu, 11 Jan 2024 11:59:00 +0000</pubDate>
    <description><![CDATA[<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: 'Open Sans', Arial, sans-serif;">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: 'Open Sans', Arial, sans-serif;">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
<p> </p>]]></description>
</item>

<item>
    <title><![CDATA[Thank you for choosing WHMCS!]]></title>
    <link>https://hostie-whmcs.themewant.com/index.php?rp=/announcements/1</link>
    <guid>https://hostie-whmcs.themewant.com/index.php?rp=/announcements/1</guid>
    <pubDate>Thu, 28 Dec 2023 09:49:36 +0000</pubDate>
    <description><![CDATA[<p>Welcome to <a title="WHMCS" href="http://whmcs.com" target="_blank">WHMCS</a>!
 You have made a great choice and we want to help you get up and running as
 quickly as possible.</p>
<p>This is a sample announcement. Announcements are a great way to keep your
 customers informed about news and special offers. You can edit or delete this
 announcement by logging into the admin area and navigating to <em>Support &gt;
 Announcements</em>.</p>
<p>If at any point you get stuck, our support team is available 24x7 to
 assist you. Simply visit <a title="www.whmcs.com/support"
 href="https://www.whmcs.com/support" target="_blank">www.whmcs.com/support</a>
 to request assistance.</p>]]></description>
</item>
                </channel>
            </rss>