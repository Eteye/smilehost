<?php
require_once 'config.php';

require 'PHPMailer-master/PHPMailer-master/src/PHPMailer.php';
require 'PHPMailer-master/PHPMailer-master/src/SMTP.php';
require 'PHPMailer-master/PHPMailer-master/src/Exception.php';
class isbrkonlineAPI
{
    private $sqlConn;
    private $users;
    private $myAsset;
    private $t_Table;
    private $store;
    private $d_board;
    private $message;
    private $ref_bonus;
    private $deposit;
    private $new_user;

    public function __construct($usersTable, $usersAsset, $tTable, $storeTable, $dashboard, $chat, $bonus, $pending, $new_user_Table, $conn)
    {
        $this->sqlConn = $conn;
        $this->users = $usersTable;
        $this->myAsset = $usersAsset;
        $this->t_Table = $tTable;
        $this->store = $storeTable;
        $this->d_board = $dashboard;
        $this->message = $chat;
        $this->ref_bonus = $bonus;
        $this->deposit = $pending;
        $this->new_user = $new_user_Table;
    }
    public function randomToken($length = 12)
    {
        $characters = '1234567890';
        $token = '';
        for ($i = 0; $i < $length; $i++) {
            $token .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $token;
    }
    public function ref_ID($length = 6)
    {
        $characters = '1234567890ABCDES';
        $token = '';
        for ($i = 0; $i < $length; $i++) {
            $token .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $token;
    }
    public function signup($user_id)
    {
        $dashboardID = 1;
        $stmt2 = $this->sqlConn->prepare("SELECT * FROM $this->d_board WHERE id = ?");
        $stmt2->bind_param("i", $dashboardID);
        $stmt2->execute();
        $result = $stmt2->get_result();
        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            $users = $row['total_users'];
            $total_users = $users + 1;

            $update = "UPDATE $this->d_board SET `total_users` = ? WHERE `id` = ?";
            $query = $this->sqlConn->prepare($update);
            $query->bind_param("ii", $total_users, $dashboardID);
            if ($query->execute()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    public function deposit($user_id, $amount, $transaction_id)
    {
        $role = 'user';
        $description = 'Deposit of ';
        $status = 1;
        $date = $this->time();
        $stmt1 = $this->sqlConn->prepare("INSERT INTO $this->t_Table (user_id, amount, role, description, status, ref_no, date) VALUES (?, ?, ?, ?, ?, ?, ?)");
        $stmt1->bind_param("sssssss", $user_id, $amount, $role, $description, $status, $transaction_id, $date);
        if ($stmt1->execute()) {
            $stmt2 = $this->sqlConn->prepare("SELECT * FROM $this->users WHERE id = ?");
            $stmt2->bind_param("i", $user_id);
            $stmt2->execute();
            $result = $stmt2->get_result();
            if ($result->num_rows > 0) {
                $row = $result->fetch_assoc();
                $balance = $row['balance'];
                $new_balance = $amount + $balance;
                $stmt3 = "UPDATE $this->users SET `balance` = ? WHERE `id` = ?";
                $query = $this->sqlConn->prepare($stmt3);
                $query->bind_param("ss", $new_balance, $user_id);
                if ($query->execute()) {
                    $dashboardID = 1;
                    $stmt4 = $this->sqlConn->prepare("SELECT * FROM $this->d_board WHERE id = ?");
                    $stmt4->bind_param("i", $dashboardID);
                    $stmt4->execute();
                    $result4 = $stmt4->get_result();
                    if ($result4->num_rows > 0) {
                        $row4 = $result4->fetch_assoc();
                        $income = $row4['total_income'];
                        $total_balance = $row4['total_balance'];
                        $percentage = 1.4; //flutterwave charge
                        $calculatedValue = $amount * ($percentage / 100);
                        $valid_amount = $amount - $calculatedValue;
                        $new_balance2 = $total_balance + $valid_amount;
                        $new_income = $income + $valid_amount;
                        $update4 = "UPDATE $this->d_board SET `total_income` = ?, `total_balance` = ?  WHERE `id` = ?";
                        $query4 = $this->sqlConn->prepare($update4);
                        $query4->bind_param("dds", $new_income, $new_balance2, $dashboardID);
                        if ($query4->execute()) {
                            return true;
                        } else {
                            return [
                                'message' => 'Failed to update dashboard',
                            ];
                        }
                    } else {
                        return [
                            'message' => 'Failed to fetch dashboard',
                        ];
                    }
                } else {
                    return false;
                }
            } else {
                return [
                    'message' => 'User not found',
                ];
            }
        } else {
            return [
                'message' => 'Failed to deposit',
            ];
        }
    }
    public function pendingDeposit($user_id, $amount, $transaction_id)
    {
        $role = 'user';
        $description = 'Deposit of ';
        $status = 0;
        $date = $this->time();
        $stmt1 = $this->sqlConn->prepare("INSERT INTO $this->t_Table (user_id, amount, role, description, status, ref_no, date) VALUES (?, ?, ?, ?, ?, ?, ?)");
        $stmt1->bind_param("sssssss", $user_id, $amount, $role, $description, $status, $transaction_id, $date);
        if ($stmt1->execute()) {
            $stmt2 = $this->sqlConn->prepare("INSERT INTO $this->deposit (user_id, amount, ref_no, date) VALUES (?, ?, ?, ?)");
            $stmt2->bind_param("idss", $user_id, $amount, $transaction_id, $date);
            if ($stmt2->execute()) {
                return true;
            } else {
                return [
                    'message' => 'Failed to deposit',
                ];
            }
        } else {
            return [
                'message' => 'Failed to deposit',
            ];
        }
    }
    public function priceDrop()
    {
        $iDs = $this->fetchAllAssetsBought();
        $successCount = 0;
        $failureCount = 0;

        foreach ($iDs as $machine_id) {
            $stmt1 = $this->sqlConn->prepare("SELECT * FROM $this->myAsset WHERE id = ?");
            $stmt1->bind_param("i", $machine_id);
            $stmt1->execute();
            $result1 = $stmt1->get_result();

            if ($result1 && $result1->num_rows > 0) {
                $row1 = $result1->fetch_assoc();
                $ini_price = $row1['ini_price'];
                $percentage = $ini_price / 120; //drop the price
                $price = $row1['price'];
                $user_id = $row1['user_id'];
                if ($percentage <= $price) {
                    $calculatedValue = $price - $percentage;
                    $calculatedValue = round($price - $percentage, 2);
                    $stmt3 = $this->sqlConn->prepare("UPDATE $this->myAsset SET price = ? WHERE id = ?");
                    $stmt3->bind_param("di", $calculatedValue, $machine_id);
                    if ($stmt3->execute()) {
                        $successCount++;
                    } else {
                        $failureCount++;
                    }
                } else {
                    //remove the machine
                    $stmt4 = $this->sqlConn->prepare("DELETE FROM $this->myAsset WHERE id = ?");
                    $stmt4->bind_param("i", $machine_id);
                    if ($stmt4->execute()) {
                        $stmt5 = $this->sqlConn->prepare("SELECT * FROM $this->users WHERE id = ?");
                        $stmt5->bind_param("i", $user_id);
                        if ($stmt5->execute()) {
                            $result5 = $stmt5->get_result();
                            $row5 = $result5->fetch_assoc();
                            $email = $row5['email'];
                            $active_machine = $row5['active_machine'];
                            $user_id = $row5['id'];
                            $new_active = $active_machine - 1;
                            $stmt3 = $this->sqlConn->prepare("UPDATE $this->users SET active_machine = ? WHERE id = ?");
                            $stmt3->bind_param("ii", $new_active, $user_id);
                            if ($stmt3->execute()) {
                                $successCount++;
                                // $mail = new PHPMailer(true);
                                // $mail->isSMTP();
                                // $mail->Host = 'smtp.gmail.com';
                                // $mail->SMTPAuth = true;
                                // $mail->Username = 'essen653@gmail.com';
                                // $mail->Password = 'yrumbywwifhqpwoa';
                                // $mail->Port = 465;
                                // $mail->SMTPSecure = 'ssl';

                                // $mail->setFrom('essen653@gmail.com', 'Golden Laundry Service');
                                // $mail->addAddress($email);

                                // $mail->Subject = 'Password Reset from Laundry API';
                                // $mail->Body = 'Your password reset token is ';

                                // if ($mail->send()) {
                                //     $successCount++;
                                // } else {
                                //     $failureCount++;
                                // }
                            } else {
                                $failureCount++;
                            }
                        } else {
                            $failureCount++;
                        }

                    } else {
                        $failureCount++;
                    }
                }

            }
        }

        return [
            'success' => $successCount,
            'failure' => $failureCount,
        ];
    }
    public function fetchAllAssetsBought()
    {
        $stmt = $this->sqlConn->prepare("SELECT id FROM $this->myAsset");
        $stmt->execute();
        $result = $stmt->get_result();

        $iDs = [];
        while ($row = $result->fetch_assoc()) {
            $iDs[] = $row['id'];
        }

        return $iDs;
    }
    public function fetchAllUserIDs()
    {
        $stmt = $this->sqlConn->prepare("SELECT DISTINCT user_id FROM $this->myAsset");
        $stmt->execute();
        $result = $stmt->get_result();

        $userIDs = [];
        while ($row = $result->fetch_assoc()) {
            $userIDs[] = $row['user_id'];
        }

        return $userIDs;
    }
    public function processUserIncome()
    {
        $userIDs = $this->fetchAllUserIDs();
        $successCount = 0;
        $failureCount = 0;

        foreach ($userIDs as $userID) {
            $stmt1 = $this->sqlConn->prepare("SELECT SUM(profit) as total_profit FROM $this->myAsset WHERE user_id = ?");
            $stmt1->bind_param("i", $userID);
            $stmt1->execute();
            $result1 = $stmt1->get_result();

            if ($result1 && $result1->num_rows > 0) {
                $row1 = $result1->fetch_assoc();
                $total_profit = $row1['total_profit'];

                $stmt22 = $this->sqlConn->prepare("SELECT * FROM $this->myAsset WHERE user_id = ?");
                $stmt22->bind_param("i", $userID);
                $stmt22->execute();
                $result22 = $stmt22->get_result();
                $row22 = $result22->fetch_assoc();
                $machine_name = $row22['machine_name'];

                $stmt2 = $this->sqlConn->prepare("SELECT * FROM $this->users WHERE id = ?");
                $stmt2->bind_param("i", $userID);
                $stmt2->execute();
                $result2 = $stmt2->get_result();

                if ($result2 && $result2->num_rows > 0) {
                    $row2 = $result2->fetch_assoc();
                    $user_balance = $row2['balance'];

                    $new_balance = $user_balance + $total_profit;

                    $stmt3 = $this->sqlConn->prepare("UPDATE $this->users SET balance = ? WHERE id = ?");
                    $stmt3->bind_param("di", $new_balance, $userID);
                    if ($stmt3->execute()) {
                        $role = 'user';
                        $description = $machine_name . ' +';
                        $status = 1;
                        $date = $this->time();
                        $transaction_id = $this->randomToken();
                        $stmt4 = $this->sqlConn->prepare("INSERT INTO $this->t_Table (user_id, amount, role, description, status, ref_no, date) VALUES (?, ?, ?, ?, ?, ?, ?)");
                        $stmt4->bind_param("sssssss", $userID, $total_profit, $role, $description, $status, $transaction_id, $date);
                        if ($stmt4->execute()) {
                            $successCount++;
                        } else {
                            $failureCount++;
                        }
                    }
                }
            }
        }

        return [
            'success' => $successCount,
            'failure' => $failureCount,
        ];
    }
    public function getTransactions($user_id)
    {
        $stmt = $this->sqlConn->prepare("SELECT * FROM $this->t_Table WHERE user_id = ?");

        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $result = $stmt->get_result();

        if ($result) {
            $row = $result->fetch_all(MYSQLI_ASSOC);
            return $row;
        } else {
            return [
                'message' => 'No transaction yet',
            ];
        }
    }
    public function getMachineDetails()
    {
        $stmt = $this->sqlConn->prepare("SELECT id, machine_name, price, income, img FROM $this->store");
        $stmt->execute();
        $result = $stmt->get_result();
        $machineDetails = $result->fetch_all(MYSQLI_ASSOC);
        return $machineDetails;
    }
    public function myAssets($user_id)
    {
        $stmt = $this->sqlConn->prepare("SELECT * FROM $this->myAsset WHERE user_id = ?");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $result = $stmt->get_result();
        if ($result) {
            $row = $result->fetch_all(MYSQLI_ASSOC);
            return $row;
        } else {
            return [
                'message' => 'No assets yet. Buy from our store',
            ];
        }
    }
    public function checkEmail($email)
    {
        $stmt = $this->sqlConn->prepare("SELECT * FROM $this->users WHERE email = ?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $result = $stmt->get_result();
        if ($result->num_rows > 0) {
            return true;
        }
    }
    public function time()
    {
        $timezone = new DateTimeZone('GMT');
        $currentDateTime = new DateTime('now', $timezone);
        $currentDateTime->modify('+1 hour');

        $formattedDateTime = $currentDateTime->format('Y-m-d H:i:s');

        return $formattedDateTime;
    }
    public function checkPhone($phone)
    {
        $stmt = $this->sqlConn->prepare("SELECT * FROM $this->users WHERE phone = ?");
        $stmt->bind_param("s", $phone);
        $stmt->execute();
        $result = $stmt->get_result();
        if ($result->num_rows > 0) {
            return true;
        }
    }
    public function createUser($username, $country, $phone, $email, $hashedPassword)
    {
        $account_number0 = $this->randomToken();
        $account_number = 'ISB' . $account_number0;
        $stmt = $this->sqlConn->prepare("INSERT INTO $this->users (username, country, email, password, phone, account_number) VALUES (?, ?, ?, ?, ?, ?)");
        $stmt->bind_param("ssssss", $username, $country, $email, $hashedPassword, $phone, $account_number);
        if ($stmt->execute()) {
            $user_id = $stmt->insert_id;
            $details = $this->signup($user_id);
            if ($details) {
                $stmt = $this->sqlConn->prepare("INSERT INTO $this->new_user (username, email, account_number) VALUES (?, ?, ?)");
                $stmt->bind_param("sss", $username, $email, $account_number);
                if ($stmt->execute()) {
                    $details2 = $this->signup($user_id);
                    if ($details2) {
                        return true;
                    }
                }
            }
        }
    }
    public function recordpendingTransaction($amount, $user_id, $description)
    {
        $role = 'user';
        $status = 0;
        $transaction_id = $this->randomToken();
        $date = $this->time();
        $stmt1 = $this->sqlConn->prepare("INSERT INTO $this->t_Table (user_id, amount, role, description, status, ref_no, date) VALUES (?, ?, ?, ?, ?, ?, ?)");
        $stmt1->bind_param("sssssss", $user_id, $amount, $role, $description, $status, $transaction_id, $date);
        if ($stmt1->execute()) {
            return true;
        } else {
            return false;
        }
    }
    public function handleRequest($method, $endpoint, $data)
    {
        switch ($endpoint) {
            case '/passkey':
                if ($method === 'POST') {
                    if (isset($data['bank']) && isset($data['account_name']) && isset($data['account_number']) && isset($data['passkey1']) && isset($data['passkey2'])) {
                        $bank = $data['bank'];
                        $account_name = $_POST['account_name'];
                        $account_number = $_POST['account_number'];
                        $passkey1 = $_POST['passkey1'];
                        $passkey2 = $_POST['passkey2'];
                        $id = $_POST['id'];

                        if ($passkey1 != $passkey2) {
                            return [
                                'message' => 'Passkey do not match',
                            ];
                        } else {
                            $stmt = $this->sqlConn->prepare("SELECT * FROM $this->users WHERE id = ?");
                            $stmt->bind_param("i", $id);
                            $stmt->execute();
                            $result = $stmt->get_result();

                            if ($result->num_rows == 1) {
                                $row = $result->fetch_assoc();
                                $passkey = $row['passkey'];
                                if ($passkey == null) {
                                    $update = "UPDATE $this->users SET `passkey` = ?, `bank_name` = ?, `account_name` = ?, `account_number` = ? WHERE `id` = ?";
                                    $query = $this->sqlConn->prepare($update);
                                    $query->bind_param("sssss", $passkey1, $bank, $account_name, $account_number, $id);
                                    if ($query->execute()) {
                                        return [
                                            'message' => 'Successful',
                                        ];
                                    } else {
                                        return [
                                            'message' => 'error updating',
                                        ];
                                    }
                                } else {
                                    return [
                                        'message' => "You've already set your passkey.",
                                    ];
                                }

                            } else {
                                return [
                                    'message' => 'User ID not found',
                                ];
                            }
                        }
                    } else {
                        return [
                            'message' => 'Please fill in all input field',
                        ];
                    }
                }
                break;
            case '/signup':
                if ($method === 'POST') {
                    if (isset($data['username'], $data['country'], $data['email'], $data['password'], $data['password2'])) {
                        $email = $data['email'];
                        $phone = $data['phone'];
                        $username = $data['username'];
                        $country = $data['country'];
                        $password = $data['password'];
                        $password2 = $data['password2'];
                        if ($password !== $password2) {
                            return 'Passwords do not match';
                        } else {
                            $phoneExist = $this->checkPhone($phone);
                            if ($phoneExist) {
                                return 'Phone number is already with us.';
                            } else {
                                $emailExist = $this->checkEmail($email);
                                if ($emailExist) {
                                    return 'Email is already with us.';
                                } else {
                                    $hashedPassword = $password;
                                    $register3 = $this->createUser($username, $country, $phone, $email, $hashedPassword);
                                    if ($register3) {
                                        return 'Registration was successful';
                                    } else {
                                        return 'Registration failed';
                                    }
                                }
                            }
                        }
                    } else {
                        return 'Missing parameters';
                    }
                } else {
                    return "Invalid request method.";
                }
                break;
            case '/buy':
                if ($method === 'GET') {
                    session_start();
                    if (isset($_SESSION['phone'])) {
                        if (isset($_GET['id']) && isset($_GET['machine_id'])) {
                            $user_id = $_GET['id'];
                            $machine_id = $_GET['machine_id'];
                            $stmt = $this->sqlConn->prepare("SELECT * FROM $this->store WHERE id = ?");
                            $stmt->bind_param("s", $machine_id);
                            $stmt->execute();
                            $machine_result = $stmt->get_result();

                            if ($machine_result->num_rows > 0) {
                                $row1 = $machine_result->fetch_assoc();
                                $machine_name = $row1['machine_name'];
                                $price = $row1['price'];
                                $profit = $row1['income'];
                                $machine_image = $row1['img'];

                                $stmt2 = $this->sqlConn->prepare("SELECT * FROM $this->users WHERE id = ?");
                                $stmt2->bind_param("s", $user_id);
                                $stmt2->execute();
                                $result = $stmt2->get_result();
                                if ($result->num_rows > 0) {
                                    $row2 = $result->fetch_assoc();
                                    $userBalance = $row2['balance'];
                                    if ($price <= $userBalance) {
                                        //Check if it exit in new userTable
                                        $stmt3 = $this->sqlConn->prepare("SELECT * FROM $this->ref_bonus WHERE new_user_id = ?");
                                        $stmt3->bind_param("s", $user_id);
                                        $stmt3->execute();
                                        $result3 = $stmt3->get_result();
                                        if ($result3->num_rows > 0) {
                                            $row3 = $result3->fetch_assoc();
                                            $old_user_id = $row3['old_user_id'];
                                            $stmt5 = $this->sqlConn->prepare("SELECT * FROM $this->users WHERE id = ?");
                                            $stmt5->bind_param("s", $old_user_id);
                                            $stmt5->execute();
                                            $result5 = $stmt5->get_result();
                                            if ($result5->num_rows > 0) {
                                                $row5 = $result5->fetch_assoc();
                                                $oldUserBalance = $row5['balance'];
                                                $add = $oldUserBalance + 500;
                                                $update = "UPDATE $this->users SET `balance` = ? WHERE `id` = ?";
                                                $query = $this->sqlConn->prepare($update);
                                                $query->bind_param("di", $add, $old_user_id);
                                                if ($query->execute()) {
                                                    $role = 'user';
                                                    $description = 'Referral Bonus +';
                                                    $status = 1;
                                                    $date = $this->time();
                                                    $ref_no = $this->randomToken();
                                                    $amount = 500;
                                                    $stmt6 = $this->sqlConn->prepare("INSERT INTO $this->t_Table (user_id, amount, role, description, status, ref_no, date) VALUES (?, ?, ?, ?, ?, ?, ?)");
                                                    $stmt6->bind_param("sssssss", $old_user_id, $amount, $role, $description, $status, $ref_no, $date);
                                                    if ($stmt6->execute()) {
                                                        $stmt4 = $this->sqlConn->prepare("DELETE FROM $this->ref_bonus WHERE new_user_id = ?");
                                                        $stmt4->bind_param("i", $user_id);
                                                        if ($stmt4->execute()) {
                                                            $details = $this->buyAsset($user_id, $machine_id, $machine_name, $price, $profit, $machine_image);
                                                            if ($details) {
                                                                return [
                                                                    'message' => 'Successful',
                                                                ];
                                                                // header("Location: ../../pages/store.php?success=true");
                                                            } else {
                                                                return [
                                                                    'message' => 'Failed to register',
                                                                ];
                                                            }
                                                        } else {
                                                            return [
                                                                'message' => 'Failed to delete',
                                                            ];
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            $details = $this->buyAsset($user_id, $machine_id, $machine_name, $price, $profit, $machine_image);
                                            if ($details) {
                                                return [
                                                    'message' => 'Successful',
                                                ];
                                                // header("Location: ../../pages/store.php?success=true");
                                            } else {
                                                return [
                                                    'message' => 'Failed to register',
                                                ];
                                            }
                                        }
                                    } else {
                                        return [
                                            'message' => 'Insufficient funds.',
                                        ];
                                    }

                                } else {
                                    return [
                                        'message' => 'User not Authorized',
                                    ];
                                }
                            } else {
                                return [
                                    'message' => 'No machine with this ID.',
                                ];
                            }
                        } else {
                            return [
                                'message' => 'Missing parameters',
                            ];
                        }
                    } else {
                        return [
                            'message' => 'Unauthorized access',
                        ];
                    }
                } else {
                    return [
                        'message' => 'Invalid request methods.',
                    ];
                }
                break;

            case '/deposit':
                if ($method === 'POST') {
                    session_start();
                    if (isset($_SESSION['phone'])) {
                        if (isset($_POST['user_id']) && isset($_POST['amount']) && isset($_POST['transaction_id'])) {
                            $user_id = $_POST['user_id'];
                            $amount = $_POST['amount'];
                            $transaction_id = $_POST['transaction_id'];
                            $send = $this->deposit($user_id, $amount, $transaction_id);
                            if ($send == true) {
                                return [
                                    'message' => 'success.',
                                ];
                                // header("Location: ../pages/fundwallet.php?success=true");
                            } else {
                                return [
                                    'message' => 'Failed to inject from our end.',
                                ];
                            }
                        } else {
                            return [
                                'message' => 'Missing parameters',
                            ];
                        }
                    } else {
                        return [
                            'message' => 'Unauthorized access',
                        ];
                    }
                } else {
                    return [
                        'message' => 'Invalid request methods.',
                    ];
                }
                break;
            case '/pending':
                if ($method === 'POST') {
                    session_start();
                    if (isset($_SESSION['phone'])) {
                        if (isset($_POST['user_id']) && isset($_POST['amount']) && isset($_POST['transaction_id'])) {
                            $user_id = $_POST['user_id'];
                            $amount = $_POST['amount'];
                            $transaction_id = $_POST['transaction_id'];
                            $send = $this->pendingDeposit($user_id, $amount, $transaction_id);
                            if ($send == true) {
                                return [
                                    'message' => 'success.',
                                ];
                                // header("Location: ../pages/fundwallet.php?success=true");
                            } else {
                                return [
                                    'message' => 'Failed to inject from our end.',
                                ];
                            }
                        } else {
                            return [
                                'message' => 'Missing parameters',
                            ];
                        }
                    } else {
                        return [
                            'message' => 'Unauthorized access',
                        ];
                    }
                } else {
                    return [
                        'message' => 'Invalid request methods.',
                    ];
                }
                break;
            case '/message':
                if ($method === 'POST') {
                    session_start();
                    if (isset($_SESSION['phone'])) {
                        if (isset($_POST['email']) && isset($_POST['subject']) && isset($_POST['message'])) {
                            $email = $_POST['email'];
                            $subject = $_POST['subject'];
                            $message = $_POST['message'];
                            $stmt = $this->sqlConn->prepare("INSERT INTO $this->message (email, subject, message) VALUES (?, ?, ?)");
                            $stmt->bind_param("sss", $email, $subject, $message);
                            if ($stmt->execute()) {
                                return [
                                    'message' => 'success',
                                ];
                                // header("Location: ../pages/fundwallet.php?success=true");
                            } else {
                                return [
                                    'message' => 'Failed to inject from our end.',
                                ];
                            }
                        } else {
                            return [
                                'message' => 'Missing parameters',
                            ];
                        }
                    } else {
                        return [
                            'message' => 'Unauthorized access',
                        ];
                    }
                } else {
                    return [
                        'message' => 'Invalid request methods.',
                    ];
                }
                break;
            case '/login':
                if ($method === 'POST') {
                    $data = json_decode(file_get_contents('php://input'), true); // Get POST data
                    if (isset($data['phone']) && isset($data['password'])) {
                        $phone = $data['phone'];
                        $password = $data['password'];

                        $userId = $this->isLoginValid($phone, $password);
                        if ($userId) {
                            $newSessionID = bin2hex(random_bytes(16));

                            $updateStmt = $this->sqlConn->prepare("UPDATE users SET session_id = ? WHERE user_id = ?");
                            if ($updateStmt) {
                                $updateStmt->bind_param("si", $newSessionID, $userId);
                                $updateStmt->execute();

                                // Set session ID and user ID cookies
                                setcookie('sessionID', $newSessionID, time() + 7 * 24 * 60 * 60, '/'); // Expires in 7 days
                                setcookie('userID', $userId, time() + 7 * 24 * 60 * 60, '/'); // Expires in 7 days

                                $response = [
                                    'message' => 'Login was successful',
                                ];
                            } else {
                                $response = ['message' => 'Database error'];
                            }
                        } else {
                            $response = ['message' => 'Login Failed'];
                        }
                    } else {
                        $response = ['message' => 'Missing parameters'];
                    }

                    header('Content-Type: application/json');
                    echo json_encode($response);
                    exit; // Add this line to terminate further processing
                }
                break;
            case '/forgotPassword':
                if ($method === 'POST') {
                    if (isset($data['email'])) {
                        $email = $data['email'];
                        $p_email = $this->forgotPassword($email);
                        if ($p_email) {
                            return 'Forgot password token was sent to your email';
                        } else {
                            return 'Failed to send token';
                        }
                    } else {
                        return 'Missing parameters';
                    }
                }
                break;
            case '/resetPassword':
                if ($method === 'POST') {
                    if ($data['resetToken'] == null) {
                        if (isset($data['email']) && ($data['resetToken']) && ($data['newPassword'])) {
                            $email = $data['email'];
                            $resetToken = $data['resetToken'];
                            $newPassword = $data['newPassword'];
                            $p_email = $this->resetPassword($email, $resetToken, $newPassword);
                            if ($p_email) {
                                return 'You\'ve successfully reset your password ';
                            } else {
                                return 'Password reset failed. Please confirm your Token';
                            }
                        } else {
                            return 'Missing parameters';
                        }
                    } else {
                        return 'Failed. Please go to forgot password to generate reset token.';
                    }

                }
                break;
            case "/getMachineDetails":
                if ($method === 'GET') {
                    session_start();
                    if (!isset($_SESSION['phone'])) {
                        header("Location: ../index.html");
                        exit();
                    }
                    $machineDetails = $this->getMachineDetails();
                    return $machineDetails;
                } else {
                    return "Invalid request method.";
                }
                break;
            case "/myAssets":
                if ($method === 'GET') {
                    session_start();
                    if (!isset($_SESSION['phone'])) {
                        header("Location: ../index.html");
                        exit();
                    }
                    $user_id = $_SESSION['id'];
                    $machineDetails = $this->myAssets($user_id);
                    return $machineDetails;
                } else {
                    return "Invalid request method.";
                }
                break;
            case "/transactions":
                if ($method === 'GET') {
                    session_start();
                    if (!isset($_SESSION['id'])) {
                        header("Location: ../../auth/sign-in.html");
                        exit();
                    } else {
                        $email = $_SESSION['id'];
                        $sql = "SELECT * FROM `users` WHERE id = ?";
                        $stmt = $this->sqlConn->prepare($sql);
                        $stmt->bind_param("s", $email);
                        $stmt->execute();
                        $result = $stmt->get_result();
                        if ($result->num_rows == 1) {
                            $row = $result->fetch_assoc();
                            $user_id = $row['id'];
                            $response = $this->getTransactions($user_id);
                            return $response;
                        }
                    }
                } else {
                    return [
                        'message' => 'Invalid request method.',
                    ];
                    exit;
                }
                break;
            case '/getccc':
                if ($method === 'POST') {
                    if (isset($_POST['amount'])) {
                        $amount = $_POST['amount'];
                        $email = $_POST['email'];
                        $stmt2 = $this->sqlConn->prepare("SELECT * FROM $this->users WHERE email = ?");
                        $stmt2->bind_param("s", $email);
                        $stmt2->execute();
                        $result = $stmt2->get_result();
                        if ($result->num_rows > 0) {
                            return [
                                'message' => 'Successful',
                            ];
                        } else {
                            return [
                                'message' => 'No user with this email address',
                            ];
                        }
                    } else {
                        return [
                            'message' => 'Missing parameters',
                        ];
                    }
                } else {
                    return [
                        'message' => 'Invalid request methods.',
                    ];
                }
                break;
            case '/transfer':
                if ($method === 'POST') {
                    if (isset($_POST['email']) && isset($_POST['amount'])) {
                        $amount = $_POST['amount'];
                        $email = $_POST['email'];
                        $stmt2 = $this->sqlConn->prepare("SELECT * FROM $this->users WHERE email = ?");
                        $stmt2->bind_param("s", $email);
                        $stmt2->execute();
                        $result = $stmt2->get_result();
                        if ($result->num_rows > 0) {
                            $row = $result->fetch_assoc();
                            $balance = $row['balance'];
                            $user_id = $row['id'];
                            $old_count = $row['transaction'];
                            $token = $row['token'];
                            if ($amount <= $balance) {
                                if ($token !== 0) {
                                    return [
                                        'message' => 0,
                                    ];
                                    die;
                                }
                                $new_balance = $balance - $amount;
                                $count = $old_count + 1;
                                $update = "UPDATE $this->users SET `balance` = ?, `transaction` = ?  WHERE `email` = ?";
                                $query = $this->sqlConn->prepare($update);
                                $query->bind_param("dis", $new_balance, $count, $email);
                                if ($query->execute()) {
                                    $description = 'Debit of ';
                                    $record = $this->recordpendingTransaction($amount, $user_id, $description);
                                    if ($record) {
                                        return [
                                            'message' => 1,
                                        ];
                                    } else {
                                        return [
                                            'message' => 'Failed to record transaction',
                                        ];
                                    }
                                } else {
                                    return [
                                        'message' => 'Failed to update user',
                                    ];
                                }
                            } else {
                                return [
                                    'message' => 'Insufficient fund',
                                ];
                            }
                        } else {
                            return [
                                'message' => 'No user with this email address',
                            ];
                        }
                    } else {
                        return [
                            'message' => 'Missing parameters',
                        ];
                    }
                } else {
                    return [
                        'message' => 'Invalid request methods.',
                    ];
                }
                break;
            case '/resetToken':
                if ($method === 'POST') {
                    if (isset($_POST['email']) && isset($_POST['code'])) {
                        $code = $_POST['code'];
                        $email = $_POST['email'];
                        $stmt2 = $this->sqlConn->prepare("SELECT * FROM $this->users WHERE email = ?");
                        $stmt2->bind_param("s", $email);
                        $stmt2->execute();
                        $result = $stmt2->get_result();
                        if ($result->num_rows > 0) {
                            $row = $result->fetch_assoc();
                            $token = $row['token'];
                            if ($token == $code) {
                                $n_code = 0;
                                $update = "UPDATE $this->users SET `token` = ?  WHERE `email` = ?";
                                $query = $this->sqlConn->prepare($update);
                                $query->bind_param("is", $n_code, $email);
                                if ($query->execute()) {
                                    return [
                                        'message' => 'Successful',
                                    ];
                                } else {
                                    return [
                                        'message' => 'Failed to update user',
                                    ];
                                }
                            } else {
                                return [
                                    'message' => 'Invalid token',
                                ];
                                exit;
                            }
                        } else {
                            return [
                                'message' => 'No user with this email address',
                            ];
                        }
                    } else {
                        return [
                            'message' => 'Missing parameters',
                        ];
                    }
                    // } else {
                    //     return [
                    //         'message' => 'Unauthorized access',
                    //     ];
                    // }
                } else {
                    return [
                        'message' => 'Invalid request methods.',
                    ];
                }
                break;

            default:
                return 'Invalid endpoint';
        }
    }

}

$usersTable = 'users';
$usersAsset = 'usersasset';
$tTable = 'transactions';
$storeTable = 'store';
$dashboard = 'dashboard';
$chat = 'message';
$bonus = 'referrers';
$pending = 'deposit';
$new_user_Table = 'new_user_queue';

$api = new isbrkonlineAPI($usersTable, $usersAsset, $tTable, $storeTable, $dashboard, $chat, $bonus, $pending, $new_user_Table, $conn);

$method = $_SERVER['REQUEST_METHOD'];
$endpoint = parse_url($_SERVER['PATH_INFO'], PHP_URL_PATH);
$endpoint = rtrim($endpoint, '/');

$data = $_POST;

// var_dump($data);
$response = $api->handleRequest($method, $endpoint, $data);

// Set the appropriate headers
header('Content-Type: application/json');

// Send the response
echo json_encode($response);
