<?php
session_start();
if (!isset($_SESSION['id'])) {
    header("Location: auth/sign-in.html");
    exit();
}
?>  
<!DOCTYPE html>
<html lang="en">

<head>
	<base href="" />
	<title>IsBank Dashboard</title>
	<meta charset="utf-8" />
	<meta name="description"
		content="The most advanced Bootstrap 5 Admin Theme with 40 unique prebuilt layouts on Themeforest trusted by 100,000 beginners and professionals. Multi-demo, Dark Mode, RTL support and complete React, Angular, Vue, Asp.Net Core, Rails, Spring, Blazor, Django, Express.js, Node.js, Flask, Symfony & Laravel versions. Grab your copy now and get life-time updates for free." />
	<meta name="keywords"
		content="metronic, bootstrap, bootstrap 5, angular, VueJs, React, Asp.Net Core, Rails, Spring, Blazor, Django, Express.js, Node.js, Flask, Symfony & Laravel starter kits, admin themes, web design, figma, web development, free templates, free admin themes, bootstrap theme, bootstrap template, bootstrap dashboard, bootstrap dak mode, bootstrap button, bootstrap datepicker, bootstrap timepicker, fullcalendar, datatables, flaticon" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta property="og:locale" content="en_US" />
	<meta property="og:type" content="article" />
	<meta property="og:title"
		content="Metronic - Bootstrap Admin Template, HTML, VueJS, React, Angular. Laravel, Asp.Net Core, Ruby on Rails, Spring Boot, Blazor, Django, Express.js, Node.js, Flask Admin Dashboard Theme & Template" />
	<meta property="og:url" content="https://keenthemes.com/metronic" />
	<meta property="og:site_name" content="Keenthemes | Metronic" />
	<link rel="canonical" href="https://preview.keenthemes.com/metronic8" />
	<link rel="shortcut icon" href="assets/media/logos/favicon.ico" />
	<!--begin::Fonts(mandatory for all pages)-->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700" />
	<!--end::Fonts-->
	<!--begin::Vendor Stylesheets(used for this page only)-->
	<link href="assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
	<link href="assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	<!--end::Vendor Stylesheets-->
	<!--begin::Global Stylesheets Bundle(mandatory for all pages)-->
	<link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
	<!--end::Global Stylesheets Bundle-->
</head>
<!--end::Head-->
<!--begin::Body-->

<body id="kt_app_body" data-kt-app-layout="dark-sidebar" data-kt-app-header-fixed="true"
	data-kt-app-sidebar-enabled="true" data-kt-app-sidebar-fixed="true" data-kt-app-sidebar-hoverable="true"
	data-kt-app-sidebar-push-header="true" data-kt-app-sidebar-push-toolbar="true"
	data-kt-app-sidebar-push-footer="true" data-kt-app-toolbar-enabled="true" class="app-default" style="">
	<!--begin::Theme mode setup on page load-->
	<script>var defaultThemeMode = "light"; var themeMode; if (document.documentElement) { if (document.documentElement.hasAttribute("data-bs-theme-mode")) { themeMode = document.documentElement.getAttribute("data-bs-theme-mode"); } else { if (localStorage.getItem("data-bs-theme") !== null) { themeMode = localStorage.getItem("data-bs-theme"); } else { themeMode = defaultThemeMode; } } if (themeMode === "system") { themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light"; } document.documentElement.setAttribute("data-bs-theme", themeMode); }</script>
	<!--end::Theme mode setup on page load-->
	<!--begin::App-->
	<div class="d-flex flex-column flex-root app-root" id="kt_app_root">
		<!--begin::Page-->
		<div class="app-page flex-column flex-column-fluid" id="kt_app_page">
			<!--begin::Header-->
			<div id="kt_app_header" class="app-header">
				<!--begin::Header container-->
				<div class="app-container container-fluid d-flex align-items-stretch justify-content-between"
					id="kt_app_header_container">
					<!--begin::Sidebar mobile toggle-->
					<div class="d-flex align-items-center d-lg-none ms-n3 me-1 me-md-2" title="Show sidebar menu">
						<div class="btn btn-icon btn-active-color-primary w-35px h-35px"
							id="kt_app_sidebar_mobile_toggle">
							<!--begin::Svg Icon | path: icons/duotune/abstract/abs015.svg-->
							<span class="svg-icon svg-icon-2 svg-icon-md-1">
								<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
									xmlns="http://www.w3.org/2000/svg">
									<path
										d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z"
										fill="currentColor"></path>
									<path opacity="0.3"
										d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z"
										fill="currentColor"></path>
								</svg>
							</span>
							<!--end::Svg Icon-->
						</div>
					</div>
					<!--end::Sidebar mobile toggle-->
					<!--begin::Mobile logo-->
					<div class="d-flex align-items-center flex-grow-1 flex-lg-grow-0">
						<a href="../../demo1/dist/index.html" class="d-lg-none">
							<img alt="Logo" src="assets/media/logo.png" class="h-35px">
						</a>
					</div>
					<!--end::Mobile logo-->
					<!--begin::Header wrapper-->
					<div class="d-flex align-items-stretch justify-content-between flex-lg-grow-1"
						id="kt_app_header_wrapper">
						<div class="app-header-menu app-header-mobile-drawer align-items-stretch" data-kt-drawer="true"
							data-kt-drawer-name="app-header-menu" data-kt-drawer-activate="{default: true, lg: false}"
							data-kt-drawer-overlay="true" data-kt-drawer-width="250px" data-kt-drawer-direction="end"
							data-kt-drawer-toggle="#kt_app_header_menu_toggle" data-kt-swapper="true"
							data-kt-swapper-mode="{default: 'append', lg: 'prepend'}"
							data-kt-swapper-parent="{default: '#kt_app_body', lg: '#kt_app_header_wrapper'}" style="">
							<!--begin::Menu-->
							<div class="menu menu-rounded menu-column menu-lg-row my-5 my-lg-0 align-items-stretch fw-semibold px-2 px-lg-0"
								id="kt_app_header_menu" data-kt-menu="true">
								<!--begin:Menu item-->
								<div data-kt-menu-trigger="{default: 'click', lg: 'hover'}"
									data-kt-menu-placement="bottom-start"
									class="menu-item here show menu-here-bg menu-lg-down-accordion me-0 me-lg-2 hiding">
									<!--begin:Menu link-->
									<span class="menu-link">
										<span class="menu-title">Dashboards</span>
										<span class="menu-arrow d-lg-none"></span>
									</span>
									<!--end:Menu link-->
								</div>
								<!--end:Menu item-->
							</div>
							<!--end::Menu-->
						</div>


						<!--begin::Menu wrapper-->

						<!--end::Menu wrapper-->
						<!--begin::Navbar-->
						<div class="app-navbar flex-shrink-0">
							<div class="app-navbar-item ms-1 ms-md-3">
								<!--begin::Menu wrapper-->
								<div
									class="btn btn-icon btn-custom btn-icon-muted btn-active-light btn-active-color-primary w-30px h-30px w-md-40px h-md-40px position-relative">
									<span class="svg-icon svg-icon-2 svg-icon-md-1">
										<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
											fill="currentColor" class="bi bi-bell-fill" viewBox="0 0 16 16">
											<path
												d="M8 16a2 2 0 0 0 2-2H6a2 2 0 0 0 2 2m.995-14.901a1 1 0 1 0-1.99 0A5 5 0 0 0 3 6c0 1.098-.5 6-2 7h14c-1.5-1-2-5.902-2-7 0-2.42-1.72-4.44-4.005-4.901">
											</path>
										</svg>
									</span>
									<!--end::Svg Icon-->
									<span
										class="bullet bullet-dot bg-success h-6px w-6px position-absolute translate-middle top-0 start-50 animation-blink"></span>
								</div>
								<!--end::Menu wrapper-->
							</div>
							<!--begin::Theme mode-->
							<div class="app-navbar-item ms-1 ms-md-3">
								<!--begin::Menu toggle-->
								<a href="#"
									class="btn btn-icon btn-custom btn-icon-muted btn-active-light btn-active-color-primary w-30px h-30px w-md-40px h-md-40px"
									data-kt-menu-trigger="{default:'click', lg: 'hover'}" data-kt-menu-attach="parent"
									data-kt-menu-placement="bottom-end">
									<!--begin::Svg Icon | path: icons/duotune/general/gen060.svg-->
									<span class="svg-icon theme-light-show svg-icon-2">
										<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
											xmlns="http://www.w3.org/2000/svg">
											<path
												d="M11.9905 5.62598C10.7293 5.62574 9.49646 5.9995 8.44775 6.69997C7.39903 7.40045 6.58159 8.39619 6.09881 9.56126C5.61603 10.7263 5.48958 12.0084 5.73547 13.2453C5.98135 14.4823 6.58852 15.6185 7.48019 16.5104C8.37186 17.4022 9.50798 18.0096 10.7449 18.2557C11.9818 18.5019 13.2639 18.3757 14.429 17.8931C15.5942 17.4106 16.5901 16.5933 17.2908 15.5448C17.9915 14.4962 18.3655 13.2634 18.3655 12.0023C18.3637 10.3119 17.6916 8.69129 16.4964 7.49593C15.3013 6.30056 13.6808 5.62806 11.9905 5.62598Z"
												fill="currentColor"></path>
											<path
												d="M22.1258 10.8771H20.627C20.3286 10.8771 20.0424 10.9956 19.8314 11.2066C19.6204 11.4176 19.5018 11.7038 19.5018 12.0023C19.5018 12.3007 19.6204 12.5869 19.8314 12.7979C20.0424 13.0089 20.3286 13.1274 20.627 13.1274H22.1258C22.4242 13.1274 22.7104 13.0089 22.9214 12.7979C23.1324 12.5869 23.2509 12.3007 23.2509 12.0023C23.2509 11.7038 23.1324 11.4176 22.9214 11.2066C22.7104 10.9956 22.4242 10.8771 22.1258 10.8771Z"
												fill="currentColor"></path>
											<path
												d="M11.9905 19.4995C11.6923 19.5 11.4064 19.6187 11.1956 19.8296C10.9848 20.0405 10.8663 20.3265 10.866 20.6247V22.1249C10.866 22.4231 10.9845 22.7091 11.1953 22.9199C11.4062 23.1308 11.6922 23.2492 11.9904 23.2492C12.2886 23.2492 12.5746 23.1308 12.7854 22.9199C12.9963 22.7091 13.1147 22.4231 13.1147 22.1249V20.6247C13.1145 20.3265 12.996 20.0406 12.7853 19.8296C12.5745 19.6187 12.2887 19.5 11.9905 19.4995Z"
												fill="currentColor"></path>
											<path
												d="M4.49743 12.0023C4.49718 11.704 4.37865 11.4181 4.16785 11.2072C3.95705 10.9962 3.67119 10.8775 3.37298 10.8771H1.87445C1.57603 10.8771 1.28984 10.9956 1.07883 11.2066C0.867812 11.4176 0.749266 11.7038 0.749266 12.0023C0.749266 12.3007 0.867812 12.5869 1.07883 12.7979C1.28984 13.0089 1.57603 13.1274 1.87445 13.1274H3.37299C3.6712 13.127 3.95706 13.0083 4.16785 12.7973C4.37865 12.5864 4.49718 12.3005 4.49743 12.0023Z"
												fill="currentColor"></path>
											<path
												d="M11.9905 4.50058C12.2887 4.50012 12.5745 4.38141 12.7853 4.17048C12.9961 3.95954 13.1147 3.67361 13.1149 3.3754V1.87521C13.1149 1.57701 12.9965 1.29103 12.7856 1.08017C12.5748 0.869313 12.2888 0.750854 11.9906 0.750854C11.6924 0.750854 11.4064 0.869313 11.1955 1.08017C10.9847 1.29103 10.8662 1.57701 10.8662 1.87521V3.3754C10.8664 3.67359 10.9849 3.95952 11.1957 4.17046C11.4065 4.3814 11.6923 4.50012 11.9905 4.50058Z"
												fill="currentColor"></path>
											<path
												d="M18.8857 6.6972L19.9465 5.63642C20.0512 5.53209 20.1343 5.40813 20.1911 5.27163C20.2479 5.13513 20.2772 4.98877 20.2774 4.84093C20.2775 4.69309 20.2485 4.54667 20.192 4.41006C20.1355 4.27344 20.0526 4.14932 19.948 4.04478C19.8435 3.94024 19.7194 3.85734 19.5828 3.80083C19.4462 3.74432 19.2997 3.71531 19.1519 3.71545C19.0041 3.7156 18.8577 3.7449 18.7212 3.80167C18.5847 3.85845 18.4607 3.94159 18.3564 4.04633L17.2956 5.10714C17.1909 5.21147 17.1077 5.33543 17.0509 5.47194C16.9942 5.60844 16.9649 5.7548 16.9647 5.90264C16.9646 6.05048 16.9936 6.19689 17.0501 6.33351C17.1066 6.47012 17.1895 6.59425 17.294 6.69878C17.3986 6.80332 17.5227 6.88621 17.6593 6.94272C17.7959 6.99923 17.9424 7.02824 18.0902 7.02809C18.238 7.02795 18.3844 6.99865 18.5209 6.94187C18.6574 6.88509 18.7814 6.80195 18.8857 6.6972Z"
												fill="currentColor"></path>
											<path
												d="M18.8855 17.3073C18.7812 17.2026 18.6572 17.1195 18.5207 17.0627C18.3843 17.006 18.2379 16.9767 18.0901 16.9766C17.9423 16.9764 17.7959 17.0055 17.6593 17.062C17.5227 17.1185 17.3986 17.2014 17.2941 17.3059C17.1895 17.4104 17.1067 17.5345 17.0501 17.6711C16.9936 17.8077 16.9646 17.9541 16.9648 18.1019C16.9649 18.2497 16.9942 18.3961 17.0509 18.5326C17.1077 18.6691 17.1908 18.793 17.2955 18.8974L18.3563 19.9582C18.4606 20.0629 18.5846 20.146 18.721 20.2027C18.8575 20.2595 19.0039 20.2887 19.1517 20.2889C19.2995 20.289 19.4459 20.26 19.5825 20.2035C19.7191 20.147 19.8432 20.0641 19.9477 19.9595C20.0523 19.855 20.1351 19.7309 20.1916 19.5943C20.2482 19.4577 20.2772 19.3113 20.277 19.1635C20.2769 19.0157 20.2476 18.8694 20.1909 18.7329C20.1341 18.5964 20.051 18.4724 19.9463 18.3681L18.8855 17.3073Z"
												fill="currentColor"></path>
											<path
												d="M5.09528 17.3072L4.0345 18.368C3.92972 18.4723 3.84655 18.5963 3.78974 18.7328C3.73294 18.8693 3.70362 19.0156 3.70346 19.1635C3.7033 19.3114 3.7323 19.4578 3.78881 19.5944C3.84532 19.7311 3.92822 19.8552 4.03277 19.9598C4.13732 20.0643 4.26147 20.1472 4.3981 20.2037C4.53473 20.2602 4.68117 20.2892 4.82902 20.2891C4.97688 20.2889 5.12325 20.2596 5.25976 20.2028C5.39627 20.146 5.52024 20.0628 5.62456 19.958L6.68536 18.8973C6.79007 18.7929 6.87318 18.6689 6.92993 18.5325C6.98667 18.396 7.01595 18.2496 7.01608 18.1018C7.01621 17.954 6.98719 17.8076 6.93068 17.671C6.87417 17.5344 6.79129 17.4103 6.68676 17.3058C6.58224 17.2012 6.45813 17.1183 6.32153 17.0618C6.18494 17.0053 6.03855 16.9763 5.89073 16.9764C5.74291 16.9766 5.59657 17.0058 5.46007 17.0626C5.32358 17.1193 5.19962 17.2024 5.09528 17.3072Z"
												fill="currentColor"></path>
											<path
												d="M5.09541 6.69715C5.19979 6.8017 5.32374 6.88466 5.4602 6.94128C5.59665 6.9979 5.74292 7.02708 5.89065 7.02714C6.03839 7.0272 6.18469 6.99815 6.32119 6.94164C6.45769 6.88514 6.58171 6.80228 6.68618 6.69782C6.79064 6.59336 6.87349 6.46933 6.93 6.33283C6.9865 6.19633 7.01556 6.05003 7.01549 5.9023C7.01543 5.75457 6.98625 5.60829 6.92963 5.47184C6.87301 5.33539 6.79005 5.21143 6.6855 5.10706L5.6247 4.04626C5.5204 3.94137 5.39643 3.8581 5.25989 3.80121C5.12335 3.74432 4.97692 3.71493 4.82901 3.71472C4.68109 3.71452 4.53458 3.7435 4.39789 3.80001C4.26119 3.85652 4.13699 3.93945 4.03239 4.04404C3.9278 4.14864 3.84487 4.27284 3.78836 4.40954C3.73185 4.54624 3.70287 4.69274 3.70308 4.84066C3.70329 4.98858 3.73268 5.135 3.78957 5.27154C3.84646 5.40808 3.92974 5.53205 4.03462 5.63635L5.09541 6.69715Z"
												fill="currentColor"></path>
										</svg>
									</span>
									<!--end::Svg Icon-->
									<!--begin::Svg Icon | path: icons/duotune/general/gen061.svg-->
									<span class="svg-icon theme-dark-show svg-icon-2">
										<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
											xmlns="http://www.w3.org/2000/svg">
											<path
												d="M19.0647 5.43757C19.3421 5.43757 19.567 5.21271 19.567 4.93534C19.567 4.65796 19.3421 4.43311 19.0647 4.43311C18.7874 4.43311 18.5625 4.65796 18.5625 4.93534C18.5625 5.21271 18.7874 5.43757 19.0647 5.43757Z"
												fill="currentColor"></path>
											<path
												d="M20.0692 9.48884C20.3466 9.48884 20.5714 9.26398 20.5714 8.98661C20.5714 8.70923 20.3466 8.48438 20.0692 8.48438C19.7918 8.48438 19.567 8.70923 19.567 8.98661C19.567 9.26398 19.7918 9.48884 20.0692 9.48884Z"
												fill="currentColor"></path>
											<path
												d="M12.0335 20.5714C15.6943 20.5714 18.9426 18.2053 20.1168 14.7338C20.1884 14.5225 20.1114 14.289 19.9284 14.161C19.746 14.034 19.5003 14.0418 19.3257 14.1821C18.2432 15.0546 16.9371 15.5156 15.5491 15.5156C12.2257 15.5156 9.48884 12.8122 9.48884 9.48886C9.48884 7.41079 10.5773 5.47137 12.3449 4.35752C12.5342 4.23832 12.6 4.00733 12.5377 3.79251C12.4759 3.57768 12.2571 3.42859 12.0335 3.42859C7.32556 3.42859 3.42857 7.29209 3.42857 12C3.42857 16.7079 7.32556 20.5714 12.0335 20.5714Z"
												fill="currentColor"></path>
											<path
												d="M13.0379 7.47998C13.8688 7.47998 14.5446 8.15585 14.5446 8.98668C14.5446 9.26428 14.7693 9.48891 15.0469 9.48891C15.3245 9.48891 15.5491 9.26428 15.5491 8.98668C15.5491 8.15585 16.225 7.47998 17.0558 7.47998C17.3334 7.47998 17.558 7.25535 17.558 6.97775C17.558 6.70015 17.3334 6.47552 17.0558 6.47552C16.225 6.47552 15.5491 5.76616 15.5491 4.93534C15.5491 4.65774 15.3245 4.43311 15.0469 4.43311C14.7693 4.43311 14.5446 4.65774 14.5446 4.93534C14.5446 5.76616 13.8688 6.47552 13.0379 6.47552C12.7603 6.47552 12.5357 6.70015 12.5357 6.97775C12.5357 7.25535 12.7603 7.47998 13.0379 7.47998Z"
												fill="currentColor"></path>
										</svg>
									</span>
									<!--end::Svg Icon-->
								</a>
								<!--begin::Menu toggle-->
								<!--begin::Menu-->
								<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-title-gray-700 menu-icon-muted menu-active-bg menu-state-color fw-semibold py-4 fs-base w-150px"
									data-kt-menu="true" data-kt-element="theme-mode-menu" style="">
									<!--begin::Menu item-->
									<div class="menu-item px-3 my-0">
										<a href="#" class="menu-link px-3 py-2 active" data-kt-element="mode"
											data-kt-value="light">
											<span class="menu-icon" data-kt-element="icon">
												<!--begin::Svg Icon | path: icons/duotune/general/gen060.svg-->
												<span class="svg-icon svg-icon-3">
													<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
														xmlns="http://www.w3.org/2000/svg">
														<path
															d="M11.9905 5.62598C10.7293 5.62574 9.49646 5.9995 8.44775 6.69997C7.39903 7.40045 6.58159 8.39619 6.09881 9.56126C5.61603 10.7263 5.48958 12.0084 5.73547 13.2453C5.98135 14.4823 6.58852 15.6185 7.48019 16.5104C8.37186 17.4022 9.50798 18.0096 10.7449 18.2557C11.9818 18.5019 13.2639 18.3757 14.429 17.8931C15.5942 17.4106 16.5901 16.5933 17.2908 15.5448C17.9915 14.4962 18.3655 13.2634 18.3655 12.0023C18.3637 10.3119 17.6916 8.69129 16.4964 7.49593C15.3013 6.30056 13.6808 5.62806 11.9905 5.62598Z"
															fill="currentColor"></path>
														<path
															d="M22.1258 10.8771H20.627C20.3286 10.8771 20.0424 10.9956 19.8314 11.2066C19.6204 11.4176 19.5018 11.7038 19.5018 12.0023C19.5018 12.3007 19.6204 12.5869 19.8314 12.7979C20.0424 13.0089 20.3286 13.1274 20.627 13.1274H22.1258C22.4242 13.1274 22.7104 13.0089 22.9214 12.7979C23.1324 12.5869 23.2509 12.3007 23.2509 12.0023C23.2509 11.7038 23.1324 11.4176 22.9214 11.2066C22.7104 10.9956 22.4242 10.8771 22.1258 10.8771Z"
															fill="currentColor"></path>
														<path
															d="M11.9905 19.4995C11.6923 19.5 11.4064 19.6187 11.1956 19.8296C10.9848 20.0405 10.8663 20.3265 10.866 20.6247V22.1249C10.866 22.4231 10.9845 22.7091 11.1953 22.9199C11.4062 23.1308 11.6922 23.2492 11.9904 23.2492C12.2886 23.2492 12.5746 23.1308 12.7854 22.9199C12.9963 22.7091 13.1147 22.4231 13.1147 22.1249V20.6247C13.1145 20.3265 12.996 20.0406 12.7853 19.8296C12.5745 19.6187 12.2887 19.5 11.9905 19.4995Z"
															fill="currentColor"></path>
														<path
															d="M4.49743 12.0023C4.49718 11.704 4.37865 11.4181 4.16785 11.2072C3.95705 10.9962 3.67119 10.8775 3.37298 10.8771H1.87445C1.57603 10.8771 1.28984 10.9956 1.07883 11.2066C0.867812 11.4176 0.749266 11.7038 0.749266 12.0023C0.749266 12.3007 0.867812 12.5869 1.07883 12.7979C1.28984 13.0089 1.57603 13.1274 1.87445 13.1274H3.37299C3.6712 13.127 3.95706 13.0083 4.16785 12.7973C4.37865 12.5864 4.49718 12.3005 4.49743 12.0023Z"
															fill="currentColor"></path>
														<path
															d="M11.9905 4.50058C12.2887 4.50012 12.5745 4.38141 12.7853 4.17048C12.9961 3.95954 13.1147 3.67361 13.1149 3.3754V1.87521C13.1149 1.57701 12.9965 1.29103 12.7856 1.08017C12.5748 0.869313 12.2888 0.750854 11.9906 0.750854C11.6924 0.750854 11.4064 0.869313 11.1955 1.08017C10.9847 1.29103 10.8662 1.57701 10.8662 1.87521V3.3754C10.8664 3.67359 10.9849 3.95952 11.1957 4.17046C11.4065 4.3814 11.6923 4.50012 11.9905 4.50058Z"
															fill="currentColor"></path>
														<path
															d="M18.8857 6.6972L19.9465 5.63642C20.0512 5.53209 20.1343 5.40813 20.1911 5.27163C20.2479 5.13513 20.2772 4.98877 20.2774 4.84093C20.2775 4.69309 20.2485 4.54667 20.192 4.41006C20.1355 4.27344 20.0526 4.14932 19.948 4.04478C19.8435 3.94024 19.7194 3.85734 19.5828 3.80083C19.4462 3.74432 19.2997 3.71531 19.1519 3.71545C19.0041 3.7156 18.8577 3.7449 18.7212 3.80167C18.5847 3.85845 18.4607 3.94159 18.3564 4.04633L17.2956 5.10714C17.1909 5.21147 17.1077 5.33543 17.0509 5.47194C16.9942 5.60844 16.9649 5.7548 16.9647 5.90264C16.9646 6.05048 16.9936 6.19689 17.0501 6.33351C17.1066 6.47012 17.1895 6.59425 17.294 6.69878C17.3986 6.80332 17.5227 6.88621 17.6593 6.94272C17.7959 6.99923 17.9424 7.02824 18.0902 7.02809C18.238 7.02795 18.3844 6.99865 18.5209 6.94187C18.6574 6.88509 18.7814 6.80195 18.8857 6.6972Z"
															fill="currentColor"></path>
														<path
															d="M18.8855 17.3073C18.7812 17.2026 18.6572 17.1195 18.5207 17.0627C18.3843 17.006 18.2379 16.9767 18.0901 16.9766C17.9423 16.9764 17.7959 17.0055 17.6593 17.062C17.5227 17.1185 17.3986 17.2014 17.2941 17.3059C17.1895 17.4104 17.1067 17.5345 17.0501 17.6711C16.9936 17.8077 16.9646 17.9541 16.9648 18.1019C16.9649 18.2497 16.9942 18.3961 17.0509 18.5326C17.1077 18.6691 17.1908 18.793 17.2955 18.8974L18.3563 19.9582C18.4606 20.0629 18.5846 20.146 18.721 20.2027C18.8575 20.2595 19.0039 20.2887 19.1517 20.2889C19.2995 20.289 19.4459 20.26 19.5825 20.2035C19.7191 20.147 19.8432 20.0641 19.9477 19.9595C20.0523 19.855 20.1351 19.7309 20.1916 19.5943C20.2482 19.4577 20.2772 19.3113 20.277 19.1635C20.2769 19.0157 20.2476 18.8694 20.1909 18.7329C20.1341 18.5964 20.051 18.4724 19.9463 18.3681L18.8855 17.3073Z"
															fill="currentColor"></path>
														<path
															d="M5.09528 17.3072L4.0345 18.368C3.92972 18.4723 3.84655 18.5963 3.78974 18.7328C3.73294 18.8693 3.70362 19.0156 3.70346 19.1635C3.7033 19.3114 3.7323 19.4578 3.78881 19.5944C3.84532 19.7311 3.92822 19.8552 4.03277 19.9598C4.13732 20.0643 4.26147 20.1472 4.3981 20.2037C4.53473 20.2602 4.68117 20.2892 4.82902 20.2891C4.97688 20.2889 5.12325 20.2596 5.25976 20.2028C5.39627 20.146 5.52024 20.0628 5.62456 19.958L6.68536 18.8973C6.79007 18.7929 6.87318 18.6689 6.92993 18.5325C6.98667 18.396 7.01595 18.2496 7.01608 18.1018C7.01621 17.954 6.98719 17.8076 6.93068 17.671C6.87417 17.5344 6.79129 17.4103 6.68676 17.3058C6.58224 17.2012 6.45813 17.1183 6.32153 17.0618C6.18494 17.0053 6.03855 16.9763 5.89073 16.9764C5.74291 16.9766 5.59657 17.0058 5.46007 17.0626C5.32358 17.1193 5.19962 17.2024 5.09528 17.3072Z"
															fill="currentColor"></path>
														<path
															d="M5.09541 6.69715C5.19979 6.8017 5.32374 6.88466 5.4602 6.94128C5.59665 6.9979 5.74292 7.02708 5.89065 7.02714C6.03839 7.0272 6.18469 6.99815 6.32119 6.94164C6.45769 6.88514 6.58171 6.80228 6.68618 6.69782C6.79064 6.59336 6.87349 6.46933 6.93 6.33283C6.9865 6.19633 7.01556 6.05003 7.01549 5.9023C7.01543 5.75457 6.98625 5.60829 6.92963 5.47184C6.87301 5.33539 6.79005 5.21143 6.6855 5.10706L5.6247 4.04626C5.5204 3.94137 5.39643 3.8581 5.25989 3.80121C5.12335 3.74432 4.97692 3.71493 4.82901 3.71472C4.68109 3.71452 4.53458 3.7435 4.39789 3.80001C4.26119 3.85652 4.13699 3.93945 4.03239 4.04404C3.9278 4.14864 3.84487 4.27284 3.78836 4.40954C3.73185 4.54624 3.70287 4.69274 3.70308 4.84066C3.70329 4.98858 3.73268 5.135 3.78957 5.27154C3.84646 5.40808 3.92974 5.53205 4.03462 5.63635L5.09541 6.69715Z"
															fill="currentColor"></path>
													</svg>
												</span>
												<!--end::Svg Icon-->
											</span>
											<span class="menu-title">Light</span>
										</a>
									</div>
									<!--end::Menu item-->
									<!--begin::Menu item-->
									<div class="menu-item px-3 my-0">
										<a href="#" class="menu-link px-3 py-2" data-kt-element="mode"
											data-kt-value="dark">
											<span class="menu-icon" data-kt-element="icon">
												<!--begin::Svg Icon | path: icons/duotune/general/gen061.svg-->
												<span class="svg-icon svg-icon-3">
													<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
														xmlns="http://www.w3.org/2000/svg">
														<path
															d="M19.0647 5.43757C19.3421 5.43757 19.567 5.21271 19.567 4.93534C19.567 4.65796 19.3421 4.43311 19.0647 4.43311C18.7874 4.43311 18.5625 4.65796 18.5625 4.93534C18.5625 5.21271 18.7874 5.43757 19.0647 5.43757Z"
															fill="currentColor"></path>
														<path
															d="M20.0692 9.48884C20.3466 9.48884 20.5714 9.26398 20.5714 8.98661C20.5714 8.70923 20.3466 8.48438 20.0692 8.48438C19.7918 8.48438 19.567 8.70923 19.567 8.98661C19.567 9.26398 19.7918 9.48884 20.0692 9.48884Z"
															fill="currentColor"></path>
														<path
															d="M12.0335 20.5714C15.6943 20.5714 18.9426 18.2053 20.1168 14.7338C20.1884 14.5225 20.1114 14.289 19.9284 14.161C19.746 14.034 19.5003 14.0418 19.3257 14.1821C18.2432 15.0546 16.9371 15.5156 15.5491 15.5156C12.2257 15.5156 9.48884 12.8122 9.48884 9.48886C9.48884 7.41079 10.5773 5.47137 12.3449 4.35752C12.5342 4.23832 12.6 4.00733 12.5377 3.79251C12.4759 3.57768 12.2571 3.42859 12.0335 3.42859C7.32556 3.42859 3.42857 7.29209 3.42857 12C3.42857 16.7079 7.32556 20.5714 12.0335 20.5714Z"
															fill="currentColor"></path>
														<path
															d="M13.0379 7.47998C13.8688 7.47998 14.5446 8.15585 14.5446 8.98668C14.5446 9.26428 14.7693 9.48891 15.0469 9.48891C15.3245 9.48891 15.5491 9.26428 15.5491 8.98668C15.5491 8.15585 16.225 7.47998 17.0558 7.47998C17.3334 7.47998 17.558 7.25535 17.558 6.97775C17.558 6.70015 17.3334 6.47552 17.0558 6.47552C16.225 6.47552 15.5491 5.76616 15.5491 4.93534C15.5491 4.65774 15.3245 4.43311 15.0469 4.43311C14.7693 4.43311 14.5446 4.65774 14.5446 4.93534C14.5446 5.76616 13.8688 6.47552 13.0379 6.47552C12.7603 6.47552 12.5357 6.70015 12.5357 6.97775C12.5357 7.25535 12.7603 7.47998 13.0379 7.47998Z"
															fill="currentColor"></path>
													</svg>
												</span>
												<!--end::Svg Icon-->
											</span>
											<span class="menu-title">Dark</span>
										</a>
									</div>
									<!--end::Menu item-->
									<!--begin::Menu item-->
									<div class="menu-item px-3 my-0">
										<a href="#" class="menu-link px-3 py-2" data-kt-element="mode"
											data-kt-value="system">
											<span class="menu-icon" data-kt-element="icon">
												<!--begin::Svg Icon | path: icons/duotune/general/gen062.svg-->
												<span class="svg-icon svg-icon-3">
													<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
														xmlns="http://www.w3.org/2000/svg">
														<path fill-rule="evenodd" clip-rule="evenodd"
															d="M1.34375 3.9463V15.2178C1.34375 16.119 2.08105 16.8563 2.98219 16.8563H8.65093V19.4594H6.15702C5.38853 19.4594 4.75981 19.9617 4.75981 20.5757V21.6921H19.2403V20.5757C19.2403 19.9617 18.6116 19.4594 17.8431 19.4594H15.3492V16.8563H21.0179C21.919 16.8563 22.6562 16.119 22.6562 15.2178V3.9463C22.6562 3.04516 21.9189 2.30786 21.0179 2.30786H2.98219C2.08105 2.30786 1.34375 3.04516 1.34375 3.9463ZM12.9034 9.9016C13.241 9.98792 13.5597 10.1216 13.852 10.2949L15.0393 9.4353L15.9893 10.3853L15.1297 11.5727C15.303 11.865 15.4366 12.1837 15.523 12.5212L16.97 12.7528V13.4089H13.9851C13.9766 12.3198 13.0912 11.4394 12 11.4394C10.9089 11.4394 10.0235 12.3198 10.015 13.4089H7.03006V12.7528L8.47712 12.5211C8.56345 12.1836 8.69703 11.8649 8.87037 11.5727L8.0107 10.3853L8.96078 9.4353L10.148 10.2949C10.4404 10.1215 10.759 9.98788 11.0966 9.9016L11.3282 8.45467H12.6718L12.9034 9.9016ZM16.1353 7.93758C15.6779 7.93758 15.3071 7.56681 15.3071 7.1094C15.3071 6.652 15.6779 6.28122 16.1353 6.28122C16.5926 6.28122 16.9634 6.652 16.9634 7.1094C16.9634 7.56681 16.5926 7.93758 16.1353 7.93758ZM2.71385 14.0964V3.90518C2.71385 3.78023 2.81612 3.67796 2.94107 3.67796H21.0589C21.1839 3.67796 21.2861 3.78023 21.2861 3.90518V14.0964C15.0954 14.0964 8.90462 14.0964 2.71385 14.0964Z"
															fill="currentColor"></path>
													</svg>
												</span>
												<!--end::Svg Icon-->
											</span>
											<span class="menu-title">System</span>
										</a>
									</div>
									<!--end::Menu item-->
								</div>
								<!--end::Menu-->
							</div>
							<!--end::Theme mode-->

							<div class="app-navbar-item ms-1 ms-md-3" id="kt_header_user_menu_toggle">
								<!--begin::Menu wrapper-->
								<div class="cursor-pointer symbol symbol-30px symbol-md-40px"
									data-kt-menu-trigger="{default: 'click', lg: 'hover'}" data-kt-menu-attach="parent"
									data-kt-menu-placement="bottom-end">
									<img src="assets/media/profile.png" alt="user">
								</div>
								<!--begin::User account menu-->
								<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-color fw-semibold py-4 fs-6 w-275px"
									data-kt-menu="true" style="">
									<!--begin::Menu item-->
									<div class="menu-item px-3">
										<div class="menu-content d-flex align-items-center px-3">
											<!--begin::Avatar-->
											<div class="symbol symbol-50px me-5">
												<img alt="Logo" src="assets/media/profile.png">
											</div>
											<!--end::Avatar-->
											<!--begin::Username-->
											<div class="d-flex flex-column">
												<div class="fw-bold d-flex align-items-center fs-5" id="fullname">
													<span
														class="badge badge-light-success fw-bold fs-8 px-2 py-1 ms-2">Pro</span>
												</div>
												<a href="#"
													class="fw-semibold text-muted text-hover-primary fs-7" id="email"></a>
											</div>
											<!--end::Username-->
										</div>
									</div>
									<!--end::Menu item-->
									<!--begin::Menu separator-->
									<div class="separator my-2"></div>
									<!--end::Menu separator-->
									<!--begin::Menu item-->
									<div class="menu-item px-5">
										<a href="../../demo1/dist/account/overview.html" class="menu-link px-5">My
											Profile</a>
									</div>
									<div class="menu-item px-5" data-kt-menu-trigger="{default: 'click', lg: 'hover'}"
										data-kt-menu-placement="left-start" data-kt-menu-offset="-15px, 0">
										<a href="#" class="menu-link px-5">
											<span class="menu-title position-relative">Language
												<span
													class="fs-8 rounded bg-light px-3 py-2 position-absolute translate-middle-y top-50 end-0">English
													<img class="w-15px h-15px rounded-1 ms-2"
														src="assets/media/flags/united-states.svg" alt=""></span></span>
										</a>
										<!--begin::Menu sub-->
										<div class="menu-sub menu-sub-dropdown w-175px py-4">
											<!--begin::Menu item-->
											<div class="menu-item px-3">
												<a href="../../demo1/dist/account/settings.html"
													class="menu-link d-flex px-5 active">
													<span class="symbol symbol-20px me-4">
														<img class="rounded-1"
															src="assets/media/flags/united-states.svg" alt="">
													</span>English</a>
											</div>
											<!--end::Menu item-->
										</div>
										<!--end::Menu sub-->
									</div>
									<!--end::Menu item-->
									<!--begin::Menu item-->
									<div class="menu-item px-5 my-1">
										<a href="../../demo1/dist/account/settings.html" class="menu-link px-5">Account
											Settings</a>
									</div>
									<!--end::Menu item-->
									<!--begin::Menu item-->
									<div class="menu-item px-5">
										<a href="assets/php/logout.php"
											class="menu-link px-5">Sign Out</a>
									</div>
									<!--end::Menu item-->
								</div>
								<!--end::User account menu-->
								<!--end::Menu wrapper-->
							</div>
							<!--end::User menu-->
							<!--begin::Header menu toggle-->

							<!--end::Header menu toggle-->
						</div>
						<!--end::Navbar-->
					</div>
					<!--end::Header wrapper-->
				</div>
				<!--end::Header container-->
			</div>
			<!--end::Header-->
			<!--begin::Wrapper-->
			<div class="app-wrapper flex-column flex-row-fluid" id="kt_app_wrapper">
				<!--begin::Sidebar-->
				<div id="kt_app_sidebar" class="app-sidebar flex-column" data-kt-drawer="true"
					data-kt-drawer-name="app-sidebar" data-kt-drawer-activate="{default: true, lg: false}"
					data-kt-drawer-overlay="true" data-kt-drawer-width="225px" data-kt-drawer-direction="start"
					data-kt-drawer-toggle="#kt_app_sidebar_mobile_toggle" style="">
					<!--begin::Logo-->
					<div class="app-sidebar-logo px-6" id="kt_app_sidebar_logo">
						<!--begin::Logo image-->
						<a href="../../demo1/dist/index.html">
							<img alt="Logo" src="assets/media/logo-white.png" class="h-45px app-sidebar-logo-default">
							<img alt="Logo" src="assets/media/mini-logo.png" class="h-40px app-sidebar-logo-minimize">
						</a>
						<!--end::Logo image-->
						<!--begin::Sidebar toggle-->
						<div id="kt_app_sidebar_toggle"
							class="app-sidebar-toggle btn btn-icon btn-shadow btn-sm btn-color-muted btn-active-color-primary body-bg h-30px w-30px position-absolute top-50 start-100 translate-middle rotate"
							data-kt-toggle="true" data-kt-toggle-state="active" data-kt-toggle-target="body"
							data-kt-toggle-name="app-sidebar-minimize">
							<!--begin::Svg Icon | path: icons/duotune/arrows/arr079.svg-->
							<span class="svg-icon svg-icon-2 rotate-180">
								<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
									xmlns="http://www.w3.org/2000/svg">
									<path opacity="0.5"
										d="M14.2657 11.4343L18.45 7.25C18.8642 6.83579 18.8642 6.16421 18.45 5.75C18.0358 5.33579 17.3642 5.33579 16.95 5.75L11.4071 11.2929C11.0166 11.6834 11.0166 12.3166 11.4071 12.7071L16.95 18.25C17.3642 18.6642 18.0358 18.6642 18.45 18.25C18.8642 17.8358 18.8642 17.1642 18.45 16.75L14.2657 12.5657C13.9533 12.2533 13.9533 11.7467 14.2657 11.4343Z"
										fill="currentColor"></path>
									<path
										d="M8.2657 11.4343L12.45 7.25C12.8642 6.83579 12.8642 6.16421 12.45 5.75C12.0358 5.33579 11.3642 5.33579 10.95 5.75L5.40712 11.2929C5.01659 11.6834 5.01659 12.3166 5.40712 12.7071L10.95 18.25C11.3642 18.6642 12.0358 18.6642 12.45 18.25C12.8642 17.8358 12.8642 17.1642 12.45 16.75L8.2657 12.5657C7.95328 12.2533 7.95328 11.7467 8.2657 11.4343Z"
										fill="currentColor"></path>
								</svg>
							</span>
							<!--end::Svg Icon-->
						</div>
						<!--end::Sidebar toggle-->
					</div>
					<!--end::Logo-->
					<!--begin::sidebar menu-->
					<div class="app-sidebar-menu overflow-hidden flex-column-fluid">
						<!--begin::Menu wrapper-->
						<div id="kt_app_sidebar_menu_wrapper" class="app-sidebar-wrapper hover-scroll-overlay-y my-5"
							data-kt-scroll="true" data-kt-scroll-activate="true" data-kt-scroll-height="auto"
							data-kt-scroll-dependencies="#kt_app_sidebar_logo, #kt_app_sidebar_footer"
							data-kt-scroll-wrappers="#kt_app_sidebar_menu" data-kt-scroll-offset="5px"
							data-kt-scroll-save-state="true" style="height: 189px;">
							<!--begin::Menu-->
							<div class="menu menu-column menu-rounded menu-sub-indention px-3" id="#kt_app_sidebar_menu"
								data-kt-menu="true" data-kt-menu-expand="false">
								<div class="menu-item here show menu-accordion hiding">
									<a href="index.php" class="menu-link">
										<span class="menu-icon">
											<span class="svg-icon svg-icon-2">
												<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
													xmlns="http://www.w3.org/2000/svg">
													<rect x="2" y="2" width="9" height="9" rx="2" fill="currentColor">
													</rect>
													<rect opacity="0.3" x="13" y="2" width="9" height="9" rx="2"
														fill="currentColor"></rect>
													<rect opacity="0.3" x="13" y="13" width="9" height="9" rx="2"
														fill="currentColor"></rect>
													<rect opacity="0.3" x="2" y="13" width="9" height="9" rx="2"
														fill="currentColor"></rect>
												</svg>
											</span>
											<!--end::Svg Icon-->
										</span>
										<span class="menu-title">Dashboards</span>

									</a>
								</div>
								<div data-kt-menu-trigger="click" class="menu-item menu-accordion">
									<!--begin:Menu link-->
									<span class="menu-link">
										<span class="menu-icon">
											<!--begin::Svg Icon | path: icons/duotune/communication/com005.svg-->
											<span class="svg-icon svg-icon-2">
												<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
													xmlns="http://www.w3.org/2000/svg">
													<path
														d="M20 14H18V10H20C20.6 10 21 10.4 21 11V13C21 13.6 20.6 14 20 14ZM21 19V17C21 16.4 20.6 16 20 16H18V20H20C20.6 20 21 19.6 21 19ZM21 7V5C21 4.4 20.6 4 20 4H18V8H20C20.6 8 21 7.6 21 7Z"
														fill="currentColor"></path>
													<path opacity="0.3"
														d="M17 22H3C2.4 22 2 21.6 2 21V3C2 2.4 2.4 2 3 2H17C17.6 2 18 2.4 18 3V21C18 21.6 17.6 22 17 22ZM10 7C8.9 7 8 7.9 8 9C8 10.1 8.9 11 10 11C11.1 11 12 10.1 12 9C12 7.9 11.1 7 10 7ZM13.3 16C14 16 14.5 15.3 14.3 14.7C13.7 13.2 12 12 10.1 12C8.10001 12 6.49999 13.1 5.89999 14.7C5.59999 15.3 6.19999 16 7.39999 16H13.3Z"
														fill="currentColor"></path>
												</svg>
											</span>
											<!--end::Svg Icon-->
										</span>
										<span class="menu-title">Support</span>

									</span>
									<!--end:Menu link-->



								</div>
								<div class="menu-item menu-accordion">
									<!--begin:Menu link-->
									<a class="menu-link" href="transfer.php">
										<span class="menu-icon">
											<!--begin::Svg Icon | path: icons/duotune/finance/fin001.svg-->
											<span class="svg-icon svg-icon-2">
												<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
													xmlns="http://www.w3.org/2000/svg">
													<path
														d="M20 19.725V18.725C20 18.125 19.6 17.725 19 17.725H5C4.4 17.725 4 18.125 4 18.725V19.725H3C2.4 19.725 2 20.125 2 20.725V21.725H22V20.725C22 20.125 21.6 19.725 21 19.725H20Z"
														fill="currentColor"></path>
													<path opacity="0.3"
														d="M22 6.725V7.725C22 8.325 21.6 8.725 21 8.725H18C18.6 8.725 19 9.125 19 9.725C19 10.325 18.6 10.725 18 10.725V15.725C18.6 15.725 19 16.125 19 16.725V17.725H15V16.725C15 16.125 15.4 15.725 16 15.725V10.725C15.4 10.725 15 10.325 15 9.725C15 9.125 15.4 8.725 16 8.725H13C13.6 8.725 14 9.125 14 9.725C14 10.325 13.6 10.725 13 10.725V15.725C13.6 15.725 14 16.125 14 16.725V17.725H10V16.725C10 16.125 10.4 15.725 11 15.725V10.725C10.4 10.725 10 10.325 10 9.725C10 9.125 10.4 8.725 11 8.725H8C8.6 8.725 9 9.125 9 9.725C9 10.325 8.6 10.725 8 10.725V15.725C8.6 15.725 9 16.125 9 16.725V17.725H5V16.725C5 16.125 5.4 15.725 6 15.725V10.725C5.4 10.725 5 10.325 5 9.725C5 9.125 5.4 8.725 6 8.725H3C2.4 8.725 2 8.325 2 7.725V6.725L11 2.225C11.6 1.925 12.4 1.925 13.1 2.225L22 6.725ZM12 3.725C11.2 3.725 10.5 4.425 10.5 5.225C10.5 6.025 11.2 6.725 12 6.725C12.8 6.725 13.5 6.025 13.5 5.225C13.5 4.425 12.8 3.725 12 3.725Z"
														fill="currentColor"></path>
												</svg>
											</span>
											<!--end::Svg Icon-->
										</span>
										<span class="menu-title">Transfer</span>

									</a>
								</div>
								<div data-kt-menu-trigger="click" class="menu-item menu-accordion">
									<!--begin:Menu link-->
									<span class="menu-link" data-bs-toggle="modal" data-bs-target="#kt_modal_new_target">
										<span class="menu-icon">
											<!--begin::Svg Icon | path: icons/duotune/graphs/gra010.svg-->
											<span class="svg-icon svg-icon-2">
												<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
													xmlns="http://www.w3.org/2000/svg">
													<path
														d="M13.0021 10.9128V3.01281C13.0021 2.41281 13.5021 1.91281 14.1021 2.01281C16.1021 2.21281 17.9021 3.11284 19.3021 4.61284C20.7021 6.01284 21.6021 7.91285 21.9021 9.81285C22.0021 10.4129 21.5021 10.9128 20.9021 10.9128H13.0021Z"
														fill="currentColor"></path>
													<path opacity="0.3"
														d="M11.0021 13.7128V4.91283C11.0021 4.31283 10.5021 3.81283 9.90208 3.91283C5.40208 4.51283 1.90209 8.41284 2.00209 13.1128C2.10209 18.0128 6.40208 22.0128 11.3021 21.9128C13.1021 21.8128 14.7021 21.3128 16.0021 20.4128C16.5021 20.1128 16.6021 19.3128 16.1021 18.9128L11.0021 13.7128Z"
														fill="currentColor"></path>
													<path opacity="0.3"
														d="M21.9021 14.0128C21.7021 15.6128 21.1021 17.1128 20.1021 18.4128C19.7021 18.9128 19.0021 18.9128 18.6021 18.5128L13.0021 12.9128H20.9021C21.5021 12.9128 22.0021 13.4128 21.9021 14.0128Z"
														fill="currentColor"></path>
												</svg>
											</span>
											<!--end::Svg Icon-->
										</span>
										<span class="menu-title">Deposit</span>

									</span>
									<!--end:Menu link-->



								</div>
								<!--end:Menu item-->







								<div class="menu-item menu-accordion">
									<!--begin:Menu link-->
									<a class="menu-link" href="history.php">
										<span class="menu-icon">
											<!--begin::Svg Icon | path: icons/duotune/art/art002.svg-->
											<span class="svg-icon svg-icon-2">
												<svg width="24" height="25" viewBox="0 0 24 25" fill="none"
													xmlns="http://www.w3.org/2000/svg">
													<path opacity="0.3"
														d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z"
														fill="currentColor"></path>
													<path
														d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z"
														fill="currentColor"></path>
												</svg>
											</span>
											<!--end::Svg Icon-->
										</span>
										<span class="menu-title">History</span>
									</a>
								</div>
								<div class="menu-item menu-accordion">
									<!--begin:Menu link-->
									<a class="menu-link" href="assets/php/logout.php">
										<span class="menu-icon">
											<!--begin::Svg Icon | path: icons/duotune/layouts/lay008.svg-->
											<span class="svg-icon svg-icon-2">
												<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
													xmlns="http://www.w3.org/2000/svg">
													<path
														d="M20 7H3C2.4 7 2 6.6 2 6V3C2 2.4 2.4 2 3 2H20C20.6 2 21 2.4 21 3V6C21 6.6 20.6 7 20 7ZM7 9H3C2.4 9 2 9.4 2 10V20C2 20.6 2.4 21 3 21H7C7.6 21 8 20.6 8 20V10C8 9.4 7.6 9 7 9Z"
														fill="currentColor"></path>
													<path opacity="0.3"
														d="M20 21H11C10.4 21 10 20.6 10 20V10C10 9.4 10.4 9 11 9H20C20.6 9 21 9.4 21 10V20C21 20.6 20.6 21 20 21Z"
														fill="currentColor"></path>
												</svg>
											</span>
											<!--end::Svg Icon-->
										</span>
										<span class="menu-title">Log Out</span>

									</span>



									<!--end:Menu sub-->
								</div>
								<!--end:Menu item-->
								<!--begin:Menu item-->






























































							</div>
							<!--end::Menu-->
						</div>
						<!--end::Menu wrapper-->
					</div>
					<!--end::sidebar menu-->
					<!--begin::Footer-->
					<div class="app-sidebar-footer flex-column-auto pt-2 pb-6 px-6" id="kt_app_sidebar_footer">
						<a href="account/overview.html"
							class="btn btn-flex flex-center btn-custom btn-primary overflow-hidden text-nowrap px-0 h-40px w-100"
							data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss-="click"
							data-kt-initialized="1">
							<span class="btn-label">My Profile</span>
							<span class="svg-icon btn-icon svg-icon-2 m-0">
								<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
									xmlns="http://www.w3.org/2000/svg">
									<path opacity="0.3"
										d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22ZM12.5 18C12.5 17.4 12.6 17.5 12 17.5H8.5C7.9 17.5 8 17.4 8 18C8 18.6 7.9 18.5 8.5 18.5L12 18C12.6 18 12.5 18.6 12.5 18ZM16.5 13C16.5 12.4 16.6 12.5 16 12.5H8.5C7.9 12.5 8 12.4 8 13C8 13.6 7.9 13.5 8.5 13.5H15.5C16.1 13.5 16.5 13.6 16.5 13ZM12.5 8C12.5 7.4 12.6 7.5 12 7.5H8C7.4 7.5 7.5 7.4 7.5 8C7.5 8.6 7.4 8.5 8 8.5H12C12.6 8.5 12.5 8.6 12.5 8Z"
										fill="currentColor"></path>
									<rect x="7" y="17" width="6" height="2" rx="1" fill="currentColor"></rect>
									<rect x="7" y="12" width="10" height="2" rx="1" fill="currentColor"></rect>
									<rect x="7" y="7" width="6" height="2" rx="1" fill="currentColor"></rect>
									<path d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z" fill="currentColor"></path>
								</svg>
							</span>
							<!--end::Svg Icon-->
						</a>
					</div>
					<!--end::Footer-->
				</div>
				<!--end::Sidebar-->
				<!--begin::Main-->
				<div class="app-main flex-column flex-row-fluid" id="kt_app_main">
					<!--begin::Content wrapper-->
					<div class="d-flex flex-column flex-column-fluid">
						<!--begin::Toolbar-->
						<div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
							<!--begin::Toolbar container-->
							<div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
								<!--begin::Page title-->
								<div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
									<!--begin::Title-->
									<h1
										class="page-heading d-flex text-dark fw-bold flex-row fs-2 my-0">
										Hi <span id="firstname" class="ms-2"></span></h1>
									<ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
										<li class="breadcrumb-item text-muted">
											<a href="#" class="text-muted text-hover-primary">Welcome back to IsBank</a>
										</li>
									</ul>
									<!--end::Breadcrumb-->
								</div>
								<!--end::Page title-->
								<!--begin::Actions-->
								<div class="d-flex align-items-center gap-2 gap-lg-3">
									<!--begin::Primary button-->
									<a href="#" class="btn btn-sm fw-bold btn-primary" data-bs-toggle="modal"
										data-bs-target="#kt_modal_new_target">Fund Account</a>
									<!--end::Primary button-->
								</div>
								<!--end::Actions-->
							</div>
							<!--end::Toolbar container-->
						</div>
						<div id="kt_app_content" class="app-content flex-column-fluid">
							<div id="kt_app_content_container" class="app-container container-fluid">
								<div class="row g-5 g-xl-10 mb-5 mb-xl-10">
									<!--begin::Col-->
									<div class="col-xl-12">
										<!--begin::Table widget 14-->
										<div class="card card-flush h-md-100">
											<!--begin::Header-->
											<div class="card-header pt-7">
												<!--begin::Title-->
												<h3 class="card-title align-items-start flex-column">
													<span class="card-label fw-bold text-gray-800">Initiate Transfer</span>
												</h3>
												<!--end::Title-->
												<!--begin::Toolbar-->
												<div class="card-toolbar">
													<a href="#"
														class="btn btn-sm btn-light">History</a>
												</div>
												<!--end::Toolbar-->
											</div>
											<div class="card-body pt-6">
                                                <div style="color: red;" id="errorMessage"></div>
                                                <div id="token-success" style="display: none; width:100%; padding: 10px; background-color: rgb(204, 248, 219); border-radius: 10px; color: rgb(0, 142, 87);">Sucessful</div>
                                                <div class="row g-9 mb-8">
                                                    <!--begin::Col-->
                                                    <div class="col-md-6 fv-row fv-plugins-icon-container">
                                                        <label class="required fs-6 fw-semibold mb-2">Bank</label>
                                                        <select id="bank" name="bank" class="form-select form-select-solid"
                                                            data-control="select2" data-hide-search="true" name="target_assign"
                                                            data-select2-id="select2-data-13-g7kh" tabindex="-1" aria-hidden="true"
                                                            data-kt-initialized="1">
                                                            <option value="" disabled="" selected="">--Select One--</option>
                                                            <option value="other">Other</option>
                                                            <option value="Akbank">Akbank</option>
                                                            <option value="Arap Türk Bankasi">Arap Türk Bankasi</option>
                                                            <option value="Akbank">Akbank</option>
                                                            <option value="Bank Ekspres">Bank Ekspres</option>
                                                            <option value="Bank Kapital">Bank Kapital</option>
                                                            <option value="Bayindirbank">Bayindirbank</option>
                                                            <option value="Citibank Turkey">Citibank Turkey</option>
                                                            <option value="Demirbank">Demirbank</option>
                                                            <option value="Disbank">Disbank</option>
                                                            <option value="Egebank">Egebank</option>
                                                            <option value="Emlakbank">Emlakbank</option>
                                                            <option value="Fibank (Fiba Bank)">Fibank (Fiba Bank)</option>
                                                            <option value="Finansbank">Finansbank</option>
                                                            <option value="Garanti Bank">Garanti Bank</option>
                                                            <option value="Halkbank">Halkbank</option>
                                                            <option value="Interbank">Interbank</option>
                                                            <option value="Takasbank">Takasbank</option>
                                                            <option value="Tarisbank">Tarisbank</option>
                                                            <option value="Türk Ticaret Bankasi">Türk Ticaret Bankasi</option>
                                                            <option value="Turkish Bank">Turkish Bank</option>
                                                            <option value="Vakiflar Bankasi – Vakifbank">Vakiflar Bankasi – Vakifbank</option>
                                                            <option value="other">Other</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6 fv-row">
                                                        <label class="required fs-6 fw-semibold mb-2">Amount</label>
                                                        <div class="position-relative d-flex align-items-center">
                                                            <input type="number" class="form-control form-control-solid" placeholder="Enter Amount" name="amount2">
                                                        </div>
                                                    </div>
                                                    <!--end::Col-->
                                                </div>
                                                <div class="col-md-12 fv-row mt-10" id="otherBank" style="display: none;">
                                                    <label class="required fs-6 fw-semibold mb-2">Other Bank Name</label>
                                                    <div class="position-relative d-flex align-items-center">
                                                        <input type="text" class="form-control form-control-solid" placeholder="Enter Bank Name" name="b_name">
                                                    </div>
                                                </div>
                                                <input type="hidden" id="email2" name="email2" value>
                                                <div class="col-md-12 fv-row mt-10">
                                                    <label class="required fs-6 fw-semibold mb-2">Beneficiary's Name</label>
                                                    <div class="position-relative d-flex align-items-center">
                                                        <input type="text" class="form-control form-control-solid" placeholder="Enter Name" name="b_name">
                                                    </div>
                                                </div>
                                                <div class="col-md-12 fv-row mt-10">
                                                    <label class="required fs-6 fw-semibold mb-2">Account Number</label>
                                                    <div class="position-relative d-flex align-items-center">
                                                        <input type="number" class="form-control form-control-solid" placeholder="Account Number" name="a_number">
                                                    </div>
                                                </div>
                                                <button type="button" onclick="transfer()" class="btn btn-primary mt-10">
                                                    <span class="indicator-label">Initiate Transfer</span>
                                                </button>
											</div>
											<!--end: Card Body-->
										</div>

									</div>
									<!--end::Content container-->
								</div>
							</div>
							<!--end::Content wrapper-->
							<!--begin::Footer-->
							<div id="kt_app_footer" class="app-footer">
								<!--begin::Footer container-->
								<div
									class="app-container container-fluid d-flex flex-column flex-md-row flex-center flex-md-stack py-3">
									<!--begin::Copyright-->
									<div class="text-dark order-2 order-md-1">
										<span class="text-muted fw-semibold me-1">2024©</span>
										<a href="#" target="_blank" class="text-gray-800 text-hover-primary"> Işbank All
											Right
											Reserved</a>
									</div>
									<!--end::Copyright-->
									<!--begin::Menu-->
									<ul class="menu menu-gray-600 menu-hover-primary fw-semibold order-1">
										<li class="menu-item">
											<a href="#" target="_blank" class="menu-link px-2">Contact us</a>
										</li>
									</ul>
									<!--end::Menu-->
								</div>
								<!--end::Footer container-->
							</div>
							<!--end::Footer-->
						</div>
						<!--end:::Main-->
					</div>
					<!--end::Wrapper-->
				</div>
				<!--end::Page-->
			</div>
			<!--end::App-->
			<!--begin::Drawers-->
			<!--begin::Activities drawer-->









			<!--begin::Scrolltop-->
			<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
				<!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
				<span class="svg-icon">
					<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
						<rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)"
							fill="currentColor"></rect>
						<path
							d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z"
							fill="currentColor"></path>
					</svg>
				</span>
				<!--end::Svg Icon-->
			</div>
			<!--end::Scrolltop-->
			<!--begin::Modals-->
			<!--begin::Modal - Upgrade plan-->
			<div class="modal fade" id="kt_modal_upgrade_plan" tabindex="-1" aria-hidden="true">
				<!--begin::Modal dialog-->
				<div class="modal-dialog modal-xl">
					<!--begin::Modal content-->
					<div class="modal-content rounded">
						<!--begin::Modal header-->
						<div class="modal-header justify-content-end border-0 pb-0">
							<!--begin::Close-->
							<div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
								<!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
								<span class="svg-icon svg-icon-1">
									<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
										xmlns="http://www.w3.org/2000/svg">
										<rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
											transform="rotate(-45 6 17.3137)" fill="currentColor"></rect>
										<rect x="7.41422" y="6" width="16" height="2" rx="1"
											transform="rotate(45 7.41422 6)" fill="currentColor"></rect>
									</svg>
								</span>
								<!--end::Svg Icon-->
							</div>
							<!--end::Close-->
						</div>
						<!--end::Modal header-->
						<!--begin::Modal body-->
						<div class="modal-body pt-0 pb-15 px-5 px-xl-20">
							<!--begin::Heading-->
							<div class="mb-13 text-center">
								<h1 class="mb-3">Upgrade a Plan</h1>
								<div class="text-muted fw-semibold fs-5">If you need more info, please check
									<a href="#" class="link-primary fw-bold">Pricing Guidelines</a>.
								</div>
							</div>
							<!--end::Heading-->
							<!--begin::Plans-->
							<div class="d-flex flex-column">
								<!--begin::Nav group-->
								<div class="nav-group nav-group-outline mx-auto" data-kt-buttons="true"
									data-kt-initialized="1">
									<button
										class="btn btn-color-gray-400 btn-active btn-active-secondary px-6 py-3 me-2 active"
										data-kt-plan="month">Monthly</button>
									<button class="btn btn-color-gray-400 btn-active btn-active-secondary px-6 py-3"
										data-kt-plan="annual">Annual</button>
								</div>
								<!--end::Nav group-->
								<!--begin::Row-->
								<div class="row mt-10">
									<!--begin::Col-->
									<div class="col-lg-6 mb-10 mb-lg-0">
										<!--begin::Tabs-->
										<div class="nav flex-column" role="tablist">
											<!--begin::Tab link-->
											<label
												class="nav-link btn btn-outline btn-outline-dashed btn-color-dark btn-active btn-active-primary d-flex flex-stack text-start p-6 active mb-6"
												data-bs-toggle="tab" data-bs-target="#kt_upgrade_plan_startup"
												aria-selected="true" role="tab">
												<!--end::Description-->
												<div class="d-flex align-items-center me-2">
													<!--begin::Radio-->
													<div
														class="form-check form-check-custom form-check-solid form-check-success flex-shrink-0 me-6">
														<input class="form-check-input" type="radio" name="plan"
															checked="checked" value="startup">
													</div>
													<!--end::Radio-->
													<!--begin::Info-->
													<div class="flex-grow-1">
														<div class="d-flex align-items-center fs-2 fw-bold flex-wrap">
															Startup
														</div>
														<div class="fw-semibold opacity-75">Best for startups</div>
													</div>
													<!--end::Info-->
												</div>
												<!--end::Description-->
												<!--begin::Price-->
												<div class="ms-5">
													<span class="mb-2">$</span>
													<span class="fs-3x fw-bold" data-kt-plan-price-month="39"
														data-kt-plan-price-annual="399">39</span>
													<span class="fs-7 opacity-50">/
														<span data-kt-element="period">Mon</span></span>
												</div>
												<!--end::Price-->
											</label>
											<!--end::Tab link-->
											<!--begin::Tab link-->
											<label
												class="nav-link btn btn-outline btn-outline-dashed btn-color-dark btn-active btn-active-primary d-flex flex-stack text-start p-6 mb-6"
												data-bs-toggle="tab" data-bs-target="#kt_upgrade_plan_advanced"
												aria-selected="false" tabindex="-1" role="tab">
												<!--end::Description-->
												<div class="d-flex align-items-center me-2">
													<!--begin::Radio-->
													<div
														class="form-check form-check-custom form-check-solid form-check-success flex-shrink-0 me-6">
														<input class="form-check-input" type="radio" name="plan"
															value="advanced">
													</div>
													<!--end::Radio-->
													<!--begin::Info-->
													<div class="flex-grow-1">
														<div class="d-flex align-items-center fs-2 fw-bold flex-wrap">
															Advanced
														</div>
														<div class="fw-semibold opacity-75">Best for 100+ team size
														</div>
													</div>
													<!--end::Info-->
												</div>
												<!--end::Description-->
												<!--begin::Price-->
												<div class="ms-5">
													<span class="mb-2">$</span>
													<span class="fs-3x fw-bold" data-kt-plan-price-month="339"
														data-kt-plan-price-annual="3399">339</span>
													<span class="fs-7 opacity-50">/
														<span data-kt-element="period">Mon</span></span>
												</div>
												<!--end::Price-->
											</label>
											<!--end::Tab link-->
											<!--begin::Tab link-->
											<label
												class="nav-link btn btn-outline btn-outline-dashed btn-color-dark btn-active btn-active-primary d-flex flex-stack text-start p-6 mb-6"
												data-bs-toggle="tab" data-bs-target="#kt_upgrade_plan_enterprise"
												aria-selected="false" tabindex="-1" role="tab">
												<!--end::Description-->
												<div class="d-flex align-items-center me-2">
													<!--begin::Radio-->
													<div
														class="form-check form-check-custom form-check-solid form-check-success flex-shrink-0 me-6">
														<input class="form-check-input" type="radio" name="plan"
															value="enterprise">
													</div>
													<!--end::Radio-->
													<!--begin::Info-->
													<div class="flex-grow-1">
														<div class="d-flex align-items-center fs-2 fw-bold flex-wrap">
															Enterprise
															<span
																class="badge badge-light-success ms-2 py-2 px-3 fs-7">Popular</span>
														</div>
														<div class="fw-semibold opacity-75">Best value for 1000+ team
														</div>
													</div>
													<!--end::Info-->
												</div>
												<!--end::Description-->
												<!--begin::Price-->
												<div class="ms-5">
													<span class="mb-2">$</span>
													<span class="fs-3x fw-bold" data-kt-plan-price-month="999"
														data-kt-plan-price-annual="9999">999</span>
													<span class="fs-7 opacity-50">/
														<span data-kt-element="period">Mon</span></span>
												</div>
												<!--end::Price-->
											</label>
											<!--end::Tab link-->
											<!--begin::Tab link-->
											<label
												class="nav-link btn btn-outline btn-outline-dashed btn-color-dark btn-active btn-active-primary d-flex flex-stack text-start p-6 mb-6"
												data-bs-toggle="tab" data-bs-target="#kt_upgrade_plan_custom"
												aria-selected="false" tabindex="-1" role="tab">
												<!--end::Description-->
												<div class="d-flex align-items-center me-2">
													<!--begin::Radio-->
													<div
														class="form-check form-check-custom form-check-solid form-check-success flex-shrink-0 me-6">
														<input class="form-check-input" type="radio" name="plan"
															value="custom">
													</div>
													<!--end::Radio-->
													<!--begin::Info-->
													<div class="flex-grow-1">
														<div class="d-flex align-items-center fs-2 fw-bold flex-wrap">
															Custom
														</div>
														<div class="fw-semibold opacity-75">Requet a custom license
														</div>
													</div>
													<!--end::Info-->
												</div>
												<!--end::Description-->
												<!--begin::Price-->
												<div class="ms-5">
													<a href="#" class="btn btn-sm btn-success">Contact Us</a>
												</div>
												<!--end::Price-->
											</label>
											<!--end::Tab link-->
										</div>
										<!--end::Tabs-->
									</div>
									<!--end::Col-->
									<!--begin::Col-->
									<div class="col-lg-6">
										<!--begin::Tab content-->
										<div class="tab-content rounded h-100 bg-light p-10">
											<!--begin::Tab Pane-->
											<div class="tab-pane fade show active" id="kt_upgrade_plan_startup"
												role="tabpanel">
												<!--begin::Heading-->
												<div class="pb-5">
													<h2 class="fw-bold text-dark">What’s in Startup Plan?</h2>
													<div class="text-muted fw-semibold">Optimal for 10+ team size and
														new
														startup</div>
												</div>
												<!--end::Heading-->
												<!--begin::Body-->
												<div class="pt-1">
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-7">
														<span class="fw-semibold fs-5 text-gray-700 flex-grow-1">Up to
															10 Active
															Users</span>
														<!--begin::Svg Icon | path: icons/duotune/general/gen043.svg-->
														<span class="svg-icon svg-icon-1 svg-icon-success">
															<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
																xmlns="http://www.w3.org/2000/svg">
																<rect opacity="0.3" x="2" y="2" width="20" height="20"
																	rx="10" fill="currentColor"></rect>
																<path
																	d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z"
																	fill="currentColor"></path>
															</svg>
														</span>
														<!--end::Svg Icon-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-7">
														<span class="fw-semibold fs-5 text-gray-700 flex-grow-1">Up to
															30
															Project Integrations</span>
														<!--begin::Svg Icon | path: icons/duotune/general/gen043.svg-->
														<span class="svg-icon svg-icon-1 svg-icon-success">
															<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
																xmlns="http://www.w3.org/2000/svg">
																<rect opacity="0.3" x="2" y="2" width="20" height="20"
																	rx="10" fill="currentColor"></rect>
																<path
																	d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z"
																	fill="currentColor"></path>
															</svg>
														</span>
														<!--end::Svg Icon-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-7">
														<span
															class="fw-semibold fs-5 text-gray-700 flex-grow-1">Analytics
															Module</span>
														<!--begin::Svg Icon | path: icons/duotune/general/gen043.svg-->
														<span class="svg-icon svg-icon-1 svg-icon-success">
															<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
																xmlns="http://www.w3.org/2000/svg">
																<rect opacity="0.3" x="2" y="2" width="20" height="20"
																	rx="10" fill="currentColor"></rect>
																<path
																	d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z"
																	fill="currentColor"></path>
															</svg>
														</span>
														<!--end::Svg Icon-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-7">
														<span class="fw-semibold fs-5 text-muted flex-grow-1">Finance
															Module</span>
														<!--begin::Svg Icon | path: icons/duotune/general/gen040.svg-->
														<span class="svg-icon svg-icon-1">
															<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
																xmlns="http://www.w3.org/2000/svg">
																<rect opacity="0.3" x="2" y="2" width="20" height="20"
																	rx="10" fill="currentColor"></rect>
																<rect x="7" y="15.3137" width="12" height="2" rx="1"
																	transform="rotate(-45 7 15.3137)"
																	fill="currentColor"></rect>
																<rect x="8.41422" y="7" width="12" height="2" rx="1"
																	transform="rotate(45 8.41422 7)"
																	fill="currentColor"></rect>
															</svg>
														</span>
														<!--end::Svg Icon-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-7">
														<span class="fw-semibold fs-5 text-muted flex-grow-1">Accounting
															Module</span>
														<!--begin::Svg Icon | path: icons/duotune/general/gen040.svg-->
														<span class="svg-icon svg-icon-1">
															<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
																xmlns="http://www.w3.org/2000/svg">
																<rect opacity="0.3" x="2" y="2" width="20" height="20"
																	rx="10" fill="currentColor"></rect>
																<rect x="7" y="15.3137" width="12" height="2" rx="1"
																	transform="rotate(-45 7 15.3137)"
																	fill="currentColor"></rect>
																<rect x="8.41422" y="7" width="12" height="2" rx="1"
																	transform="rotate(45 8.41422 7)"
																	fill="currentColor"></rect>
															</svg>
														</span>
														<!--end::Svg Icon-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-7">
														<span class="fw-semibold fs-5 text-muted flex-grow-1">Network
															Platform</span>
														<!--begin::Svg Icon | path: icons/duotune/general/gen040.svg-->
														<span class="svg-icon svg-icon-1">
															<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
																xmlns="http://www.w3.org/2000/svg">
																<rect opacity="0.3" x="2" y="2" width="20" height="20"
																	rx="10" fill="currentColor"></rect>
																<rect x="7" y="15.3137" width="12" height="2" rx="1"
																	transform="rotate(-45 7 15.3137)"
																	fill="currentColor"></rect>
																<rect x="8.41422" y="7" width="12" height="2" rx="1"
																	transform="rotate(45 8.41422 7)"
																	fill="currentColor"></rect>
															</svg>
														</span>
														<!--end::Svg Icon-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center">
														<span class="fw-semibold fs-5 text-muted flex-grow-1">Unlimited
															Cloud
															Space</span>
														<!--begin::Svg Icon | path: icons/duotune/general/gen040.svg-->
														<span class="svg-icon svg-icon-1">
															<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
																xmlns="http://www.w3.org/2000/svg">
																<rect opacity="0.3" x="2" y="2" width="20" height="20"
																	rx="10" fill="currentColor"></rect>
																<rect x="7" y="15.3137" width="12" height="2" rx="1"
																	transform="rotate(-45 7 15.3137)"
																	fill="currentColor"></rect>
																<rect x="8.41422" y="7" width="12" height="2" rx="1"
																	transform="rotate(45 8.41422 7)"
																	fill="currentColor"></rect>
															</svg>
														</span>
														<!--end::Svg Icon-->
													</div>
													<!--end::Item-->
												</div>
												<!--end::Body-->
											</div>
											<!--end::Tab Pane-->
											<!--begin::Tab Pane-->
											<div class="tab-pane fade" id="kt_upgrade_plan_advanced" role="tabpanel">
												<!--begin::Heading-->
												<div class="pb-5">
													<h2 class="fw-bold text-dark">What’s in Startup Plan?</h2>
													<div class="text-muted fw-semibold">Optimal for 100+ team size and
														grown
														company</div>
												</div>
												<!--end::Heading-->
												<!--begin::Body-->
												<div class="pt-1">
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-7">
														<span class="fw-semibold fs-5 text-gray-700 flex-grow-1">Up to
															10 Active
															Users</span>
														<!--begin::Svg Icon | path: icons/duotune/general/gen043.svg-->
														<span class="svg-icon svg-icon-1 svg-icon-success">
															<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
																xmlns="http://www.w3.org/2000/svg">
																<rect opacity="0.3" x="2" y="2" width="20" height="20"
																	rx="10" fill="currentColor"></rect>
																<path
																	d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z"
																	fill="currentColor"></path>
															</svg>
														</span>
														<!--end::Svg Icon-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-7">
														<span class="fw-semibold fs-5 text-gray-700 flex-grow-1">Up to
															30
															Project Integrations</span>
														<!--begin::Svg Icon | path: icons/duotune/general/gen043.svg-->
														<span class="svg-icon svg-icon-1 svg-icon-success">
															<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
																xmlns="http://www.w3.org/2000/svg">
																<rect opacity="0.3" x="2" y="2" width="20" height="20"
																	rx="10" fill="currentColor"></rect>
																<path
																	d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z"
																	fill="currentColor"></path>
															</svg>
														</span>
														<!--end::Svg Icon-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-7">
														<span
															class="fw-semibold fs-5 text-gray-700 flex-grow-1">Analytics
															Module</span>
														<!--begin::Svg Icon | path: icons/duotune/general/gen043.svg-->
														<span class="svg-icon svg-icon-1 svg-icon-success">
															<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
																xmlns="http://www.w3.org/2000/svg">
																<rect opacity="0.3" x="2" y="2" width="20" height="20"
																	rx="10" fill="currentColor"></rect>
																<path
																	d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z"
																	fill="currentColor"></path>
															</svg>
														</span>
														<!--end::Svg Icon-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-7">
														<span class="fw-semibold fs-5 text-gray-700 flex-grow-1">Finance
															Module</span>
														<!--begin::Svg Icon | path: icons/duotune/general/gen043.svg-->
														<span class="svg-icon svg-icon-1 svg-icon-success">
															<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
																xmlns="http://www.w3.org/2000/svg">
																<rect opacity="0.3" x="2" y="2" width="20" height="20"
																	rx="10" fill="currentColor"></rect>
																<path
																	d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z"
																	fill="currentColor"></path>
															</svg>
														</span>
														<!--end::Svg Icon-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-7">
														<span
															class="fw-semibold fs-5 text-gray-700 flex-grow-1">Accounting
															Module</span>
														<!--begin::Svg Icon | path: icons/duotune/general/gen043.svg-->
														<span class="svg-icon svg-icon-1 svg-icon-success">
															<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
																xmlns="http://www.w3.org/2000/svg">
																<rect opacity="0.3" x="2" y="2" width="20" height="20"
																	rx="10" fill="currentColor"></rect>
																<path
																	d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z"
																	fill="currentColor"></path>
															</svg>
														</span>
														<!--end::Svg Icon-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-7">
														<span class="fw-semibold fs-5 text-muted flex-grow-1">Network
															Platform</span>
														<!--begin::Svg Icon | path: icons/duotune/general/gen040.svg-->
														<span class="svg-icon svg-icon-1">
															<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
																xmlns="http://www.w3.org/2000/svg">
																<rect opacity="0.3" x="2" y="2" width="20" height="20"
																	rx="10" fill="currentColor"></rect>
																<rect x="7" y="15.3137" width="12" height="2" rx="1"
																	transform="rotate(-45 7 15.3137)"
																	fill="currentColor"></rect>
																<rect x="8.41422" y="7" width="12" height="2" rx="1"
																	transform="rotate(45 8.41422 7)"
																	fill="currentColor"></rect>
															</svg>
														</span>
														<!--end::Svg Icon-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center">
														<span class="fw-semibold fs-5 text-muted flex-grow-1">Unlimited
															Cloud
															Space</span>
														<!--begin::Svg Icon | path: icons/duotune/general/gen040.svg-->
														<span class="svg-icon svg-icon-1">
															<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
																xmlns="http://www.w3.org/2000/svg">
																<rect opacity="0.3" x="2" y="2" width="20" height="20"
																	rx="10" fill="currentColor"></rect>
																<rect x="7" y="15.3137" width="12" height="2" rx="1"
																	transform="rotate(-45 7 15.3137)"
																	fill="currentColor"></rect>
																<rect x="8.41422" y="7" width="12" height="2" rx="1"
																	transform="rotate(45 8.41422 7)"
																	fill="currentColor"></rect>
															</svg>
														</span>
														<!--end::Svg Icon-->
													</div>
													<!--end::Item-->
												</div>
												<!--end::Body-->
											</div>
											<!--end::Tab Pane-->
											<!--begin::Tab Pane-->
											<div class="tab-pane fade" id="kt_upgrade_plan_enterprise" role="tabpanel">
												<!--begin::Heading-->
												<div class="pb-5">
													<h2 class="fw-bold text-dark">What’s in Startup Plan?</h2>
													<div class="text-muted fw-semibold">Optimal for 1000+ team and
														enterpise
													</div>
												</div>
												<!--end::Heading-->
												<!--begin::Body-->
												<div class="pt-1">
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-7">
														<span class="fw-semibold fs-5 text-gray-700 flex-grow-1">Up to
															10 Active
															Users</span>
														<!--begin::Svg Icon | path: icons/duotune/general/gen043.svg-->
														<span class="svg-icon svg-icon-1 svg-icon-success">
															<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
																xmlns="http://www.w3.org/2000/svg">
																<rect opacity="0.3" x="2" y="2" width="20" height="20"
																	rx="10" fill="currentColor"></rect>
																<path
																	d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z"
																	fill="currentColor"></path>
															</svg>
														</span>
														<!--end::Svg Icon-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-7">
														<span class="fw-semibold fs-5 text-gray-700 flex-grow-1">Up to
															30
															Project Integrations</span>
														<!--begin::Svg Icon | path: icons/duotune/general/gen043.svg-->
														<span class="svg-icon svg-icon-1 svg-icon-success">
															<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
																xmlns="http://www.w3.org/2000/svg">
																<rect opacity="0.3" x="2" y="2" width="20" height="20"
																	rx="10" fill="currentColor"></rect>
																<path
																	d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z"
																	fill="currentColor"></path>
															</svg>
														</span>
														<!--end::Svg Icon-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-7">
														<span
															class="fw-semibold fs-5 text-gray-700 flex-grow-1">Analytics
															Module</span>
														<!--begin::Svg Icon | path: icons/duotune/general/gen043.svg-->
														<span class="svg-icon svg-icon-1 svg-icon-success">
															<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
																xmlns="http://www.w3.org/2000/svg">
																<rect opacity="0.3" x="2" y="2" width="20" height="20"
																	rx="10" fill="currentColor"></rect>
																<path
																	d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z"
																	fill="currentColor"></path>
															</svg>
														</span>
														<!--end::Svg Icon-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-7">
														<span class="fw-semibold fs-5 text-gray-700 flex-grow-1">Finance
															Module</span>
														<!--begin::Svg Icon | path: icons/duotune/general/gen043.svg-->
														<span class="svg-icon svg-icon-1 svg-icon-success">
															<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
																xmlns="http://www.w3.org/2000/svg">
																<rect opacity="0.3" x="2" y="2" width="20" height="20"
																	rx="10" fill="currentColor"></rect>
																<path
																	d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z"
																	fill="currentColor"></path>
															</svg>
														</span>
														<!--end::Svg Icon-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-7">
														<span
															class="fw-semibold fs-5 text-gray-700 flex-grow-1">Accounting
															Module</span>
														<!--begin::Svg Icon | path: icons/duotune/general/gen043.svg-->
														<span class="svg-icon svg-icon-1 svg-icon-success">
															<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
																xmlns="http://www.w3.org/2000/svg">
																<rect opacity="0.3" x="2" y="2" width="20" height="20"
																	rx="10" fill="currentColor"></rect>
																<path
																	d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z"
																	fill="currentColor"></path>
															</svg>
														</span>
														<!--end::Svg Icon-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-7">
														<span class="fw-semibold fs-5 text-gray-700 flex-grow-1">Network
															Platform</span>
														<!--begin::Svg Icon | path: icons/duotune/general/gen043.svg-->
														<span class="svg-icon svg-icon-1 svg-icon-success">
															<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
																xmlns="http://www.w3.org/2000/svg">
																<rect opacity="0.3" x="2" y="2" width="20" height="20"
																	rx="10" fill="currentColor"></rect>
																<path
																	d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z"
																	fill="currentColor"></path>
															</svg>
														</span>
														<!--end::Svg Icon-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center">
														<span
															class="fw-semibold fs-5 text-gray-700 flex-grow-1">Unlimited
															Cloud
															Space</span>
														<!--begin::Svg Icon | path: icons/duotune/general/gen043.svg-->
														<span class="svg-icon svg-icon-1 svg-icon-success">
															<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
																xmlns="http://www.w3.org/2000/svg">
																<rect opacity="0.3" x="2" y="2" width="20" height="20"
																	rx="10" fill="currentColor"></rect>
																<path
																	d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z"
																	fill="currentColor"></path>
															</svg>
														</span>
														<!--end::Svg Icon-->
													</div>
													<!--end::Item-->
												</div>
												<!--end::Body-->
											</div>
											<!--end::Tab Pane-->
											<!--begin::Tab Pane-->
											<div class="tab-pane fade" id="kt_upgrade_plan_custom" role="tabpanel">
												<!--begin::Heading-->
												<div class="pb-5">
													<h2 class="fw-bold text-dark">What’s in Startup Plan?</h2>
													<div class="text-muted fw-semibold">Optimal for corporations</div>
												</div>
												<!--end::Heading-->
												<!--begin::Body-->
												<div class="pt-1">
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-7">
														<span
															class="fw-semibold fs-5 text-gray-700 flex-grow-1">Unlimited
															Users</span>
														<!--begin::Svg Icon | path: icons/duotune/general/gen043.svg-->
														<span class="svg-icon svg-icon-1 svg-icon-success">
															<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
																xmlns="http://www.w3.org/2000/svg">
																<rect opacity="0.3" x="2" y="2" width="20" height="20"
																	rx="10" fill="currentColor"></rect>
																<path
																	d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z"
																	fill="currentColor"></path>
															</svg>
														</span>
														<!--end::Svg Icon-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-7">
														<span
															class="fw-semibold fs-5 text-gray-700 flex-grow-1">Unlimited
															Project Integrations</span>
														<!--begin::Svg Icon | path: icons/duotune/general/gen043.svg-->
														<span class="svg-icon svg-icon-1 svg-icon-success">
															<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
																xmlns="http://www.w3.org/2000/svg">
																<rect opacity="0.3" x="2" y="2" width="20" height="20"
																	rx="10" fill="currentColor"></rect>
																<path
																	d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z"
																	fill="currentColor"></path>
															</svg>
														</span>
														<!--end::Svg Icon-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-7">
														<span
															class="fw-semibold fs-5 text-gray-700 flex-grow-1">Analytics
															Module</span>
														<!--begin::Svg Icon | path: icons/duotune/general/gen043.svg-->
														<span class="svg-icon svg-icon-1 svg-icon-success">
															<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
																xmlns="http://www.w3.org/2000/svg">
																<rect opacity="0.3" x="2" y="2" width="20" height="20"
																	rx="10" fill="currentColor"></rect>
																<path
																	d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z"
																	fill="currentColor"></path>
															</svg>
														</span>
														<!--end::Svg Icon-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-7">
														<span class="fw-semibold fs-5 text-gray-700 flex-grow-1">Finance
															Module</span>
														<!--begin::Svg Icon | path: icons/duotune/general/gen043.svg-->
														<span class="svg-icon svg-icon-1 svg-icon-success">
															<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
																xmlns="http://www.w3.org/2000/svg">
																<rect opacity="0.3" x="2" y="2" width="20" height="20"
																	rx="10" fill="currentColor"></rect>
																<path
																	d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z"
																	fill="currentColor"></path>
															</svg>
														</span>
														<!--end::Svg Icon-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-7">
														<span
															class="fw-semibold fs-5 text-gray-700 flex-grow-1">Accounting
															Module</span>
														<!--begin::Svg Icon | path: icons/duotune/general/gen043.svg-->
														<span class="svg-icon svg-icon-1 svg-icon-success">
															<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
																xmlns="http://www.w3.org/2000/svg">
																<rect opacity="0.3" x="2" y="2" width="20" height="20"
																	rx="10" fill="currentColor"></rect>
																<path
																	d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z"
																	fill="currentColor"></path>
															</svg>
														</span>
														<!--end::Svg Icon-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-7">
														<span class="fw-semibold fs-5 text-gray-700 flex-grow-1">Network
															Platform</span>
														<!--begin::Svg Icon | path: icons/duotune/general/gen043.svg-->
														<span class="svg-icon svg-icon-1 svg-icon-success">
															<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
																xmlns="http://www.w3.org/2000/svg">
																<rect opacity="0.3" x="2" y="2" width="20" height="20"
																	rx="10" fill="currentColor"></rect>
																<path
																	d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z"
																	fill="currentColor"></path>
															</svg>
														</span>
														<!--end::Svg Icon-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center">
														<span
															class="fw-semibold fs-5 text-gray-700 flex-grow-1">Unlimited
															Cloud
															Space</span>
														<!--begin::Svg Icon | path: icons/duotune/general/gen043.svg-->
														<span class="svg-icon svg-icon-1 svg-icon-success">
															<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
																xmlns="http://www.w3.org/2000/svg">
																<rect opacity="0.3" x="2" y="2" width="20" height="20"
																	rx="10" fill="currentColor"></rect>
																<path
																	d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z"
																	fill="currentColor"></path>
															</svg>
														</span>
														<!--end::Svg Icon-->
													</div>
													<!--end::Item-->
												</div>
												<!--end::Body-->
											</div>
											<!--end::Tab Pane-->
										</div>
										<!--end::Tab content-->
									</div>
									<!--end::Col-->
								</div>
								<!--end::Row-->
							</div>
							<!--end::Plans-->
							<!--begin::Actions-->
							<div class="d-flex flex-center flex-row-fluid pt-12">
								<button type="reset" class="btn btn-light me-3" data-bs-dismiss="modal">Cancel</button>
								<button type="submit" class="btn btn-primary" id="kt_modal_upgrade_plan_btn">
									<!--begin::Indicator label-->
									<span class="indicator-label">Upgrade Plan</span>
									<!--end::Indicator label-->
									<!--begin::Indicator progress-->
									<span class="indicator-progress">Please wait...
										<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
									<!--end::Indicator progress-->
								</button>
							</div>
							<!--end::Actions-->
						</div>
						<!--end::Modal body-->
					</div>
					<!--end::Modal content-->
				</div>
				<!--end::Modal dialog-->
			</div>
			<!--end::Modal - Upgrade plan-->
			<!--begin::Modal - Create App-->
			<div class="modal fade" id="kt_modal_create_app" tabindex="-1" aria-hidden="true">
				<!--begin::Modal dialog-->
				<div class="modal-dialog modal-dialog-centered mw-900px">
					<!--begin::Modal content-->
					<div class="modal-content">
						<!--begin::Modal header-->
						<div class="modal-header">
							<!--begin::Modal title-->
							<h2>Create App</h2>
							<!--end::Modal title-->
							<!--begin::Close-->
							<div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
								<!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
								<span class="svg-icon svg-icon-1">
									<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
										xmlns="http://www.w3.org/2000/svg">
										<rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
											transform="rotate(-45 6 17.3137)" fill="currentColor"></rect>
										<rect x="7.41422" y="6" width="16" height="2" rx="1"
											transform="rotate(45 7.41422 6)" fill="currentColor"></rect>
									</svg>
								</span>
								<!--end::Svg Icon-->
							</div>
							<!--end::Close-->
						</div>
						<!--end::Modal header-->
						<!--begin::Modal body-->
						<div class="modal-body py-lg-10 px-lg-10">
							<!--begin::Stepper-->
							<div class="stepper stepper-pills stepper-column d-flex flex-column flex-xl-row flex-row-fluid"
								id="kt_modal_create_app_stepper" data-kt-stepper="true">
								<!--begin::Aside-->
								<div
									class="d-flex justify-content-center justify-content-xl-start flex-row-auto w-100 w-xl-300px">
									<!--begin::Nav-->
									<div class="stepper-nav ps-lg-10">
										<!--begin::Step 1-->
										<div class="stepper-item current" data-kt-stepper-element="nav">
											<!--begin::Wrapper-->
											<div class="stepper-wrapper">
												<!--begin::Icon-->
												<div class="stepper-icon w-40px h-40px">
													<i class="stepper-check fas fa-check"></i>
													<span class="stepper-number">1</span>
												</div>
												<!--end::Icon-->
												<!--begin::Label-->
												<div class="stepper-label">
													<h3 class="stepper-title">Details</h3>
													<div class="stepper-desc">Name your App</div>
												</div>
												<!--end::Label-->
											</div>
											<!--end::Wrapper-->
											<!--begin::Line-->
											<div class="stepper-line h-40px"></div>
											<!--end::Line-->
										</div>
										<!--end::Step 1-->
										<!--begin::Step 2-->
										<div class="stepper-item" data-kt-stepper-element="nav">
											<!--begin::Wrapper-->
											<div class="stepper-wrapper">
												<!--begin::Icon-->
												<div class="stepper-icon w-40px h-40px">
													<i class="stepper-check fas fa-check"></i>
													<span class="stepper-number">2</span>
												</div>
												<!--begin::Icon-->
												<!--begin::Label-->
												<div class="stepper-label">
													<h3 class="stepper-title">Frameworks</h3>
													<div class="stepper-desc">Define your app framework</div>
												</div>
												<!--begin::Label-->
											</div>
											<!--end::Wrapper-->
											<!--begin::Line-->
											<div class="stepper-line h-40px"></div>
											<!--end::Line-->
										</div>
										<!--end::Step 2-->
										<!--begin::Step 3-->
										<div class="stepper-item" data-kt-stepper-element="nav">
											<!--begin::Wrapper-->
											<div class="stepper-wrapper">
												<!--begin::Icon-->
												<div class="stepper-icon w-40px h-40px">
													<i class="stepper-check fas fa-check"></i>
													<span class="stepper-number">3</span>
												</div>
												<!--end::Icon-->
												<!--begin::Label-->
												<div class="stepper-label">
													<h3 class="stepper-title">Database</h3>
													<div class="stepper-desc">Select the app database type</div>
												</div>
												<!--end::Label-->
											</div>
											<!--end::Wrapper-->
											<!--begin::Line-->
											<div class="stepper-line h-40px"></div>
											<!--end::Line-->
										</div>
										<!--end::Step 3-->
										<!--begin::Step 4-->
										<div class="stepper-item" data-kt-stepper-element="nav">
											<!--begin::Wrapper-->
											<div class="stepper-wrapper">
												<!--begin::Icon-->
												<div class="stepper-icon w-40px h-40px">
													<i class="stepper-check fas fa-check"></i>
													<span class="stepper-number">4</span>
												</div>
												<!--end::Icon-->
												<!--begin::Label-->
												<div class="stepper-label">
													<h3 class="stepper-title">Billing</h3>
													<div class="stepper-desc">Provide payment details</div>
												</div>
												<!--end::Label-->
											</div>
											<!--end::Wrapper-->
											<!--begin::Line-->
											<div class="stepper-line h-40px"></div>
											<!--end::Line-->
										</div>
										<!--end::Step 4-->
										<!--begin::Step 5-->
										<div class="stepper-item mark-completed" data-kt-stepper-element="nav">
											<!--begin::Wrapper-->
											<div class="stepper-wrapper">
												<!--begin::Icon-->
												<div class="stepper-icon w-40px h-40px">
													<i class="stepper-check fas fa-check"></i>
													<span class="stepper-number">5</span>
												</div>
												<!--end::Icon-->
												<!--begin::Label-->
												<div class="stepper-label">
													<h3 class="stepper-title">Completed</h3>
													<div class="stepper-desc">Review and Submit</div>
												</div>
												<!--end::Label-->
											</div>
											<!--end::Wrapper-->
										</div>
										<!--end::Step 5-->
									</div>
									<!--end::Nav-->
								</div>
								<!--begin::Aside-->
								<!--begin::Content-->
								<div class="flex-row-fluid py-lg-5 px-lg-15">
									<!--begin::Form-->
									<form class="form fv-plugins-bootstrap5 fv-plugins-framework"
										novalidate="novalidate" id="kt_modal_create_app_form">
										<!--begin::Step 1-->
										<div class="current" data-kt-stepper-element="content">
											<div class="w-100">
												<!--begin::Input group-->
												<div class="fv-row mb-10 fv-plugins-icon-container">
													<!--begin::Label-->
													<label class="d-flex align-items-center fs-5 fw-semibold mb-2">
														<span class="required">App Name</span>
														<i class="fas fa-exclamation-circle ms-2 fs-7"
															data-bs-toggle="tooltip"
															aria-label="Specify your unique app name"
															data-kt-initialized="1"></i>
													</label>
													<!--end::Label-->
													<!--begin::Input-->
													<input type="text"
														class="form-control form-control-lg form-control-solid"
														name="name" placeholder="" value="">
													<!--end::Input-->
													<div class="fv-plugins-message-container invalid-feedback"></div>
													<div class="fv-plugins-message-container invalid-feedback"></div>
													<div class="fv-plugins-message-container invalid-feedback"></div>
													<div class="fv-plugins-message-container invalid-feedback"></div>
													<div class="fv-plugins-message-container invalid-feedback"></div>
													<div class="fv-plugins-message-container invalid-feedback"></div>
												</div>
												<!--end::Input group-->
												<!--begin::Input group-->
												<div class="fv-row">
													<!--begin::Label-->
													<label class="d-flex align-items-center fs-5 fw-semibold mb-4">
														<span class="required">Category</span>
														<i class="fas fa-exclamation-circle ms-2 fs-7"
															data-bs-toggle="tooltip"
															aria-label="Select your app category"
															data-kt-initialized="1"></i>
													</label>
													<!--end::Label-->
													<!--begin:Options-->
													<div class="fv-row fv-plugins-icon-container">
														<!--begin:Option-->
														<label class="d-flex flex-stack mb-5 cursor-pointer">
															<!--begin:Label-->
															<span class="d-flex align-items-center me-2">
																<!--begin:Icon-->
																<span class="symbol symbol-50px me-6">
																	<span class="symbol-label bg-light-primary">
																		<!--begin::Svg Icon | path: icons/duotune/maps/map004.svg-->
																		<span
																			class="svg-icon svg-icon-1 svg-icon-primary">
																			<svg width="24" height="24"
																				viewBox="0 0 24 24" fill="none"
																				xmlns="http://www.w3.org/2000/svg">
																				<path opacity="0.3"
																					d="M18.4 5.59998C21.9 9.09998 21.9 14.8 18.4 18.3C14.9 21.8 9.2 21.8 5.7 18.3L18.4 5.59998Z"
																					fill="currentColor"></path>
																				<path
																					d="M12 2C6.5 2 2 6.5 2 12C2 17.5 6.5 22 12 22C17.5 22 22 17.5 22 12C22 6.5 17.5 2 12 2ZM19.9 11H13V8.8999C14.9 8.6999 16.7 8.00005 18.1 6.80005C19.1 8.00005 19.7 9.4 19.9 11ZM11 19.8999C9.7 19.6999 8.39999 19.2 7.39999 18.5C8.49999 17.7 9.7 17.2001 11 17.1001V19.8999ZM5.89999 6.90002C7.39999 8.10002 9.2 8.8 11 9V11.1001H4.10001C4.30001 9.4001 4.89999 8.00002 5.89999 6.90002ZM7.39999 5.5C8.49999 4.7 9.7 4.19998 11 4.09998V7C9.7 6.8 8.39999 6.3 7.39999 5.5ZM13 17.1001C14.3 17.3001 15.6 17.8 16.6 18.5C15.5 19.3 14.3 19.7999 13 19.8999V17.1001ZM13 4.09998C14.3 4.29998 15.6 4.8 16.6 5.5C15.5 6.3 14.3 6.80002 13 6.90002V4.09998ZM4.10001 13H11V15.1001C9.1 15.3001 7.29999 16 5.89999 17.2C4.89999 16 4.30001 14.6 4.10001 13ZM18.1 17.1001C16.6 15.9001 14.8 15.2 13 15V12.8999H19.9C19.7 14.5999 19.1 16.0001 18.1 17.1001Z"
																					fill="currentColor"></path>
																			</svg>
																		</span>
																		<!--end::Svg Icon-->
																	</span>
																</span>
																<!--end:Icon-->
																<!--begin:Info-->
																<span class="d-flex flex-column">
																	<span class="fw-bold fs-6">Quick Online
																		Courses</span>
																	<span class="fs-7 text-muted">Creating a clear text
																		structure is just one SEO</span>
																</span>
																<!--end:Info-->
															</span>
															<!--end:Label-->
															<!--begin:Input-->
															<span class="form-check form-check-custom form-check-solid">
																<input class="form-check-input" type="radio"
																	name="category" value="1">
															</span>
															<!--end:Input-->
														</label>
														<!--end::Option-->
														<!--begin:Option-->
														<label class="d-flex flex-stack mb-5 cursor-pointer">
															<!--begin:Label-->
															<span class="d-flex align-items-center me-2">
																<!--begin:Icon-->
																<span class="symbol symbol-50px me-6">
																	<span class="symbol-label bg-light-danger">
																		<!--begin::Svg Icon | path: icons/duotune/general/gen025.svg-->
																		<span
																			class="svg-icon svg-icon-1 svg-icon-danger">
																			<svg width="24" height="24"
																				viewBox="0 0 24 24" fill="none"
																				xmlns="http://www.w3.org/2000/svg">
																				<rect x="2" y="2" width="9" height="9"
																					rx="2" fill="currentColor"></rect>
																				<rect opacity="0.3" x="13" y="2"
																					width="9" height="9" rx="2"
																					fill="currentColor"></rect>
																				<rect opacity="0.3" x="13" y="13"
																					width="9" height="9" rx="2"
																					fill="currentColor"></rect>
																				<rect opacity="0.3" x="2" y="13"
																					width="9" height="9" rx="2"
																					fill="currentColor"></rect>
																			</svg>
																		</span>
																		<!--end::Svg Icon-->
																	</span>
																</span>
																<!--end:Icon-->
																<!--begin:Info-->
																<span class="d-flex flex-column">
																	<span class="fw-bold fs-6">Face to Face
																		Discussions</span>
																	<span class="fs-7 text-muted">Creating a clear text
																		structure is just one aspect</span>
																</span>
																<!--end:Info-->
															</span>
															<!--end:Label-->
															<!--begin:Input-->
															<span class="form-check form-check-custom form-check-solid">
																<input class="form-check-input" type="radio"
																	name="category" value="2">
															</span>
															<!--end:Input-->
														</label>
														<!--end::Option-->
														<!--begin:Option-->
														<label class="d-flex flex-stack cursor-pointer">
															<!--begin:Label-->
															<span class="d-flex align-items-center me-2">
																<!--begin:Icon-->
																<span class="symbol symbol-50px me-6">
																	<span class="symbol-label bg-light-success">
																		<!--begin::Svg Icon | path: icons/duotune/general/gen013.svg-->
																		<span
																			class="svg-icon svg-icon-1 svg-icon-success">
																			<svg width="24" height="24"
																				viewBox="0 0 24 24" fill="none"
																				xmlns="http://www.w3.org/2000/svg">
																				<path opacity="0.3"
																					d="M20.9 12.9C20.3 12.9 19.9 12.5 19.9 11.9C19.9 11.3 20.3 10.9 20.9 10.9H21.8C21.3 6.2 17.6 2.4 12.9 2V2.9C12.9 3.5 12.5 3.9 11.9 3.9C11.3 3.9 10.9 3.5 10.9 2.9V2C6.19999 2.5 2.4 6.2 2 10.9H2.89999C3.49999 10.9 3.89999 11.3 3.89999 11.9C3.89999 12.5 3.49999 12.9 2.89999 12.9H2C2.5 17.6 6.19999 21.4 10.9 21.8V20.9C10.9 20.3 11.3 19.9 11.9 19.9C12.5 19.9 12.9 20.3 12.9 20.9V21.8C17.6 21.3 21.4 17.6 21.8 12.9H20.9Z"
																					fill="currentColor"></path>
																				<path
																					d="M16.9 10.9H13.6C13.4 10.6 13.2 10.4 12.9 10.2V5.90002C12.9 5.30002 12.5 4.90002 11.9 4.90002C11.3 4.90002 10.9 5.30002 10.9 5.90002V10.2C10.6 10.4 10.4 10.6 10.2 10.9H9.89999C9.29999 10.9 8.89999 11.3 8.89999 11.9C8.89999 12.5 9.29999 12.9 9.89999 12.9H10.2C10.4 13.2 10.6 13.4 10.9 13.6V13.9C10.9 14.5 11.3 14.9 11.9 14.9C12.5 14.9 12.9 14.5 12.9 13.9V13.6C13.2 13.4 13.4 13.2 13.6 12.9H16.9C17.5 12.9 17.9 12.5 17.9 11.9C17.9 11.3 17.5 10.9 16.9 10.9Z"
																					fill="currentColor"></path>
																			</svg>
																		</span>
																		<!--end::Svg Icon-->
																	</span>
																</span>
																<!--end:Icon-->
																<!--begin:Info-->
																<span class="d-flex flex-column">
																	<span class="fw-bold fs-6">Full Intro
																		Training</span>
																	<span class="fs-7 text-muted">Creating a clear text
																		structure copywriting</span>
																</span>
																<!--end:Info-->
															</span>
															<!--end:Label-->
															<!--begin:Input-->
															<span class="form-check form-check-custom form-check-solid">
																<input class="form-check-input" type="radio"
																	name="category" value="3">
															</span>
															<!--end:Input-->
														</label>
														<!--end::Option-->
														<div class="fv-plugins-message-container invalid-feedback">
														</div>
														<div class="fv-plugins-message-container invalid-feedback">
														</div>
														<div class="fv-plugins-message-container invalid-feedback">
														</div>
														<div class="fv-plugins-message-container invalid-feedback">
														</div>
														<div class="fv-plugins-message-container invalid-feedback">
														</div>
														<div class="fv-plugins-message-container invalid-feedback">
														</div>
													</div>
													<!--end:Options-->
												</div>
												<!--end::Input group-->
											</div>
										</div>
										<!--end::Step 1-->
										<!--begin::Step 2-->
										<div data-kt-stepper-element="content">
											<div class="w-100">
												<!--begin::Input group-->
												<div class="fv-row fv-plugins-icon-container">
													<!--begin::Label-->
													<label class="d-flex align-items-center fs-5 fw-semibold mb-4">
														<span class="required">Select Framework</span>
														<i class="fas fa-exclamation-circle ms-2 fs-7"
															data-bs-toggle="tooltip"
															aria-label="Specify your apps framework"
															data-kt-initialized="1"></i>
													</label>
													<!--end::Label-->
													<!--begin:Option-->
													<label class="d-flex flex-stack cursor-pointer mb-5">
														<!--begin:Label-->
														<span class="d-flex align-items-center me-2">
															<!--begin:Icon-->
															<span class="symbol symbol-50px me-6">
																<span class="symbol-label bg-light-warning">
																	<i class="fab fa-html5 text-warning fs-2x"></i>
																</span>
															</span>
															<!--end:Icon-->
															<!--begin:Info-->
															<span class="d-flex flex-column">
																<span class="fw-bold fs-6">HTML5</span>
																<span class="fs-7 text-muted">Base Web Projec</span>
															</span>
															<!--end:Info-->
														</span>
														<!--end:Label-->
														<!--begin:Input-->
														<span class="form-check form-check-custom form-check-solid">
															<input class="form-check-input" type="radio"
																checked="checked" name="framework" value="1">
														</span>
														<!--end:Input-->
													</label>
													<!--end::Option-->
													<!--begin:Option-->
													<label class="d-flex flex-stack cursor-pointer mb-5">
														<!--begin:Label-->
														<span class="d-flex align-items-center me-2">
															<!--begin:Icon-->
															<span class="symbol symbol-50px me-6">
																<span class="symbol-label bg-light-success">
																	<i class="fab fa-react text-success fs-2x"></i>
																</span>
															</span>
															<!--end:Icon-->
															<!--begin:Info-->
															<span class="d-flex flex-column">
																<span class="fw-bold fs-6">ReactJS</span>
																<span class="fs-7 text-muted">Robust and flexible app
																	framework</span>
															</span>
															<!--end:Info-->
														</span>
														<!--end:Label-->
														<!--begin:Input-->
														<span class="form-check form-check-custom form-check-solid">
															<input class="form-check-input" type="radio"
																name="framework" value="2">
														</span>
														<!--end:Input-->
													</label>
													<!--end::Option-->
													<!--begin:Option-->
													<label class="d-flex flex-stack cursor-pointer mb-5">
														<!--begin:Label-->
														<span class="d-flex align-items-center me-2">
															<!--begin:Icon-->
															<span class="symbol symbol-50px me-6">
																<span class="symbol-label bg-light-danger">
																	<i class="fab fa-angular text-danger fs-2x"></i>
																</span>
															</span>
															<!--end:Icon-->
															<!--begin:Info-->
															<span class="d-flex flex-column">
																<span class="fw-bold fs-6">Angular</span>
																<span class="fs-7 text-muted">Powerful data
																	mangement</span>
															</span>
															<!--end:Info-->
														</span>
														<!--end:Label-->
														<!--begin:Input-->
														<span class="form-check form-check-custom form-check-solid">
															<input class="form-check-input" type="radio"
																name="framework" value="3">
														</span>
														<!--end:Input-->
													</label>
													<!--end::Option-->
													<!--begin:Option-->
													<label class="d-flex flex-stack cursor-pointer">
														<!--begin:Label-->
														<span class="d-flex align-items-center me-2">
															<!--begin:Icon-->
															<span class="symbol symbol-50px me-6">
																<span class="symbol-label bg-light-primary">
																	<i class="fab fa-vuejs text-primary fs-2x"></i>
																</span>
															</span>
															<!--end:Icon-->
															<!--begin:Info-->
															<span class="d-flex flex-column">
																<span class="fw-bold fs-6">Vue</span>
																<span class="fs-7 text-muted">Lightweight and responsive
																	framework</span>
															</span>
															<!--end:Info-->
														</span>
														<!--end:Label-->
														<!--begin:Input-->
														<span class="form-check form-check-custom form-check-solid">
															<input class="form-check-input" type="radio"
																name="framework" value="4">
														</span>
														<!--end:Input-->
													</label>
													<!--end::Option-->
													<div class="fv-plugins-message-container invalid-feedback"></div>
													<div class="fv-plugins-message-container invalid-feedback"></div>
													<div class="fv-plugins-message-container invalid-feedback"></div>
													<div class="fv-plugins-message-container invalid-feedback"></div>
													<div class="fv-plugins-message-container invalid-feedback"></div>
													<div class="fv-plugins-message-container invalid-feedback"></div>
												</div>
												<!--end::Input group-->
											</div>
										</div>
										<!--end::Step 2-->
										<!--begin::Step 3-->
										<div data-kt-stepper-element="content">
											<div class="w-100">
												<!--begin::Input group-->
												<div class="fv-row mb-10 fv-plugins-icon-container">
													<!--begin::Label-->
													<label class="required fs-5 fw-semibold mb-2">Database Name</label>
													<!--end::Label-->
													<!--begin::Input-->
													<input type="text"
														class="form-control form-control-lg form-control-solid"
														name="dbname" placeholder="" value="master_db">
													<!--end::Input-->
													<div class="fv-plugins-message-container invalid-feedback"></div>
													<div class="fv-plugins-message-container invalid-feedback"></div>
													<div class="fv-plugins-message-container invalid-feedback"></div>
													<div class="fv-plugins-message-container invalid-feedback"></div>
													<div class="fv-plugins-message-container invalid-feedback"></div>
													<div class="fv-plugins-message-container invalid-feedback"></div>
												</div>
												<!--end::Input group-->
												<!--begin::Input group-->
												<div class="fv-row fv-plugins-icon-container">
													<!--begin::Label-->
													<label class="d-flex align-items-center fs-5 fw-semibold mb-4">
														<span class="required">Select Database Engine</span>
														<i class="fas fa-exclamation-circle ms-2 fs-7"
															data-bs-toggle="tooltip"
															aria-label="Select your app database engine"
															data-kt-initialized="1"></i>
													</label>
													<!--end::Label-->
													<!--begin:Option-->
													<label class="d-flex flex-stack cursor-pointer mb-5">
														<!--begin::Label-->
														<span class="d-flex align-items-center me-2">
															<!--begin::Icon-->
															<span class="symbol symbol-50px me-6">
																<span class="symbol-label bg-light-success">
																	<i class="fas fa-database text-success fs-2x"></i>
																</span>
															</span>
															<!--end::Icon-->
															<!--begin::Info-->
															<span class="d-flex flex-column">
																<span class="fw-bold fs-6">MySQL</span>
																<span class="fs-7 text-muted">Basic MySQL
																	database</span>
															</span>
															<!--end::Info-->
														</span>
														<!--end::Label-->
														<!--begin::Input-->
														<span class="form-check form-check-custom form-check-solid">
															<input class="form-check-input" type="radio" name="dbengine"
																checked="checked" value="1">
														</span>
														<!--end::Input-->
													</label>
													<!--end::Option-->
													<!--begin:Option-->
													<label class="d-flex flex-stack cursor-pointer mb-5">
														<!--begin::Label-->
														<span class="d-flex align-items-center me-2">
															<!--begin::Icon-->
															<span class="symbol symbol-50px me-6">
																<span class="symbol-label bg-light-danger">
																	<i class="fab fa-google text-danger fs-2x"></i>
																</span>
															</span>
															<!--end::Icon-->
															<!--begin::Info-->
															<span class="d-flex flex-column">
																<span class="fw-bold fs-6">Firebase</span>
																<span class="fs-7 text-muted">Google based app data
																	management</span>
															</span>
															<!--end::Info-->
														</span>
														<!--end::Label-->
														<!--begin::Input-->
														<span class="form-check form-check-custom form-check-solid">
															<input class="form-check-input" type="radio" name="dbengine"
																value="2">
														</span>
														<!--end::Input-->
													</label>
													<!--end::Option-->
													<!--begin:Option-->
													<label class="d-flex flex-stack cursor-pointer">
														<!--begin::Label-->
														<span class="d-flex align-items-center me-2">
															<!--begin::Icon-->
															<span class="symbol symbol-50px me-6">
																<span class="symbol-label bg-light-warning">
																	<i class="fab fa-amazon text-warning fs-2x"></i>
																</span>
															</span>
															<!--end::Icon-->
															<!--begin::Info-->
															<span class="d-flex flex-column">
																<span class="fw-bold fs-6">DynamoDB</span>
																<span class="fs-7 text-muted">Amazon Fast NoSQL
																	Database</span>
															</span>
															<!--end::Info-->
														</span>
														<!--end::Label-->
														<!--begin::Input-->
														<span class="form-check form-check-custom form-check-solid">
															<input class="form-check-input" type="radio" name="dbengine"
																value="3">
														</span>
														<!--end::Input-->
													</label>
													<!--end::Option-->
													<div class="fv-plugins-message-container invalid-feedback"></div>
													<div class="fv-plugins-message-container invalid-feedback"></div>
													<div class="fv-plugins-message-container invalid-feedback"></div>
													<div class="fv-plugins-message-container invalid-feedback"></div>
													<div class="fv-plugins-message-container invalid-feedback"></div>
													<div class="fv-plugins-message-container invalid-feedback"></div>
												</div>
												<!--end::Input group-->
											</div>
										</div>
										<!--end::Step 3-->
										<!--begin::Step 4-->
										<div data-kt-stepper-element="content">
											<div class="w-100">
												<!--begin::Input group-->
												<div class="d-flex flex-column mb-7 fv-row fv-plugins-icon-container">
													<!--begin::Label-->
													<label
														class="d-flex align-items-center fs-6 fw-semibold form-label mb-2">
														<span class="required">Name On Card</span>
														<i class="fas fa-exclamation-circle ms-2 fs-7"
															data-bs-toggle="tooltip"
															aria-label="Specify a card holder's name"
															data-kt-initialized="1"></i>
													</label>
													<!--end::Label-->
													<input type="text" class="form-control form-control-solid"
														placeholder="" name="card_name" value="Max Doe">
													<div class="fv-plugins-message-container invalid-feedback"></div>
													<div class="fv-plugins-message-container invalid-feedback"></div>
													<div class="fv-plugins-message-container invalid-feedback"></div>
													<div class="fv-plugins-message-container invalid-feedback"></div>
													<div class="fv-plugins-message-container invalid-feedback"></div>
													<div class="fv-plugins-message-container invalid-feedback"></div>
												</div>
												<!--end::Input group-->
												<!--begin::Input group-->
												<div class="d-flex flex-column mb-7 fv-row fv-plugins-icon-container">
													<!--begin::Label-->
													<label class="required fs-6 fw-semibold form-label mb-2">Card
														Number</label>
													<!--end::Label-->
													<!--begin::Input wrapper-->
													<div class="position-relative">
														<!--begin::Input-->
														<input type="text" class="form-control form-control-solid"
															placeholder="Enter card number" name="card_number"
															value="4111 1111 1111 1111">
														<!--end::Input-->
														<!--begin::Card logos-->
														<div
															class="position-absolute translate-middle-y top-50 end-0 me-5">
															<img src="assets/media/svg/card-logos/visa.svg" alt=""
																class="h-25px">
															<img src="assets/media/svg/card-logos/mastercard.svg" alt=""
																class="h-25px">
															<img src="assets/media/svg/card-logos/american-express.svg"
																alt="" class="h-25px">
														</div>
														<!--end::Card logos-->
													</div>
													<!--end::Input wrapper-->
													<div class="fv-plugins-message-container invalid-feedback"></div>
													<div class="fv-plugins-message-container invalid-feedback"></div>
													<div class="fv-plugins-message-container invalid-feedback"></div>
													<div class="fv-plugins-message-container invalid-feedback"></div>
													<div class="fv-plugins-message-container invalid-feedback"></div>
													<div class="fv-plugins-message-container invalid-feedback"></div>
												</div>
												<!--end::Input group-->
												<!--begin::Input group-->
												<div class="row mb-10">
													<!--begin::Col-->
													<div class="col-md-8 fv-row">
														<!--begin::Label-->
														<label
															class="required fs-6 fw-semibold form-label mb-2">Expiration
															Date</label>
														<!--end::Label-->
														<!--begin::Row-->
														<div class="row fv-row fv-plugins-icon-container">
															<!--begin::Col-->
															<div class="col-6">
																<select name="card_expiry_month"
																	class="form-select form-select-solid select2-hidden-accessible"
																	data-control="select2" data-hide-search="true"
																	data-placeholder="Month"
																	data-select2-id="select2-data-7-onnq" tabindex="-1"
																	aria-hidden="true" data-kt-initialized="1">
																	<option data-select2-id="select2-data-9-5393">
																	</option>
																	<option value="1">1</option>
																	<option value="2">2</option>
																	<option value="3">3</option>
																	<option value="4">4</option>
																	<option value="5">5</option>
																	<option value="6">6</option>
																	<option value="7">7</option>
																	<option value="8">8</option>
																	<option value="9">9</option>
																	<option value="10">10</option>
																	<option value="11">11</option>
																	<option value="12">12</option>
																</select><span
																	class="select2 select2-container select2-container--bootstrap5"
																	dir="ltr" data-select2-id="select2-data-8-n6th"
																	style="width: 100%;"><span class="selection"><span
																			class="select2-selection select2-selection--single form-select form-select-solid"
																			role="combobox" aria-haspopup="true"
																			aria-expanded="false" tabindex="0"
																			aria-disabled="false"
																			aria-labelledby="select2-card_expiry_month-j6-container"
																			aria-controls="select2-card_expiry_month-j6-container"><span
																				class="select2-selection__rendered"
																				id="select2-card_expiry_month-j6-container"
																				role="textbox" aria-readonly="true"
																				title="Month"><span
																					class="select2-selection__placeholder">Month</span></span><span
																				class="select2-selection__arrow"
																				role="presentation"><b
																					role="presentation"></b></span></span></span><span
																		class="dropdown-wrapper"
																		aria-hidden="true"></span></span>
																<div
																	class="fv-plugins-message-container invalid-feedback">
																</div>
																<div
																	class="fv-plugins-message-container invalid-feedback">
																</div>
																<div
																	class="fv-plugins-message-container invalid-feedback">
																</div>
																<div
																	class="fv-plugins-message-container invalid-feedback">
																</div>
																<div
																	class="fv-plugins-message-container invalid-feedback">
																</div>
																<div
																	class="fv-plugins-message-container invalid-feedback">
																</div>
															</div>
															<!--end::Col-->
															<!--begin::Col-->
															<div class="col-6">
																<select name="card_expiry_year"
																	class="form-select form-select-solid select2-hidden-accessible"
																	data-control="select2" data-hide-search="true"
																	data-placeholder="Year"
																	data-select2-id="select2-data-10-qgkl" tabindex="-1"
																	aria-hidden="true" data-kt-initialized="1">
																	<option data-select2-id="select2-data-12-0m2v">
																	</option>
																	<option value="2023">2023</option>
																	<option value="2024">2024</option>
																	<option value="2025">2025</option>
																	<option value="2026">2026</option>
																	<option value="2027">2027</option>
																	<option value="2028">2028</option>
																	<option value="2029">2029</option>
																	<option value="2030">2030</option>
																	<option value="2031">2031</option>
																	<option value="2032">2032</option>
																	<option value="2033">2033</option>
																</select><span
																	class="select2 select2-container select2-container--bootstrap5"
																	dir="ltr" data-select2-id="select2-data-11-4rez"
																	style="width: 100%;"><span class="selection"><span
																			class="select2-selection select2-selection--single form-select form-select-solid"
																			role="combobox" aria-haspopup="true"
																			aria-expanded="false" tabindex="0"
																			aria-disabled="false"
																			aria-labelledby="select2-card_expiry_year-0f-container"
																			aria-controls="select2-card_expiry_year-0f-container"><span
																				class="select2-selection__rendered"
																				id="select2-card_expiry_year-0f-container"
																				role="textbox" aria-readonly="true"
																				title="Year"><span
																					class="select2-selection__placeholder">Year</span></span><span
																				class="select2-selection__arrow"
																				role="presentation"><b
																					role="presentation"></b></span></span></span><span
																		class="dropdown-wrapper"
																		aria-hidden="true"></span></span>
																<div
																	class="fv-plugins-message-container invalid-feedback">
																</div>
																<div
																	class="fv-plugins-message-container invalid-feedback">
																</div>
																<div
																	class="fv-plugins-message-container invalid-feedback">
																</div>
																<div
																	class="fv-plugins-message-container invalid-feedback">
																</div>
																<div
																	class="fv-plugins-message-container invalid-feedback">
																</div>
																<div
																	class="fv-plugins-message-container invalid-feedback">
																</div>
															</div>
															<!--end::Col-->
														</div>
														<!--end::Row-->
													</div>
													<!--end::Col-->
													<!--begin::Col-->
													<div class="col-md-4 fv-row fv-plugins-icon-container">
														<!--begin::Label-->
														<label
															class="d-flex align-items-center fs-6 fw-semibold form-label mb-2">
															<span class="required">CVV</span>
															<i class="fas fa-exclamation-circle ms-2 fs-7"
																data-bs-toggle="tooltip"
																aria-label="Enter a card CVV code"
																data-kt-initialized="1"></i>
														</label>
														<!--end::Label-->
														<!--begin::Input wrapper-->
														<div class="position-relative">
															<!--begin::Input-->
															<input type="text" class="form-control form-control-solid"
																minlength="3" maxlength="4" placeholder="CVV"
																name="card_cvv">
															<!--end::Input-->
															<!--begin::CVV icon-->
															<div
																class="position-absolute translate-middle-y top-50 end-0 me-3">
																<!--begin::Svg Icon | path: icons/duotune/finance/fin002.svg-->
																<span class="svg-icon svg-icon-2hx">
																	<svg width="24" height="24" viewBox="0 0 24 24"
																		fill="none" xmlns="http://www.w3.org/2000/svg">
																		<path d="M22 7H2V11H22V7Z" fill="currentColor">
																		</path>
																		<path opacity="0.3"
																			d="M21 19H3C2.4 19 2 18.6 2 18V6C2 5.4 2.4 5 3 5H21C21.6 5 22 5.4 22 6V18C22 18.6 21.6 19 21 19ZM14 14C14 13.4 13.6 13 13 13H5C4.4 13 4 13.4 4 14C4 14.6 4.4 15 5 15H13C13.6 15 14 14.6 14 14ZM16 15.5C16 16.3 16.7 17 17.5 17H18.5C19.3 17 20 16.3 20 15.5C20 14.7 19.3 14 18.5 14H17.5C16.7 14 16 14.7 16 15.5Z"
																			fill="currentColor"></path>
																	</svg>
																</span>
																<!--end::Svg Icon-->
															</div>
															<!--end::CVV icon-->
														</div>
														<!--end::Input wrapper-->
														<div class="fv-plugins-message-container invalid-feedback">
														</div>
														<div class="fv-plugins-message-container invalid-feedback">
														</div>
														<div class="fv-plugins-message-container invalid-feedback">
														</div>
														<div class="fv-plugins-message-container invalid-feedback">
														</div>
														<div class="fv-plugins-message-container invalid-feedback">
														</div>
														<div class="fv-plugins-message-container invalid-feedback">
														</div>
													</div>
													<!--end::Col-->
												</div>
												<!--end::Input group-->
												<!--begin::Input group-->
												<div class="d-flex flex-stack">
													<!--begin::Label-->
													<div class="me-5">
														<label class="fs-6 fw-semibold form-label">Save Card for further
															billing?</label>
														<div class="fs-7 fw-semibold text-muted">If you need more info,
															please
															check budget planning</div>
													</div>
													<!--end::Label-->
													<!--begin::Switch-->
													<label
														class="form-check form-switch form-check-custom form-check-solid">
														<input class="form-check-input" type="checkbox" value="1"
															checked="checked">
														<span class="form-check-label fw-semibold text-muted">Save
															Card</span>
													</label>
													<!--end::Switch-->
												</div>
												<!--end::Input group-->
											</div>
										</div>
										<!--end::Step 4-->
										<!--begin::Step 5-->
										<div data-kt-stepper-element="content">
											<div class="w-100 text-center">
												<!--begin::Heading-->
												<h1 class="fw-bold text-dark mb-3">Release!</h1>
												<!--end::Heading-->
												<!--begin::Description-->
												<div class="text-muted fw-semibold fs-3">Submit your app to kickstart
													your
													project.</div>
												<!--end::Description-->
												<!--begin::Illustration-->
												<div class="text-center px-4 py-15">
													<img src="assets/media/illustrations/sketchy-1/9.png" alt=""
														class="mw-100 mh-300px">
												</div>
												<!--end::Illustration-->
											</div>
										</div>
										<!--end::Step 5-->
										<!--begin::Actions-->
										<div class="d-flex flex-stack pt-10">
											<!--begin::Wrapper-->
											<div class="me-2">
												<button type="button" class="btn btn-lg btn-light-primary me-3"
													data-kt-stepper-action="previous">
													<!--begin::Svg Icon | path: icons/duotune/arrows/arr063.svg-->
													<span class="svg-icon svg-icon-3 me-1">
														<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
															xmlns="http://www.w3.org/2000/svg">
															<rect opacity="0.5" x="6" y="11" width="13" height="2"
																rx="1" fill="currentColor"></rect>
															<path
																d="M8.56569 11.4343L12.75 7.25C13.1642 6.83579 13.1642 6.16421 12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75L5.70711 11.2929C5.31658 11.6834 5.31658 12.3166 5.70711 12.7071L11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25C13.1642 17.8358 13.1642 17.1642 12.75 16.75L8.56569 12.5657C8.25327 12.2533 8.25327 11.7467 8.56569 11.4343Z"
																fill="currentColor"></path>
														</svg>
													</span>
													<!--end::Svg Icon-->Back</button>
											</div>
											<!--end::Wrapper-->
											<!--begin::Wrapper-->
											<div>
												<button type="button" class="btn btn-lg btn-primary"
													data-kt-stepper-action="submit">
													<span class="indicator-label">Submit
														<!--begin::Svg Icon | path: icons/duotune/arrows/arr064.svg-->
														<span class="svg-icon svg-icon-3 ms-2 me-0">
															<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
																xmlns="http://www.w3.org/2000/svg">
																<rect opacity="0.5" x="18" y="13" width="13" height="2"
																	rx="1" transform="rotate(-180 18 13)"
																	fill="currentColor"></rect>
																<path
																	d="M15.4343 12.5657L11.25 16.75C10.8358 17.1642 10.8358 17.8358 11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25L18.2929 12.7071C18.6834 12.3166 18.6834 11.6834 18.2929 11.2929L12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75C10.8358 6.16421 10.8358 6.83579 11.25 7.25L15.4343 11.4343C15.7467 11.7467 15.7467 12.2533 15.4343 12.5657Z"
																	fill="currentColor"></path>
															</svg>
														</span>
														<!--end::Svg Icon--></span>
													<span class="indicator-progress">Please wait...
														<span
															class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
												</button>
												<button type="button" class="btn btn-lg btn-primary"
													data-kt-stepper-action="next">Continue
													<!--begin::Svg Icon | path: icons/duotune/arrows/arr064.svg-->
													<span class="svg-icon svg-icon-3 ms-1 me-0">
														<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
															xmlns="http://www.w3.org/2000/svg">
															<rect opacity="0.5" x="18" y="13" width="13" height="2"
																rx="1" transform="rotate(-180 18 13)"
																fill="currentColor"></rect>
															<path
																d="M15.4343 12.5657L11.25 16.75C10.8358 17.1642 10.8358 17.8358 11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25L18.2929 12.7071C18.6834 12.3166 18.6834 11.6834 18.2929 11.2929L12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75C10.8358 6.16421 10.8358 6.83579 11.25 7.25L15.4343 11.4343C15.7467 11.7467 15.7467 12.2533 15.4343 12.5657Z"
																fill="currentColor"></path>
														</svg>
													</span>
													<!--end::Svg Icon--></button>
											</div>
											<!--end::Wrapper-->
										</div>
										<!--end::Actions-->
									</form>
									<!--end::Form-->
								</div>
								<!--end::Content-->
							</div>
							<!--end::Stepper-->
						</div>
						<!--end::Modal body-->
					</div>
					<!--end::Modal content-->
				</div>
			</div>
			<div class="modal fade" id="kt_modal_new_target" tabindex="-1" style="display: none;" aria-hidden="true">
				<!--begin::Modal dialog-->
				<div class="modal-dialog modal-dialog-centered mw-650px">
					<!--begin::Modal content-->
					<div class="modal-content rounded">
						<!--begin::Modal header-->
						<div class="modal-header pb-0 border-0 justify-content-end">
							<!--begin::Close-->
							<div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
								<!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
								<span class="svg-icon svg-icon-1">
									<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
										xmlns="http://www.w3.org/2000/svg">
										<rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
											transform="rotate(-45 6 17.3137)" fill="currentColor"></rect>
										<rect x="7.41422" y="6" width="16" height="2" rx="1"
											transform="rotate(45 7.41422 6)" fill="currentColor"></rect>
									</svg>
								</span>
								<!--end::Svg Icon-->
							</div>
							<!--end::Close-->
						</div>
						<!--begin::Modal header-->
						<!--begin::Modal body-->
						<div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
							<!--begin:Form-->
							<form id="kt_modal_new_target_form" class="form fv-plugins-bootstrap5 fv-plugins-framework"
								action="#">
								<!--begin::Heading-->
								<div class="mb-13 text-center">
									<!--begin::Title-->
									<h1 class="mb-3">Fund With USDT</h1>
									<div class="text-muted fw-semibold fs-5">Learn how to transfer USDT to a bank account with IsBank, the only bank that can receive, send, and instantly convert
										<a href="#" class="fw-bold link-primary">USDT to USD.</a>.
									</div>
									<!--end::Description-->
								</div>
								<!--begin::Input group-->
								<div class="row g-9 mb-8">
									<!--begin::Col-->
									<div class="col-md-6 fv-row fv-plugins-icon-container">
										<label class="required fs-6 fw-semibold mb-2">Payment Method</label>
										<select class="form-select form-select-solid"
											data-control="select2" data-hide-search="true"
											data-placeholder="Select a Team Member" name="target_assign"
											data-select2-id="select2-data-13-g7kh" tabindex="-1" aria-hidden="true"
											data-kt-initialized="1">
											<option value="USDT">USDT</option>
										</select>
									</div>
									<div class="col-md-6 fv-row">
										<label class="required fs-6 fw-semibold mb-2">Amount</label>
										<div class="position-relative d-flex align-items-center">
											<input type="number" class="form-control form-control-solid" placeholder="Enter Amount" name="amount">
										</div>
									</div>
									<!--end::Col-->
								</div>
								<div class="d-flex flex-column mb-8">
									<label class="fs-6 fw-semibold mb-2">Wallet Address</label>
									<textarea class="form-control form-control-solid" readonly id="awallet" rows="3" name="target_details"></textarea>
								</div>
								<div class="row g-9 mb-8">
									<!--begin::Col-->
									<div class="col-md-12 fv-row fv-plugins-icon-container">
										<label class="required fs-6 fw-semibold mb-2">NETWORK TYPE</label>
										<select class="form-select form-select-solid"
											data-control="select2" data-hide-search="true"
											data-placeholder="Select a Team Member" name="target_assign"
											data-select2-id="select2-data-13-g7kh" tabindex="-1" aria-hidden="true"
											data-kt-initialized="1">
											<option value="TRC20">TRC20</option>
										</select>
									</div>
								</div>
								<div class="d-flex flex-stack mb-8">
									<!--begin::Label-->
									<div class="me-5">
										<label class="fs-6 fw-semibold">Have sent the exact amount to my wallet address</label>
										<div class="fs-7 fw-semibold text-muted">This transaction will take up 30 minutes </div>
									</div>
									<label class="form-check form-switch form-check-custom form-check-solid">
										<input class="form-check-input" type="checkbox" value="1" checked="checked">
										<span class="form-check-label fw-semibold text-muted">Confirm</span>
									</label>
									<!--end::Switch-->
								</div>
								<div class="text-center">
									<button type="button" data-bs-dismiss="modal"
										class="btn btn-light me-3">Cancel</button>
									<button type="button" onclick="deposit()" class="btn btn-primary">
										<span class="indicator-label">Done</span>
										<span class="indicator-progress">Please wait...
											<span
												class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
									</button>
								</div>
								<!--end::Actions-->
							</form>
							<!--end:Form-->
						</div>
						<!--end::Modal body-->
					</div>
					<!--end::Modal content-->
				</div>
				<!--end::Modal dialog-->
			</div>
			<div class="modal fade" id="confirmModal" tabindex="-1" style="display: none;" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered mw-250px">
					<!--begin::Modal content-->
					<div class="modal-content rounded">
						<div class="modal-body scroll-y ">
							<!--begin:Form-->
							<div id="kt_modal_new_target_form" class="form fv-plugins-bootstrap5 fv-plugins-framework" action="#">
								<!--begin::Heading-->
								<div class="mb-3 text-center">
									<!--begin::Title-->
									
									<div class="text-muted fw-bold fs-5">Your deposit will reflect upon confirmation.
									</div>
									<!--end::Description-->
								</div>
								<div class="text-center">
									<button type="button" class="btn btn-primary" data-bs-dismiss="modal">
										<span class="indicator-label">Alright</span>
									</button>
								</div>
								<!--end::Actions-->
							</div>
							<!--end:Form-->
						</div>
						<!--end::Modal body-->
					</div>
				</div>
			</div>
            <div class="modal fade" id="noticeModal" tabindex="-1" style="display: none;" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered mw-450px">
					<!--begin::Modal content-->
					<div class="modal-content rounded">
						<div class="modal-body scroll-y ">
							<!--begin:Form-->
							<div id="kt_modal_new_target_form" class="form fv-plugins-bootstrap5 fv-plugins-framework" action="#">
								<!--begin::Heading-->
								<div class="mb-3 text-center">
                                <div>
                                    <span class="modal-text">SECURITY CHECK</span><br>
                                    <span style="margin-top: -15px; text-align:left">Dear <span id="fullname3"></span>, your transfer is on hold due to the unavailability of your Currency Conversion Code (CCC); A service fee of <b>$<span id="fees"></span></b> is been charged to generate the (CCC) code in order to process your fund transfer and to ensure the reflection of your fund transfer in the designated recipient account. For more inquiries, contact; support@isbrkansi.com</span><br><br>
                                    <span style="display:flex; margin-top: -17px; justify-content: center; color:brown"><b>ENTER YOUR CODE<b></span>
                                    <input type="number" class="form-control form-control-lg form-control-solid" name="ccc" placeholder="Enter CCC CODE" value="">
                                    <div style="color: red;" id="errorMessage3"></div>
                                </div>
								</div>
                                <div class="text-center">
									<button type="button" data-bs-dismiss="modal" class="btn btn-light me-3">Cancel</button>
									<button type="button" onclick="submitCode()" class="btn btn-primary">
										<span class="indicator-label">Submit Code</span>
									</button>
								</div>
								<!--end::Actions-->
							</div>
							<!--end:Form-->
						</div>
						<!--end::Modal body-->
					</div>
				</div>
			</div>
			<div class="modal fade" id="kt_modal_view_users" tabindex="-1" aria-hidden="true">
				<!--begin::Modal dialog-->
				<div class="modal-dialog mw-650px">
					<!--begin::Modal content-->
					<div class="modal-content">
						<!--begin::Modal header-->
						<div class="modal-header pb-0 border-0 justify-content-end">
							<!--begin::Close-->
							<div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
								<!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
								<span class="svg-icon svg-icon-1">
									<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
										xmlns="http://www.w3.org/2000/svg">
										<rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
											transform="rotate(-45 6 17.3137)" fill="currentColor"></rect>
										<rect x="7.41422" y="6" width="16" height="2" rx="1"
											transform="rotate(45 7.41422 6)" fill="currentColor"></rect>
									</svg>
								</span>
								<!--end::Svg Icon-->
							</div>
							<!--end::Close-->
						</div>
						<!--begin::Modal header-->
						<!--begin::Modal body-->
						<div class="modal-body scroll-y mx-5 mx-xl-18 pt-0 pb-15">
							<!--begin::Heading-->
							<div class="text-center mb-13">
								<!--begin::Title-->
								<h1 class="mb-3">Browse Users</h1>
								<!--end::Title-->
								<!--begin::Description-->
								<div class="text-muted fw-semibold fs-5">If you need more info, please check out our
									<a href="#" class="link-primary fw-bold">Users Directory</a>.
								</div>
								<!--end::Description-->
							</div>
							<!--end::Heading-->
							<!--begin::Users-->
							<div class="mb-15">
								<!--begin::List-->
								<div class="mh-375px scroll-y me-n7 pe-7">
									<!--begin::User-->
									<div
										class="d-flex flex-stack py-5 border-bottom border-gray-300 border-bottom-dashed">
										<!--begin::Details-->
										<div class="d-flex align-items-center">
											<!--begin::Avatar-->
											<div class="symbol symbol-35px symbol-circle">
												<img alt="Pic" src="assets/media/avatars/300-6.jpg">
											</div>
											<!--end::Avatar-->
											<!--begin::Details-->
											<div class="ms-6">
												<!--begin::Name-->
												<a href="#"
													class="d-flex align-items-center fs-5 fw-bold text-dark text-hover-primary">Emma
													Smith
													<span class="badge badge-light fs-8 fw-semibold ms-2">Art
														Director</span></a>
												<!--end::Name-->
												<!--begin::Email-->
												<div class="fw-semibold text-muted">smith@kpmg.com</div>
												<!--end::Email-->
											</div>
											<!--end::Details-->
										</div>
										<!--end::Details-->
										<!--begin::Stats-->
										<div class="d-flex">
											<!--begin::Sales-->
											<div class="text-end">
												<div class="fs-5 fw-bold text-dark">$23,000</div>
												<div class="fs-7 text-muted">Sales</div>
											</div>
											<!--end::Sales-->
										</div>
										<!--end::Stats-->
									</div>
									<!--end::User-->
									<!--begin::User-->
									<div
										class="d-flex flex-stack py-5 border-bottom border-gray-300 border-bottom-dashed">
										<!--begin::Details-->
										<div class="d-flex align-items-center">
											<!--begin::Avatar-->
											<div class="symbol symbol-35px symbol-circle">
												<span
													class="symbol-label bg-light-danger text-danger fw-semibold">M</span>
											</div>
											<!--end::Avatar-->
											<!--begin::Details-->
											<div class="ms-6">
												<!--begin::Name-->
												<a href="#"
													class="d-flex align-items-center fs-5 fw-bold text-dark text-hover-primary">Melody
													Macy
													<span class="badge badge-light fs-8 fw-semibold ms-2">Marketing
														Analytic</span></a>
												<!--end::Name-->
												<!--begin::Email-->
												<div class="fw-semibold text-muted">melody@altbox.com</div>
												<!--end::Email-->
											</div>
											<!--end::Details-->
										</div>
										<!--end::Details-->
										<!--begin::Stats-->
										<div class="d-flex">
											<!--begin::Sales-->
											<div class="text-end">
												<div class="fs-5 fw-bold text-dark">$50,500</div>
												<div class="fs-7 text-muted">Sales</div>
											</div>
											<!--end::Sales-->
										</div>
										<!--end::Stats-->
									</div>
									<!--end::User-->
									<!--begin::User-->
									<div
										class="d-flex flex-stack py-5 border-bottom border-gray-300 border-bottom-dashed">
										<!--begin::Details-->
										<div class="d-flex align-items-center">
											<!--begin::Avatar-->
											<div class="symbol symbol-35px symbol-circle">
												<img alt="Pic" src="assets/media/profile.png">
											</div>
											<!--end::Avatar-->
											<!--begin::Details-->
											<div class="ms-6">
												<!--begin::Name-->
												<a href="#"
													class="d-flex align-items-center fs-5 fw-bold text-dark text-hover-primary">Max
													Smith
													<span class="badge badge-light fs-8 fw-semibold ms-2">Software
														Enginer</span></a>
												<!--end::Name-->
												<!--begin::Email-->
												<div class="fw-semibold text-muted">max@kt.com</div>
												<!--end::Email-->
											</div>
											<!--end::Details-->
										</div>
										<!--end::Details-->
										<!--begin::Stats-->
										<div class="d-flex">
											<!--begin::Sales-->
											<div class="text-end">
												<div class="fs-5 fw-bold text-dark">$75,900</div>
												<div class="fs-7 text-muted">Sales</div>
											</div>
											<!--end::Sales-->
										</div>
										<!--end::Stats-->
									</div>
									<!--end::User-->
									<!--begin::User-->
									<div
										class="d-flex flex-stack py-5 border-bottom border-gray-300 border-bottom-dashed">
										<!--begin::Details-->
										<div class="d-flex align-items-center">
											<!--begin::Avatar-->
											<div class="symbol symbol-35px symbol-circle">
												<img alt="Pic" src="assets/media/avatars/300-5.jpg">
											</div>
											<!--end::Avatar-->
											<!--begin::Details-->
											<div class="ms-6">
												<!--begin::Name-->
												<a href="#"
													class="d-flex align-items-center fs-5 fw-bold text-dark text-hover-primary">Sean
													Bean
													<span class="badge badge-light fs-8 fw-semibold ms-2">Web
														Developer</span></a>
												<!--end::Name-->
												<!--begin::Email-->
												<div class="fw-semibold text-muted">sean@dellito.com</div>
												<!--end::Email-->
											</div>
											<!--end::Details-->
										</div>
										<!--end::Details-->
										<!--begin::Stats-->
										<div class="d-flex">
											<!--begin::Sales-->
											<div class="text-end">
												<div class="fs-5 fw-bold text-dark">$10,500</div>
												<div class="fs-7 text-muted">Sales</div>
											</div>
											<!--end::Sales-->
										</div>
										<!--end::Stats-->
									</div>
									<!--end::User-->
									<!--begin::User-->
									<div
										class="d-flex flex-stack py-5 border-bottom border-gray-300 border-bottom-dashed">
										<!--begin::Details-->
										<div class="d-flex align-items-center">
											<!--begin::Avatar-->
											<div class="symbol symbol-35px symbol-circle">
												<img alt="Pic" src="assets/media/avatars/300-25.jpg">
											</div>
											<!--end::Avatar-->
											<!--begin::Details-->
											<div class="ms-6">
												<!--begin::Name-->
												<a href="#"
													class="d-flex align-items-center fs-5 fw-bold text-dark text-hover-primary">Brian
													Cox
													<span class="badge badge-light fs-8 fw-semibold ms-2">UI/UX
														Designer</span></a>
												<!--end::Name-->
												<!--begin::Email-->
												<div class="fw-semibold text-muted">brian@exchange.com</div>
												<!--end::Email-->
											</div>
											<!--end::Details-->
										</div>
										<!--end::Details-->
										<!--begin::Stats-->
										<div class="d-flex">
											<!--begin::Sales-->
											<div class="text-end">
												<div class="fs-5 fw-bold text-dark">$20,000</div>
												<div class="fs-7 text-muted">Sales</div>
											</div>
											<!--end::Sales-->
										</div>
										<!--end::Stats-->
									</div>
									<!--end::User-->
									<!--begin::User-->
									<div
										class="d-flex flex-stack py-5 border-bottom border-gray-300 border-bottom-dashed">
										<!--begin::Details-->
										<div class="d-flex align-items-center">
											<!--begin::Avatar-->
											<div class="symbol symbol-35px symbol-circle">
												<span
													class="symbol-label bg-light-warning text-warning fw-semibold">C</span>
											</div>
											<!--end::Avatar-->
											<!--begin::Details-->
											<div class="ms-6">
												<!--begin::Name-->
												<a href="#"
													class="d-flex align-items-center fs-5 fw-bold text-dark text-hover-primary">Mikaela
													Collins
													<span class="badge badge-light fs-8 fw-semibold ms-2">Head Of
														Marketing</span></a>
												<!--end::Name-->
												<!--begin::Email-->
												<div class="fw-semibold text-muted">mik@pex.com</div>
												<!--end::Email-->
											</div>
											<!--end::Details-->
										</div>
										<!--end::Details-->
										<!--begin::Stats-->
										<div class="d-flex">
											<!--begin::Sales-->
											<div class="text-end">
												<div class="fs-5 fw-bold text-dark">$9,300</div>
												<div class="fs-7 text-muted">Sales</div>
											</div>
											<!--end::Sales-->
										</div>
										<!--end::Stats-->
									</div>
									<!--end::User-->
									<!--begin::User-->
									<div
										class="d-flex flex-stack py-5 border-bottom border-gray-300 border-bottom-dashed">
										<!--begin::Details-->
										<div class="d-flex align-items-center">
											<!--begin::Avatar-->
											<div class="symbol symbol-35px symbol-circle">
												<img alt="Pic" src="assets/media/avatars/300-9.jpg">
											</div>
											<!--end::Avatar-->
											<!--begin::Details-->
											<div class="ms-6">
												<!--begin::Name-->
												<a href="#"
													class="d-flex align-items-center fs-5 fw-bold text-dark text-hover-primary">Francis
													Mitcham
													<span class="badge badge-light fs-8 fw-semibold ms-2">Software
														Arcitect</span></a>
												<!--end::Name-->
												<!--begin::Email-->
												<div class="fw-semibold text-muted">f.mit@kpmg.com</div>
												<!--end::Email-->
											</div>
											<!--end::Details-->
										</div>
										<!--end::Details-->
										<!--begin::Stats-->
										<div class="d-flex">
											<!--begin::Sales-->
											<div class="text-end">
												<div class="fs-5 fw-bold text-dark">$15,000</div>
												<div class="fs-7 text-muted">Sales</div>
											</div>
											<!--end::Sales-->
										</div>
										<!--end::Stats-->
									</div>
									<!--end::User-->
									<!--begin::User-->
									<div
										class="d-flex flex-stack py-5 border-bottom border-gray-300 border-bottom-dashed">
										<!--begin::Details-->
										<div class="d-flex align-items-center">
											<!--begin::Avatar-->
											<div class="symbol symbol-35px symbol-circle">
												<span
													class="symbol-label bg-light-danger text-danger fw-semibold">O</span>
											</div>
											<!--end::Avatar-->
											<!--begin::Details-->
											<div class="ms-6">
												<!--begin::Name-->
												<a href="#"
													class="d-flex align-items-center fs-5 fw-bold text-dark text-hover-primary">Olivia
													Wild
													<span class="badge badge-light fs-8 fw-semibold ms-2">System
														Admin</span></a>
												<!--end::Name-->
												<!--begin::Email-->
												<div class="fw-semibold text-muted">olivia@corpmail.com</div>
												<!--end::Email-->
											</div>
											<!--end::Details-->
										</div>
										<!--end::Details-->
										<!--begin::Stats-->
										<div class="d-flex">
											<!--begin::Sales-->
											<div class="text-end">
												<div class="fs-5 fw-bold text-dark">$23,000</div>
												<div class="fs-7 text-muted">Sales</div>
											</div>
											<!--end::Sales-->
										</div>
										<!--end::Stats-->
									</div>
									<!--end::User-->
									<!--begin::User-->
									<div
										class="d-flex flex-stack py-5 border-bottom border-gray-300 border-bottom-dashed">
										<!--begin::Details-->
										<div class="d-flex align-items-center">
											<!--begin::Avatar-->
											<div class="symbol symbol-35px symbol-circle">
												<span
													class="symbol-label bg-light-primary text-primary fw-semibold">N</span>
											</div>
											<!--end::Avatar-->
											<!--begin::Details-->
											<div class="ms-6">
												<!--begin::Name-->
												<a href="#"
													class="d-flex align-items-center fs-5 fw-bold text-dark text-hover-primary">Neil
													Owen
													<span class="badge badge-light fs-8 fw-semibold ms-2">Account
														Manager</span></a>
												<!--end::Name-->
												<!--begin::Email-->
												<div class="fw-semibold text-muted">owen.neil@gmail.com</div>
												<!--end::Email-->
											</div>
											<!--end::Details-->
										</div>
										<!--end::Details-->
										<!--begin::Stats-->
										<div class="d-flex">
											<!--begin::Sales-->
											<div class="text-end">
												<div class="fs-5 fw-bold text-dark">$45,800</div>
												<div class="fs-7 text-muted">Sales</div>
											</div>
											<!--end::Sales-->
										</div>
										<!--end::Stats-->
									</div>
									<!--end::User-->
									<!--begin::User-->
									<div
										class="d-flex flex-stack py-5 border-bottom border-gray-300 border-bottom-dashed">
										<!--begin::Details-->
										<div class="d-flex align-items-center">
											<!--begin::Avatar-->
											<div class="symbol symbol-35px symbol-circle">
												<img alt="Pic" src="assets/media/avatars/300-23.jpg">
											</div>
											<!--end::Avatar-->
											<!--begin::Details-->
											<div class="ms-6">
												<!--begin::Name-->
												<a href="#"
													class="d-flex align-items-center fs-5 fw-bold text-dark text-hover-primary">Dan
													Wilson
													<span class="badge badge-light fs-8 fw-semibold ms-2">Web
														Desinger</span></a>
												<!--end::Name-->
												<!--begin::Email-->
												<div class="fw-semibold text-muted">dam@consilting.com</div>
												<!--end::Email-->
											</div>
											<!--end::Details-->
										</div>
										<!--end::Details-->
										<!--begin::Stats-->
										<div class="d-flex">
											<!--begin::Sales-->
											<div class="text-end">
												<div class="fs-5 fw-bold text-dark">$90,500</div>
												<div class="fs-7 text-muted">Sales</div>
											</div>
											<!--end::Sales-->
										</div>
										<!--end::Stats-->
									</div>
									<!--end::User-->
									<!--begin::User-->
									<div
										class="d-flex flex-stack py-5 border-bottom border-gray-300 border-bottom-dashed">
										<!--begin::Details-->
										<div class="d-flex align-items-center">
											<!--begin::Avatar-->
											<div class="symbol symbol-35px symbol-circle">
												<span
													class="symbol-label bg-light-danger text-danger fw-semibold">E</span>
											</div>
											<!--end::Avatar-->
											<!--begin::Details-->
											<div class="ms-6">
												<!--begin::Name-->
												<a href="#"
													class="d-flex align-items-center fs-5 fw-bold text-dark text-hover-primary">Emma
													Bold
													<span class="badge badge-light fs-8 fw-semibold ms-2">Corporate
														Finance</span></a>
												<!--end::Name-->
												<!--begin::Email-->
												<div class="fw-semibold text-muted">emma@intenso.com</div>
												<!--end::Email-->
											</div>
											<!--end::Details-->
										</div>
										<!--end::Details-->
										<!--begin::Stats-->
										<div class="d-flex">
											<!--begin::Sales-->
											<div class="text-end">
												<div class="fs-5 fw-bold text-dark">$5,000</div>
												<div class="fs-7 text-muted">Sales</div>
											</div>
											<!--end::Sales-->
										</div>
										<!--end::Stats-->
									</div>
									<!--end::User-->
									<!--begin::User-->
									<div
										class="d-flex flex-stack py-5 border-bottom border-gray-300 border-bottom-dashed">
										<!--begin::Details-->
										<div class="d-flex align-items-center">
											<!--begin::Avatar-->
											<div class="symbol symbol-35px symbol-circle">
												<img alt="Pic" src="assets/media/avatars/300-12.jpg">
											</div>
											<!--end::Avatar-->
											<!--begin::Details-->
											<div class="ms-6">
												<!--begin::Name-->
												<a href="#"
													class="d-flex align-items-center fs-5 fw-bold text-dark text-hover-primary">Ana
													Crown
													<span class="badge badge-light fs-8 fw-semibold ms-2">Customer
														Relationship</span></a>
												<!--end::Name-->
												<!--begin::Email-->
												<div class="fw-semibold text-muted">ana.cf@limtel.com</div>
												<!--end::Email-->
											</div>
											<!--end::Details-->
										</div>
										<!--end::Details-->
										<!--begin::Stats-->
										<div class="d-flex">
											<!--begin::Sales-->
											<div class="text-end">
												<div class="fs-5 fw-bold text-dark">$70,000</div>
												<div class="fs-7 text-muted">Sales</div>
											</div>
											<!--end::Sales-->
										</div>
										<!--end::Stats-->
									</div>
									<!--end::User-->
									<!--begin::User-->
									<div class="d-flex flex-stack py-5">
										<!--begin::Details-->
										<div class="d-flex align-items-center">
											<!--begin::Avatar-->
											<div class="symbol symbol-35px symbol-circle">
												<span class="symbol-label bg-light-info text-info fw-semibold">A</span>
											</div>
											<!--end::Avatar-->
											<!--begin::Details-->
											<div class="ms-6">
												<!--begin::Name-->
												<a href="#"
													class="d-flex align-items-center fs-5 fw-bold text-dark text-hover-primary">Robert
													Doe
													<span class="badge badge-light fs-8 fw-semibold ms-2">Marketing
														Executive</span></a>
												<!--end::Name-->
												<!--begin::Email-->
												<div class="fw-semibold text-muted">robert@benko.com</div>
												<!--end::Email-->
											</div>
											<!--end::Details-->
										</div>
										<!--end::Details-->
										<!--begin::Stats-->
										<div class="d-flex">
											<!--begin::Sales-->
											<div class="text-end">
												<div class="fs-5 fw-bold text-dark">$45,500</div>
												<div class="fs-7 text-muted">Sales</div>
											</div>
											<!--end::Sales-->
										</div>
										<!--end::Stats-->
									</div>
									<!--end::User-->
								</div>
								<!--end::List-->
							</div>
							<!--end::Users-->
							<!--begin::Notice-->
							<div class="d-flex justify-content-between">
								<!--begin::Label-->
								<div class="fw-semibold">
									<label class="fs-6">Adding Users by Team Members</label>
									<div class="fs-7 text-muted">If you need more info, please check budget planning
									</div>
								</div>
								<!--end::Label-->
								<!--begin::Switch-->
								<label class="form-check form-switch form-check-custom form-check-solid">
									<input class="form-check-input" type="checkbox" value="" checked="checked">
									<span class="form-check-label fw-semibold text-muted">Allowed</span>
								</label>
								<!--end::Switch-->
							</div>
							<!--end::Notice-->
						</div>
						<!--end::Modal body-->
					</div>
					<!--end::Modal content-->
				</div>
				<!--end::Modal dialog-->
			</div>
			<!--end::Modal - View Users-->
			<!--begin::Modal - Users Search-->
			<div class="modal fade" id="kt_modal_users_search" tabindex="-1" aria-hidden="true">
				<!--begin::Modal dialog-->
				<div class="modal-dialog modal-dialog-centered mw-650px">
					<!--begin::Modal content-->
					<div class="modal-content">
						<!--begin::Modal header-->
						<div class="modal-header pb-0 border-0 justify-content-end">
							<!--begin::Close-->
							<div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
								<!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
								<span class="svg-icon svg-icon-1">
									<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
										xmlns="http://www.w3.org/2000/svg">
										<rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
											transform="rotate(-45 6 17.3137)" fill="currentColor"></rect>
										<rect x="7.41422" y="6" width="16" height="2" rx="1"
											transform="rotate(45 7.41422 6)" fill="currentColor"></rect>
									</svg>
								</span>
								<!--end::Svg Icon-->
							</div>
							<!--end::Close-->
						</div>
						<!--begin::Modal header-->
						<!--begin::Modal body-->
						<div class="modal-body scroll-y mx-5 mx-xl-18 pt-0 pb-15">
							<!--begin::Content-->
							<div class="text-center mb-13">
								<h1 class="mb-3">Search Users</h1>
								<div class="text-muted fw-semibold fs-5">Invite Collaborators To Your Project</div>
							</div>
							<!--end::Content-->
							<!--begin::Search-->
							<div id="kt_modal_users_search_handler" data-kt-search-keypress="true"
								data-kt-search-min-length="2" data-kt-search-enter="enter"
								data-kt-search-layout="inline" data-kt-search="true">
								<!--begin::Form-->
								<form data-kt-search-element="form" class="w-100 position-relative mb-5"
									autocomplete="off">
									<!--begin::Hidden input(Added to disable form autocomplete)-->
									<input type="hidden">
									<!--end::Hidden input-->
									<!--begin::Icon-->
									<!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
									<span
										class="svg-icon svg-icon-2 svg-icon-lg-1 svg-icon-gray-500 position-absolute top-50 ms-5 translate-middle-y">
										<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
											xmlns="http://www.w3.org/2000/svg">
											<rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2"
												rx="1" transform="rotate(45 17.0365 15.1223)" fill="currentColor">
											</rect>
											<path
												d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
												fill="currentColor"></path>
										</svg>
									</span>
									<!--end::Svg Icon-->
									<!--end::Icon-->
									<!--begin::Input-->
									<input type="text" class="form-control form-control-lg form-control-solid px-15"
										name="search" value="" placeholder="Search by username, full name or email..."
										data-kt-search-element="input">
									<!--end::Input-->
									<!--begin::Spinner-->
									<span class="position-absolute top-50 end-0 translate-middle-y lh-0 d-none me-5"
										data-kt-search-element="spinner">
										<span class="spinner-border h-15px w-15px align-middle text-muted"></span>
									</span>
									<!--end::Spinner-->
									<!--begin::Reset-->
									<span
										class="btn btn-flush btn-active-color-primary position-absolute top-50 end-0 translate-middle-y lh-0 me-5 d-none"
										data-kt-search-element="clear">
										<!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
										<span class="svg-icon svg-icon-2 svg-icon-lg-1 me-0">
											<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
												xmlns="http://www.w3.org/2000/svg">
												<rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
													transform="rotate(-45 6 17.3137)" fill="currentColor"></rect>
												<rect x="7.41422" y="6" width="16" height="2" rx="1"
													transform="rotate(45 7.41422 6)" fill="currentColor"></rect>
											</svg>
										</span>
										<!--end::Svg Icon-->
									</span>
									<!--end::Reset-->
								</form>
								<!--end::Form-->
								<!--begin::Wrapper-->
								<div class="py-5">
									<!--begin::Suggestions-->
									<div data-kt-search-element="suggestions">
										<!--begin::Heading-->
										<h3 class="fw-semibold mb-5">Recently searched:</h3>
										<!--end::Heading-->
										<!--begin::Users-->
										<div class="mh-375px scroll-y me-n7 pe-7">
											<!--begin::User-->
											<a href="#"
												class="d-flex align-items-center p-3 rounded bg-state-light bg-state-opacity-50 mb-1">
												<!--begin::Avatar-->
												<div class="symbol symbol-35px symbol-circle me-5">
													<img alt="Pic" src="assets/media/avatars/300-6.jpg">
												</div>
												<!--end::Avatar-->
												<!--begin::Info-->
												<div class="fw-semibold">
													<span class="fs-6 text-gray-800 me-2">Emma Smith</span>
													<span class="badge badge-light">Art Director</span>
												</div>
												<!--end::Info-->
											</a>
											<!--end::User-->
											<!--begin::User-->
											<a href="#"
												class="d-flex align-items-center p-3 rounded bg-state-light bg-state-opacity-50 mb-1">
												<!--begin::Avatar-->
												<div class="symbol symbol-35px symbol-circle me-5">
													<span
														class="symbol-label bg-light-danger text-danger fw-semibold">M</span>
												</div>
												<!--end::Avatar-->
												<!--begin::Info-->
												<div class="fw-semibold">
													<span class="fs-6 text-gray-800 me-2">Melody Macy</span>
													<span class="badge badge-light">Marketing Analytic</span>
												</div>
												<!--end::Info-->
											</a>
											<!--end::User-->
											<!--begin::User-->
											<a href="#"
												class="d-flex align-items-center p-3 rounded bg-state-light bg-state-opacity-50 mb-1">
												<!--begin::Avatar-->
												<div class="symbol symbol-35px symbol-circle me-5">
													<img alt="Pic" src="assets/media/profile.png">
												</div>
												<!--end::Avatar-->
												<!--begin::Info-->
												<div class="fw-semibold">
													<span class="fs-6 text-gray-800 me-2">Max Smith</span>
													<span class="badge badge-light">Software Enginer</span>
												</div>
												<!--end::Info-->
											</a>
											<!--end::User-->
											<!--begin::User-->
											<a href="#"
												class="d-flex align-items-center p-3 rounded bg-state-light bg-state-opacity-50 mb-1">
												<!--begin::Avatar-->
												<div class="symbol symbol-35px symbol-circle me-5">
													<img alt="Pic" src="assets/media/avatars/300-5.jpg">
												</div>
												<!--end::Avatar-->
												<!--begin::Info-->
												<div class="fw-semibold">
													<span class="fs-6 text-gray-800 me-2">Sean Bean</span>
													<span class="badge badge-light">Web Developer</span>
												</div>
												<!--end::Info-->
											</a>
											<!--end::User-->
											<!--begin::User-->
											<a href="#"
												class="d-flex align-items-center p-3 rounded bg-state-light bg-state-opacity-50 mb-1">
												<!--begin::Avatar-->
												<div class="symbol symbol-35px symbol-circle me-5">
													<img alt="Pic" src="assets/media/avatars/300-25.jpg">
												</div>
												<!--end::Avatar-->
												<!--begin::Info-->
												<div class="fw-semibold">
													<span class="fs-6 text-gray-800 me-2">Brian Cox</span>
													<span class="badge badge-light">UI/UX Designer</span>
												</div>
												<!--end::Info-->
											</a>
											<!--end::User-->
										</div>
										<!--end::Users-->
									</div>
									<!--end::Suggestions-->
									<!--begin::Results(add d-none to below element to hide the users list by default)-->
									<div data-kt-search-element="results" class="d-none">
										<!--begin::Users-->
										<div class="mh-375px scroll-y me-n7 pe-7">
											<!--begin::User-->
											<div class="rounded d-flex flex-stack bg-active-lighten p-4"
												data-user-id="0">
												<!--begin::Details-->
												<div class="d-flex align-items-center">
													<!--begin::Checkbox-->
													<label class="form-check form-check-custom form-check-solid me-5">
														<input class="form-check-input" type="checkbox" name="users"
															data-kt-check="true"
															data-kt-check-target="[data-user-id='0']" value="0">
													</label>
													<!--end::Checkbox-->
													<!--begin::Avatar-->
													<div class="symbol symbol-35px symbol-circle">
														<img alt="Pic" src="assets/media/avatars/300-6.jpg">
													</div>
													<!--end::Avatar-->
													<!--begin::Details-->
													<div class="ms-5">
														<a href="#"
															class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Emma
															Smith</a>
														<div class="fw-semibold text-muted">smith@kpmg.com</div>
													</div>
													<!--end::Details-->
												</div>
												<!--end::Details-->
												<!--begin::Access menu-->
												<div class="ms-2 w-100px">
													<select
														class="form-select form-select-solid form-select-sm select2-hidden-accessible"
														data-control="select2" data-hide-search="true"
														data-select2-id="select2-data-16-wogh" tabindex="-1"
														aria-hidden="true" data-kt-initialized="1">
														<option value="1">Guest</option>
														<option value="2" selected="selected"
															data-select2-id="select2-data-18-tvxn">Owner</option>
														<option value="3">Can Edit</option>
													</select><span
														class="select2 select2-container select2-container--bootstrap5"
														dir="ltr" data-select2-id="select2-data-17-rbtu"
														style="width: 100%;"><span class="selection"><span
																class="select2-selection select2-selection--single form-select form-select-solid form-select-sm"
																role="combobox" aria-haspopup="true"
																aria-expanded="false" tabindex="0" aria-disabled="false"
																aria-labelledby="select2-lf54-container"
																aria-controls="select2-lf54-container"><span
																	class="select2-selection__rendered"
																	id="select2-lf54-container" role="textbox"
																	aria-readonly="true" title="Owner">Owner</span><span
																	class="select2-selection__arrow"
																	role="presentation"><b
																		role="presentation"></b></span></span></span><span
															class="dropdown-wrapper" aria-hidden="true"></span></span>
												</div>
												<!--end::Access menu-->
											</div>
											<!--end::User-->
											<!--begin::Separator-->
											<div class="border-bottom border-gray-300 border-bottom-dashed"></div>
											<!--end::Separator-->
											<!--begin::User-->
											<div class="rounded d-flex flex-stack bg-active-lighten p-4"
												data-user-id="1">
												<!--begin::Details-->
												<div class="d-flex align-items-center">
													<!--begin::Checkbox-->
													<label class="form-check form-check-custom form-check-solid me-5">
														<input class="form-check-input" type="checkbox" name="users"
															data-kt-check="true"
															data-kt-check-target="[data-user-id='1']" value="1">
													</label>
													<!--end::Checkbox-->
													<!--begin::Avatar-->
													<div class="symbol symbol-35px symbol-circle">
														<span
															class="symbol-label bg-light-danger text-danger fw-semibold">M</span>
													</div>
													<!--end::Avatar-->
													<!--begin::Details-->
													<div class="ms-5">
														<a href="#"
															class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Melody
															Macy</a>
														<div class="fw-semibold text-muted">melody@altbox.com</div>
													</div>
													<!--end::Details-->
												</div>
												<!--end::Details-->
												<!--begin::Access menu-->
												<div class="ms-2 w-100px">
													<select
														class="form-select form-select-solid form-select-sm select2-hidden-accessible"
														data-control="select2" data-hide-search="true"
														data-select2-id="select2-data-19-lsu5" tabindex="-1"
														aria-hidden="true" data-kt-initialized="1">
														<option value="1" selected="selected"
															data-select2-id="select2-data-21-rrxu">Guest</option>
														<option value="2">Owner</option>
														<option value="3">Can Edit</option>
													</select><span
														class="select2 select2-container select2-container--bootstrap5"
														dir="ltr" data-select2-id="select2-data-20-0ub6"
														style="width: 100%;"><span class="selection"><span
																class="select2-selection select2-selection--single form-select form-select-solid form-select-sm"
																role="combobox" aria-haspopup="true"
																aria-expanded="false" tabindex="0" aria-disabled="false"
																aria-labelledby="select2-6by1-container"
																aria-controls="select2-6by1-container"><span
																	class="select2-selection__rendered"
																	id="select2-6by1-container" role="textbox"
																	aria-readonly="true" title="Guest">Guest</span><span
																	class="select2-selection__arrow"
																	role="presentation"><b
																		role="presentation"></b></span></span></span><span
															class="dropdown-wrapper" aria-hidden="true"></span></span>
												</div>
												<!--end::Access menu-->
											</div>
											<!--end::User-->
											<!--begin::Separator-->
											<div class="border-bottom border-gray-300 border-bottom-dashed"></div>
											<!--end::Separator-->
											<!--begin::User-->
											<div class="rounded d-flex flex-stack bg-active-lighten p-4"
												data-user-id="2">
												<!--begin::Details-->
												<div class="d-flex align-items-center">
													<!--begin::Checkbox-->
													<label class="form-check form-check-custom form-check-solid me-5">
														<input class="form-check-input" type="checkbox" name="users"
															data-kt-check="true"
															data-kt-check-target="[data-user-id='2']" value="2">
													</label>
													<!--end::Checkbox-->
													<!--begin::Avatar-->
													<div class="symbol symbol-35px symbol-circle">
														<img alt="Pic" src="assets/media/profile.png">
													</div>
													<!--end::Avatar-->
													<!--begin::Details-->
													<div class="ms-5">
														<a href="#"
															class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Max
															Smith</a>
														<div class="fw-semibold text-muted">max@kt.com</div>
													</div>
													<!--end::Details-->
												</div>
												<!--end::Details-->
												<!--begin::Access menu-->
												<div class="ms-2 w-100px">
													<select
														class="form-select form-select-solid form-select-sm select2-hidden-accessible"
														data-control="select2" data-hide-search="true"
														data-select2-id="select2-data-22-i29x" tabindex="-1"
														aria-hidden="true" data-kt-initialized="1">
														<option value="1">Guest</option>
														<option value="2">Owner</option>
														<option value="3" selected="selected"
															data-select2-id="select2-data-24-3ov6">Can Edit</option>
													</select><span
														class="select2 select2-container select2-container--bootstrap5"
														dir="ltr" data-select2-id="select2-data-23-44vn"
														style="width: 100%;"><span class="selection"><span
																class="select2-selection select2-selection--single form-select form-select-solid form-select-sm"
																role="combobox" aria-haspopup="true"
																aria-expanded="false" tabindex="0" aria-disabled="false"
																aria-labelledby="select2-yf19-container"
																aria-controls="select2-yf19-container"><span
																	class="select2-selection__rendered"
																	id="select2-yf19-container" role="textbox"
																	aria-readonly="true" title="Can Edit">Can
																	Edit</span><span class="select2-selection__arrow"
																	role="presentation"><b
																		role="presentation"></b></span></span></span><span
															class="dropdown-wrapper" aria-hidden="true"></span></span>
												</div>
												<!--end::Access menu-->
											</div>
											<!--end::User-->
											<!--begin::Separator-->
											<div class="border-bottom border-gray-300 border-bottom-dashed"></div>
											<!--end::Separator-->
											<!--begin::User-->
											<div class="rounded d-flex flex-stack bg-active-lighten p-4"
												data-user-id="3">
												<!--begin::Details-->
												<div class="d-flex align-items-center">
													<!--begin::Checkbox-->
													<label class="form-check form-check-custom form-check-solid me-5">
														<input class="form-check-input" type="checkbox" name="users"
															data-kt-check="true"
															data-kt-check-target="[data-user-id='3']" value="3">
													</label>
													<!--end::Checkbox-->
													<!--begin::Avatar-->
													<div class="symbol symbol-35px symbol-circle">
														<img alt="Pic" src="assets/media/avatars/300-5.jpg">
													</div>
													<!--end::Avatar-->
													<!--begin::Details-->
													<div class="ms-5">
														<a href="#"
															class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Sean
															Bean</a>
														<div class="fw-semibold text-muted">sean@dellito.com</div>
													</div>
													<!--end::Details-->
												</div>
												<!--end::Details-->
												<!--begin::Access menu-->
												<div class="ms-2 w-100px">
													<select
														class="form-select form-select-solid form-select-sm select2-hidden-accessible"
														data-control="select2" data-hide-search="true"
														data-select2-id="select2-data-25-62z4" tabindex="-1"
														aria-hidden="true" data-kt-initialized="1">
														<option value="1">Guest</option>
														<option value="2" selected="selected"
															data-select2-id="select2-data-27-x1go">Owner</option>
														<option value="3">Can Edit</option>
													</select><span
														class="select2 select2-container select2-container--bootstrap5"
														dir="ltr" data-select2-id="select2-data-26-gss1"
														style="width: 100%;"><span class="selection"><span
																class="select2-selection select2-selection--single form-select form-select-solid form-select-sm"
																role="combobox" aria-haspopup="true"
																aria-expanded="false" tabindex="0" aria-disabled="false"
																aria-labelledby="select2-andi-container"
																aria-controls="select2-andi-container"><span
																	class="select2-selection__rendered"
																	id="select2-andi-container" role="textbox"
																	aria-readonly="true" title="Owner">Owner</span><span
																	class="select2-selection__arrow"
																	role="presentation"><b
																		role="presentation"></b></span></span></span><span
															class="dropdown-wrapper" aria-hidden="true"></span></span>
												</div>
												<!--end::Access menu-->
											</div>
											<!--end::User-->
											<!--begin::Separator-->
											<div class="border-bottom border-gray-300 border-bottom-dashed"></div>
											<!--end::Separator-->
											<!--begin::User-->
											<div class="rounded d-flex flex-stack bg-active-lighten p-4"
												data-user-id="4">
												<!--begin::Details-->
												<div class="d-flex align-items-center">
													<!--begin::Checkbox-->
													<label class="form-check form-check-custom form-check-solid me-5">
														<input class="form-check-input" type="checkbox" name="users"
															data-kt-check="true"
															data-kt-check-target="[data-user-id='4']" value="4">
													</label>
													<!--end::Checkbox-->
													<!--begin::Avatar-->
													<div class="symbol symbol-35px symbol-circle">
														<img alt="Pic" src="assets/media/avatars/300-25.jpg">
													</div>
													<!--end::Avatar-->
													<!--begin::Details-->
													<div class="ms-5">
														<a href="#"
															class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Brian
															Cox</a>
														<div class="fw-semibold text-muted">brian@exchange.com</div>
													</div>
													<!--end::Details-->
												</div>
												<!--end::Details-->
												<!--begin::Access menu-->
												<div class="ms-2 w-100px">
													<select
														class="form-select form-select-solid form-select-sm select2-hidden-accessible"
														data-control="select2" data-hide-search="true"
														data-select2-id="select2-data-28-jgje" tabindex="-1"
														aria-hidden="true" data-kt-initialized="1">
														<option value="1">Guest</option>
														<option value="2">Owner</option>
														<option value="3" selected="selected"
															data-select2-id="select2-data-30-oelv">Can Edit</option>
													</select><span
														class="select2 select2-container select2-container--bootstrap5"
														dir="ltr" data-select2-id="select2-data-29-2jar"
														style="width: 100%;"><span class="selection"><span
																class="select2-selection select2-selection--single form-select form-select-solid form-select-sm"
																role="combobox" aria-haspopup="true"
																aria-expanded="false" tabindex="0" aria-disabled="false"
																aria-labelledby="select2-8r5c-container"
																aria-controls="select2-8r5c-container"><span
																	class="select2-selection__rendered"
																	id="select2-8r5c-container" role="textbox"
																	aria-readonly="true" title="Can Edit">Can
																	Edit</span><span class="select2-selection__arrow"
																	role="presentation"><b
																		role="presentation"></b></span></span></span><span
															class="dropdown-wrapper" aria-hidden="true"></span></span>
												</div>
												<!--end::Access menu-->
											</div>
											<!--end::User-->
											<!--begin::Separator-->
											<div class="border-bottom border-gray-300 border-bottom-dashed"></div>
											<!--end::Separator-->
											<!--begin::User-->
											<div class="rounded d-flex flex-stack bg-active-lighten p-4"
												data-user-id="5">
												<!--begin::Details-->
												<div class="d-flex align-items-center">
													<!--begin::Checkbox-->
													<label class="form-check form-check-custom form-check-solid me-5">
														<input class="form-check-input" type="checkbox" name="users"
															data-kt-check="true"
															data-kt-check-target="[data-user-id='5']" value="5">
													</label>
													<!--end::Checkbox-->
													<!--begin::Avatar-->
													<div class="symbol symbol-35px symbol-circle">
														<span
															class="symbol-label bg-light-warning text-warning fw-semibold">C</span>
													</div>
													<!--end::Avatar-->
													<!--begin::Details-->
													<div class="ms-5">
														<a href="#"
															class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Mikaela
															Collins</a>
														<div class="fw-semibold text-muted">mik@pex.com</div>
													</div>
													<!--end::Details-->
												</div>
												<!--end::Details-->
												<!--begin::Access menu-->
												<div class="ms-2 w-100px">
													<select
														class="form-select form-select-solid form-select-sm select2-hidden-accessible"
														data-control="select2" data-hide-search="true"
														data-select2-id="select2-data-31-riam" tabindex="-1"
														aria-hidden="true" data-kt-initialized="1">
														<option value="1">Guest</option>
														<option value="2" selected="selected"
															data-select2-id="select2-data-33-x12a">Owner</option>
														<option value="3">Can Edit</option>
													</select><span
														class="select2 select2-container select2-container--bootstrap5"
														dir="ltr" data-select2-id="select2-data-32-51eg"
														style="width: 100%;"><span class="selection"><span
																class="select2-selection select2-selection--single form-select form-select-solid form-select-sm"
																role="combobox" aria-haspopup="true"
																aria-expanded="false" tabindex="0" aria-disabled="false"
																aria-labelledby="select2-hcmv-container"
																aria-controls="select2-hcmv-container"><span
																	class="select2-selection__rendered"
																	id="select2-hcmv-container" role="textbox"
																	aria-readonly="true" title="Owner">Owner</span><span
																	class="select2-selection__arrow"
																	role="presentation"><b
																		role="presentation"></b></span></span></span><span
															class="dropdown-wrapper" aria-hidden="true"></span></span>
												</div>
												<!--end::Access menu-->
											</div>
											<!--end::User-->
											<!--begin::Separator-->
											<div class="border-bottom border-gray-300 border-bottom-dashed"></div>
											<!--end::Separator-->
											<!--begin::User-->
											<div class="rounded d-flex flex-stack bg-active-lighten p-4"
												data-user-id="6">
												<!--begin::Details-->
												<div class="d-flex align-items-center">
													<!--begin::Checkbox-->
													<label class="form-check form-check-custom form-check-solid me-5">
														<input class="form-check-input" type="checkbox" name="users"
															data-kt-check="true"
															data-kt-check-target="[data-user-id='6']" value="6">
													</label>
													<!--end::Checkbox-->
													<!--begin::Avatar-->
													<div class="symbol symbol-35px symbol-circle">
														<img alt="Pic" src="assets/media/avatars/300-9.jpg">
													</div>
													<!--end::Avatar-->
													<!--begin::Details-->
													<div class="ms-5">
														<a href="#"
															class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Francis
															Mitcham</a>
														<div class="fw-semibold text-muted">f.mit@kpmg.com</div>
													</div>
													<!--end::Details-->
												</div>
												<!--end::Details-->
												<!--begin::Access menu-->
												<div class="ms-2 w-100px">
													<select
														class="form-select form-select-solid form-select-sm select2-hidden-accessible"
														data-control="select2" data-hide-search="true"
														data-select2-id="select2-data-34-0yq5" tabindex="-1"
														aria-hidden="true" data-kt-initialized="1">
														<option value="1">Guest</option>
														<option value="2">Owner</option>
														<option value="3" selected="selected"
															data-select2-id="select2-data-36-bzop">Can Edit</option>
													</select><span
														class="select2 select2-container select2-container--bootstrap5"
														dir="ltr" data-select2-id="select2-data-35-h47u"
														style="width: 100%;"><span class="selection"><span
																class="select2-selection select2-selection--single form-select form-select-solid form-select-sm"
																role="combobox" aria-haspopup="true"
																aria-expanded="false" tabindex="0" aria-disabled="false"
																aria-labelledby="select2-keka-container"
																aria-controls="select2-keka-container"><span
																	class="select2-selection__rendered"
																	id="select2-keka-container" role="textbox"
																	aria-readonly="true" title="Can Edit">Can
																	Edit</span><span class="select2-selection__arrow"
																	role="presentation"><b
																		role="presentation"></b></span></span></span><span
															class="dropdown-wrapper" aria-hidden="true"></span></span>
												</div>
												<!--end::Access menu-->
											</div>
											<!--end::User-->
											<!--begin::Separator-->
											<div class="border-bottom border-gray-300 border-bottom-dashed"></div>
											<!--end::Separator-->
											<!--begin::User-->
											<div class="rounded d-flex flex-stack bg-active-lighten p-4"
												data-user-id="7">
												<!--begin::Details-->
												<div class="d-flex align-items-center">
													<!--begin::Checkbox-->
													<label class="form-check form-check-custom form-check-solid me-5">
														<input class="form-check-input" type="checkbox" name="users"
															data-kt-check="true"
															data-kt-check-target="[data-user-id='7']" value="7">
													</label>
													<!--end::Checkbox-->
													<!--begin::Avatar-->
													<div class="symbol symbol-35px symbol-circle">
														<span
															class="symbol-label bg-light-danger text-danger fw-semibold">O</span>
													</div>
													<!--end::Avatar-->
													<!--begin::Details-->
													<div class="ms-5">
														<a href="#"
															class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Olivia
															Wild</a>
														<div class="fw-semibold text-muted">olivia@corpmail.com</div>
													</div>
													<!--end::Details-->
												</div>
												<!--end::Details-->
												<!--begin::Access menu-->
												<div class="ms-2 w-100px">
													<select
														class="form-select form-select-solid form-select-sm select2-hidden-accessible"
														data-control="select2" data-hide-search="true"
														data-select2-id="select2-data-37-rnwx" tabindex="-1"
														aria-hidden="true" data-kt-initialized="1">
														<option value="1">Guest</option>
														<option value="2" selected="selected"
															data-select2-id="select2-data-39-03nf">Owner</option>
														<option value="3">Can Edit</option>
													</select><span
														class="select2 select2-container select2-container--bootstrap5"
														dir="ltr" data-select2-id="select2-data-38-nnso"
														style="width: 100%;"><span class="selection"><span
																class="select2-selection select2-selection--single form-select form-select-solid form-select-sm"
																role="combobox" aria-haspopup="true"
																aria-expanded="false" tabindex="0" aria-disabled="false"
																aria-labelledby="select2-dqrb-container"
																aria-controls="select2-dqrb-container"><span
																	class="select2-selection__rendered"
																	id="select2-dqrb-container" role="textbox"
																	aria-readonly="true" title="Owner">Owner</span><span
																	class="select2-selection__arrow"
																	role="presentation"><b
																		role="presentation"></b></span></span></span><span
															class="dropdown-wrapper" aria-hidden="true"></span></span>
												</div>
												<!--end::Access menu-->
											</div>
											<!--end::User-->
											<!--begin::Separator-->
											<div class="border-bottom border-gray-300 border-bottom-dashed"></div>
											<!--end::Separator-->
											<!--begin::User-->
											<div class="rounded d-flex flex-stack bg-active-lighten p-4"
												data-user-id="8">
												<!--begin::Details-->
												<div class="d-flex align-items-center">
													<!--begin::Checkbox-->
													<label class="form-check form-check-custom form-check-solid me-5">
														<input class="form-check-input" type="checkbox" name="users"
															data-kt-check="true"
															data-kt-check-target="[data-user-id='8']" value="8">
													</label>
													<!--end::Checkbox-->
													<!--begin::Avatar-->
													<div class="symbol symbol-35px symbol-circle">
														<span
															class="symbol-label bg-light-primary text-primary fw-semibold">N</span>
													</div>
													<!--end::Avatar-->
													<!--begin::Details-->
													<div class="ms-5">
														<a href="#"
															class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Neil
															Owen</a>
														<div class="fw-semibold text-muted">owen.neil@gmail.com</div>
													</div>
													<!--end::Details-->
												</div>
												<!--end::Details-->
												<!--begin::Access menu-->
												<div class="ms-2 w-100px">
													<select
														class="form-select form-select-solid form-select-sm select2-hidden-accessible"
														data-control="select2" data-hide-search="true"
														data-select2-id="select2-data-40-o7n4" tabindex="-1"
														aria-hidden="true" data-kt-initialized="1">
														<option value="1" selected="selected"
															data-select2-id="select2-data-42-yhde">Guest</option>
														<option value="2">Owner</option>
														<option value="3">Can Edit</option>
													</select><span
														class="select2 select2-container select2-container--bootstrap5"
														dir="ltr" data-select2-id="select2-data-41-z4fr"
														style="width: 100%;"><span class="selection"><span
																class="select2-selection select2-selection--single form-select form-select-solid form-select-sm"
																role="combobox" aria-haspopup="true"
																aria-expanded="false" tabindex="0" aria-disabled="false"
																aria-labelledby="select2-av6u-container"
																aria-controls="select2-av6u-container"><span
																	class="select2-selection__rendered"
																	id="select2-av6u-container" role="textbox"
																	aria-readonly="true" title="Guest">Guest</span><span
																	class="select2-selection__arrow"
																	role="presentation"><b
																		role="presentation"></b></span></span></span><span
															class="dropdown-wrapper" aria-hidden="true"></span></span>
												</div>
												<!--end::Access menu-->
											</div>
											<!--end::User-->
											<!--begin::Separator-->
											<div class="border-bottom border-gray-300 border-bottom-dashed"></div>
											<!--end::Separator-->
											<!--begin::User-->
											<div class="rounded d-flex flex-stack bg-active-lighten p-4"
												data-user-id="9">
												<!--begin::Details-->
												<div class="d-flex align-items-center">
													<!--begin::Checkbox-->
													<label class="form-check form-check-custom form-check-solid me-5">
														<input class="form-check-input" type="checkbox" name="users"
															data-kt-check="true"
															data-kt-check-target="[data-user-id='9']" value="9">
													</label>
													<!--end::Checkbox-->
													<!--begin::Avatar-->
													<div class="symbol symbol-35px symbol-circle">
														<img alt="Pic" src="assets/media/avatars/300-23.jpg">
													</div>
													<!--end::Avatar-->
													<!--begin::Details-->
													<div class="ms-5">
														<a href="#"
															class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Dan
															Wilson</a>
														<div class="fw-semibold text-muted">dam@consilting.com</div>
													</div>
													<!--end::Details-->
												</div>
												<!--end::Details-->
												<!--begin::Access menu-->
												<div class="ms-2 w-100px">
													<select
														class="form-select form-select-solid form-select-sm select2-hidden-accessible"
														data-control="select2" data-hide-search="true"
														data-select2-id="select2-data-43-yoss" tabindex="-1"
														aria-hidden="true" data-kt-initialized="1">
														<option value="1">Guest</option>
														<option value="2">Owner</option>
														<option value="3" selected="selected"
															data-select2-id="select2-data-45-bnhm">Can Edit</option>
													</select><span
														class="select2 select2-container select2-container--bootstrap5"
														dir="ltr" data-select2-id="select2-data-44-63v6"
														style="width: 100%;"><span class="selection"><span
																class="select2-selection select2-selection--single form-select form-select-solid form-select-sm"
																role="combobox" aria-haspopup="true"
																aria-expanded="false" tabindex="0" aria-disabled="false"
																aria-labelledby="select2-r26t-container"
																aria-controls="select2-r26t-container"><span
																	class="select2-selection__rendered"
																	id="select2-r26t-container" role="textbox"
																	aria-readonly="true" title="Can Edit">Can
																	Edit</span><span class="select2-selection__arrow"
																	role="presentation"><b
																		role="presentation"></b></span></span></span><span
															class="dropdown-wrapper" aria-hidden="true"></span></span>
												</div>
												<!--end::Access menu-->
											</div>
											<!--end::User-->
											<!--begin::Separator-->
											<div class="border-bottom border-gray-300 border-bottom-dashed"></div>
											<!--end::Separator-->
											<!--begin::User-->
											<div class="rounded d-flex flex-stack bg-active-lighten p-4"
												data-user-id="10">
												<!--begin::Details-->
												<div class="d-flex align-items-center">
													<!--begin::Checkbox-->
													<label class="form-check form-check-custom form-check-solid me-5">
														<input class="form-check-input" type="checkbox" name="users"
															data-kt-check="true"
															data-kt-check-target="[data-user-id='10']" value="10">
													</label>
													<!--end::Checkbox-->
													<!--begin::Avatar-->
													<div class="symbol symbol-35px symbol-circle">
														<span
															class="symbol-label bg-light-danger text-danger fw-semibold">E</span>
													</div>
													<!--end::Avatar-->
													<!--begin::Details-->
													<div class="ms-5">
														<a href="#"
															class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Emma
															Bold</a>
														<div class="fw-semibold text-muted">emma@intenso.com</div>
													</div>
													<!--end::Details-->
												</div>
												<!--end::Details-->
												<!--begin::Access menu-->
												<div class="ms-2 w-100px">
													<select
														class="form-select form-select-solid form-select-sm select2-hidden-accessible"
														data-control="select2" data-hide-search="true"
														data-select2-id="select2-data-46-dm3v" tabindex="-1"
														aria-hidden="true" data-kt-initialized="1">
														<option value="1">Guest</option>
														<option value="2" selected="selected"
															data-select2-id="select2-data-48-6s2d">Owner</option>
														<option value="3">Can Edit</option>
													</select><span
														class="select2 select2-container select2-container--bootstrap5"
														dir="ltr" data-select2-id="select2-data-47-md8o"
														style="width: 100%;"><span class="selection"><span
																class="select2-selection select2-selection--single form-select form-select-solid form-select-sm"
																role="combobox" aria-haspopup="true"
																aria-expanded="false" tabindex="0" aria-disabled="false"
																aria-labelledby="select2-gq2r-container"
																aria-controls="select2-gq2r-container"><span
																	class="select2-selection__rendered"
																	id="select2-gq2r-container" role="textbox"
																	aria-readonly="true" title="Owner">Owner</span><span
																	class="select2-selection__arrow"
																	role="presentation"><b
																		role="presentation"></b></span></span></span><span
															class="dropdown-wrapper" aria-hidden="true"></span></span>
												</div>
												<!--end::Access menu-->
											</div>
											<!--end::User-->
											<!--begin::Separator-->
											<div class="border-bottom border-gray-300 border-bottom-dashed"></div>
											<!--end::Separator-->
											<!--begin::User-->
											<div class="rounded d-flex flex-stack bg-active-lighten p-4"
												data-user-id="11">
												<!--begin::Details-->
												<div class="d-flex align-items-center">
													<!--begin::Checkbox-->
													<label class="form-check form-check-custom form-check-solid me-5">
														<input class="form-check-input" type="checkbox" name="users"
															data-kt-check="true"
															data-kt-check-target="[data-user-id='11']" value="11">
													</label>
													<!--end::Checkbox-->
													<!--begin::Avatar-->
													<div class="symbol symbol-35px symbol-circle">
														<img alt="Pic" src="assets/media/avatars/300-12.jpg">
													</div>
													<!--end::Avatar-->
													<!--begin::Details-->
													<div class="ms-5">
														<a href="#"
															class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Ana
															Crown</a>
														<div class="fw-semibold text-muted">ana.cf@limtel.com</div>
													</div>
													<!--end::Details-->
												</div>
												<!--end::Details-->
												<!--begin::Access menu-->
												<div class="ms-2 w-100px">
													<select
														class="form-select form-select-solid form-select-sm select2-hidden-accessible"
														data-control="select2" data-hide-search="true"
														data-select2-id="select2-data-49-dy7q" tabindex="-1"
														aria-hidden="true" data-kt-initialized="1">
														<option value="1" selected="selected"
															data-select2-id="select2-data-51-3v89">Guest</option>
														<option value="2">Owner</option>
														<option value="3">Can Edit</option>
													</select><span
														class="select2 select2-container select2-container--bootstrap5"
														dir="ltr" data-select2-id="select2-data-50-57r5"
														style="width: 100%;"><span class="selection"><span
																class="select2-selection select2-selection--single form-select form-select-solid form-select-sm"
																role="combobox" aria-haspopup="true"
																aria-expanded="false" tabindex="0" aria-disabled="false"
																aria-labelledby="select2-d9d9-container"
																aria-controls="select2-d9d9-container"><span
																	class="select2-selection__rendered"
																	id="select2-d9d9-container" role="textbox"
																	aria-readonly="true" title="Guest">Guest</span><span
																	class="select2-selection__arrow"
																	role="presentation"><b
																		role="presentation"></b></span></span></span><span
															class="dropdown-wrapper" aria-hidden="true"></span></span>
												</div>
												<!--end::Access menu-->
											</div>
											<!--end::User-->
											<!--begin::Separator-->
											<div class="border-bottom border-gray-300 border-bottom-dashed"></div>
											<!--end::Separator-->
											<!--begin::User-->
											<div class="rounded d-flex flex-stack bg-active-lighten p-4"
												data-user-id="12">
												<!--begin::Details-->
												<div class="d-flex align-items-center">
													<!--begin::Checkbox-->
													<label class="form-check form-check-custom form-check-solid me-5">
														<input class="form-check-input" type="checkbox" name="users"
															data-kt-check="true"
															data-kt-check-target="[data-user-id='12']" value="12">
													</label>
													<!--end::Checkbox-->
													<!--begin::Avatar-->
													<div class="symbol symbol-35px symbol-circle">
														<span
															class="symbol-label bg-light-info text-info fw-semibold">A</span>
													</div>
													<!--end::Avatar-->
													<!--begin::Details-->
													<div class="ms-5">
														<a href="#"
															class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Robert
															Doe</a>
														<div class="fw-semibold text-muted">robert@benko.com</div>
													</div>
													<!--end::Details-->
												</div>
												<!--end::Details-->
												<!--begin::Access menu-->
												<div class="ms-2 w-100px">
													<select
														class="form-select form-select-solid form-select-sm select2-hidden-accessible"
														data-control="select2" data-hide-search="true"
														data-select2-id="select2-data-52-oz1t" tabindex="-1"
														aria-hidden="true" data-kt-initialized="1">
														<option value="1">Guest</option>
														<option value="2">Owner</option>
														<option value="3" selected="selected"
															data-select2-id="select2-data-54-71gi">Can Edit</option>
													</select><span
														class="select2 select2-container select2-container--bootstrap5"
														dir="ltr" data-select2-id="select2-data-53-taqp"
														style="width: 100%;"><span class="selection"><span
																class="select2-selection select2-selection--single form-select form-select-solid form-select-sm"
																role="combobox" aria-haspopup="true"
																aria-expanded="false" tabindex="0" aria-disabled="false"
																aria-labelledby="select2-w9tt-container"
																aria-controls="select2-w9tt-container"><span
																	class="select2-selection__rendered"
																	id="select2-w9tt-container" role="textbox"
																	aria-readonly="true" title="Can Edit">Can
																	Edit</span><span class="select2-selection__arrow"
																	role="presentation"><b
																		role="presentation"></b></span></span></span><span
															class="dropdown-wrapper" aria-hidden="true"></span></span>
												</div>
												<!--end::Access menu-->
											</div>
											<!--end::User-->
											<!--begin::Separator-->
											<div class="border-bottom border-gray-300 border-bottom-dashed"></div>
											<!--end::Separator-->
											<!--begin::User-->
											<div class="rounded d-flex flex-stack bg-active-lighten p-4"
												data-user-id="13">
												<!--begin::Details-->
												<div class="d-flex align-items-center">
													<!--begin::Checkbox-->
													<label class="form-check form-check-custom form-check-solid me-5">
														<input class="form-check-input" type="checkbox" name="users"
															data-kt-check="true"
															data-kt-check-target="[data-user-id='13']" value="13">
													</label>
													<!--end::Checkbox-->
													<!--begin::Avatar-->
													<div class="symbol symbol-35px symbol-circle">
														<img alt="Pic" src="assets/media/avatars/300-13.jpg">
													</div>
													<!--end::Avatar-->
													<!--begin::Details-->
													<div class="ms-5">
														<a href="#"
															class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">John
															Miller</a>
														<div class="fw-semibold text-muted">miller@mapple.com</div>
													</div>
													<!--end::Details-->
												</div>
												<!--end::Details-->
												<!--begin::Access menu-->
												<div class="ms-2 w-100px">
													<select
														class="form-select form-select-solid form-select-sm select2-hidden-accessible"
														data-control="select2" data-hide-search="true"
														data-select2-id="select2-data-55-4qd2" tabindex="-1"
														aria-hidden="true" data-kt-initialized="1">
														<option value="1">Guest</option>
														<option value="2">Owner</option>
														<option value="3" selected="selected"
															data-select2-id="select2-data-57-r3ve">Can Edit</option>
													</select><span
														class="select2 select2-container select2-container--bootstrap5"
														dir="ltr" data-select2-id="select2-data-56-zgoi"
														style="width: 100%;"><span class="selection"><span
																class="select2-selection select2-selection--single form-select form-select-solid form-select-sm"
																role="combobox" aria-haspopup="true"
																aria-expanded="false" tabindex="0" aria-disabled="false"
																aria-labelledby="select2-7l36-container"
																aria-controls="select2-7l36-container"><span
																	class="select2-selection__rendered"
																	id="select2-7l36-container" role="textbox"
																	aria-readonly="true" title="Can Edit">Can
																	Edit</span><span class="select2-selection__arrow"
																	role="presentation"><b
																		role="presentation"></b></span></span></span><span
															class="dropdown-wrapper" aria-hidden="true"></span></span>
												</div>
												<!--end::Access menu-->
											</div>
											<!--end::User-->
											<!--begin::Separator-->
											<div class="border-bottom border-gray-300 border-bottom-dashed"></div>
											<!--end::Separator-->
											<!--begin::User-->
											<div class="rounded d-flex flex-stack bg-active-lighten p-4"
												data-user-id="14">
												<!--begin::Details-->
												<div class="d-flex align-items-center">
													<!--begin::Checkbox-->
													<label class="form-check form-check-custom form-check-solid me-5">
														<input class="form-check-input" type="checkbox" name="users"
															data-kt-check="true"
															data-kt-check-target="[data-user-id='14']" value="14">
													</label>
													<!--end::Checkbox-->
													<!--begin::Avatar-->
													<div class="symbol symbol-35px symbol-circle">
														<span
															class="symbol-label bg-light-success text-success fw-semibold">L</span>
													</div>
													<!--end::Avatar-->
													<!--begin::Details-->
													<div class="ms-5">
														<a href="#"
															class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Lucy
															Kunic</a>
														<div class="fw-semibold text-muted">lucy.m@fentech.com</div>
													</div>
													<!--end::Details-->
												</div>
												<!--end::Details-->
												<!--begin::Access menu-->
												<div class="ms-2 w-100px">
													<select
														class="form-select form-select-solid form-select-sm select2-hidden-accessible"
														data-control="select2" data-hide-search="true"
														data-select2-id="select2-data-58-e04r" tabindex="-1"
														aria-hidden="true" data-kt-initialized="1">
														<option value="1">Guest</option>
														<option value="2" selected="selected"
															data-select2-id="select2-data-60-l2up">Owner</option>
														<option value="3">Can Edit</option>
													</select><span
														class="select2 select2-container select2-container--bootstrap5"
														dir="ltr" data-select2-id="select2-data-59-s4im"
														style="width: 100%;"><span class="selection"><span
																class="select2-selection select2-selection--single form-select form-select-solid form-select-sm"
																role="combobox" aria-haspopup="true"
																aria-expanded="false" tabindex="0" aria-disabled="false"
																aria-labelledby="select2-tlg6-container"
																aria-controls="select2-tlg6-container"><span
																	class="select2-selection__rendered"
																	id="select2-tlg6-container" role="textbox"
																	aria-readonly="true" title="Owner">Owner</span><span
																	class="select2-selection__arrow"
																	role="presentation"><b
																		role="presentation"></b></span></span></span><span
															class="dropdown-wrapper" aria-hidden="true"></span></span>
												</div>
												<!--end::Access menu-->
											</div>
											<!--end::User-->
											<!--begin::Separator-->
											<div class="border-bottom border-gray-300 border-bottom-dashed"></div>
											<!--end::Separator-->
											<!--begin::User-->
											<div class="rounded d-flex flex-stack bg-active-lighten p-4"
												data-user-id="15">
												<!--begin::Details-->
												<div class="d-flex align-items-center">
													<!--begin::Checkbox-->
													<label class="form-check form-check-custom form-check-solid me-5">
														<input class="form-check-input" type="checkbox" name="users"
															data-kt-check="true"
															data-kt-check-target="[data-user-id='15']" value="15">
													</label>
													<!--end::Checkbox-->
													<!--begin::Avatar-->
													<div class="symbol symbol-35px symbol-circle">
														<img alt="Pic" src="assets/media/avatars/300-21.jpg">
													</div>
													<!--end::Avatar-->
													<!--begin::Details-->
													<div class="ms-5">
														<a href="#"
															class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Ethan
															Wilder</a>
														<div class="fw-semibold text-muted">ethan@loop.com.au</div>
													</div>
													<!--end::Details-->
												</div>
												<!--end::Details-->
												<!--begin::Access menu-->
												<div class="ms-2 w-100px">
													<select
														class="form-select form-select-solid form-select-sm select2-hidden-accessible"
														data-control="select2" data-hide-search="true"
														data-select2-id="select2-data-61-o7zi" tabindex="-1"
														aria-hidden="true" data-kt-initialized="1">
														<option value="1" selected="selected"
															data-select2-id="select2-data-63-5z81">Guest</option>
														<option value="2">Owner</option>
														<option value="3">Can Edit</option>
													</select><span
														class="select2 select2-container select2-container--bootstrap5"
														dir="ltr" data-select2-id="select2-data-62-pe0o"
														style="width: 100%;"><span class="selection"><span
																class="select2-selection select2-selection--single form-select form-select-solid form-select-sm"
																role="combobox" aria-haspopup="true"
																aria-expanded="false" tabindex="0" aria-disabled="false"
																aria-labelledby="select2-qh9l-container"
																aria-controls="select2-qh9l-container"><span
																	class="select2-selection__rendered"
																	id="select2-qh9l-container" role="textbox"
																	aria-readonly="true" title="Guest">Guest</span><span
																	class="select2-selection__arrow"
																	role="presentation"><b
																		role="presentation"></b></span></span></span><span
															class="dropdown-wrapper" aria-hidden="true"></span></span>
												</div>
												<!--end::Access menu-->
											</div>
											<!--end::User-->
											<!--begin::Separator-->
											<div class="border-bottom border-gray-300 border-bottom-dashed"></div>
											<!--end::Separator-->
											<!--begin::User-->
											<div class="rounded d-flex flex-stack bg-active-lighten p-4"
												data-user-id="16">
												<!--begin::Details-->
												<div class="d-flex align-items-center">
													<!--begin::Checkbox-->
													<label class="form-check form-check-custom form-check-solid me-5">
														<input class="form-check-input" type="checkbox" name="users"
															data-kt-check="true"
															data-kt-check-target="[data-user-id='16']" value="16">
													</label>
													<!--end::Checkbox-->
													<!--begin::Avatar-->
													<div class="symbol symbol-35px symbol-circle">
														<img alt="Pic" src="assets/media/avatars/300-6.jpg">
													</div>
													<!--end::Avatar-->
													<!--begin::Details-->
													<div class="ms-5">
														<a href="#"
															class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Emma
															Smith</a>
														<div class="fw-semibold text-muted">smith@kpmg.com</div>
													</div>
													<!--end::Details-->
												</div>
												<!--end::Details-->
												<!--begin::Access menu-->
												<div class="ms-2 w-100px">
													<select
														class="form-select form-select-solid form-select-sm select2-hidden-accessible"
														data-control="select2" data-hide-search="true"
														data-select2-id="select2-data-64-sb7b" tabindex="-1"
														aria-hidden="true" data-kt-initialized="1">
														<option value="1">Guest</option>
														<option value="2">Owner</option>
														<option value="3" selected="selected"
															data-select2-id="select2-data-66-d5dc">Can Edit</option>
													</select><span
														class="select2 select2-container select2-container--bootstrap5"
														dir="ltr" data-select2-id="select2-data-65-8vjc"
														style="width: 100%;"><span class="selection"><span
																class="select2-selection select2-selection--single form-select form-select-solid form-select-sm"
																role="combobox" aria-haspopup="true"
																aria-expanded="false" tabindex="0" aria-disabled="false"
																aria-labelledby="select2-aesn-container"
																aria-controls="select2-aesn-container"><span
																	class="select2-selection__rendered"
																	id="select2-aesn-container" role="textbox"
																	aria-readonly="true" title="Can Edit">Can
																	Edit</span><span class="select2-selection__arrow"
																	role="presentation"><b
																		role="presentation"></b></span></span></span><span
															class="dropdown-wrapper" aria-hidden="true"></span></span>
												</div>
												<!--end::Access menu-->
											</div>
											<!--end::User-->
										</div>
										<div class="d-flex flex-center mt-15">
											<button type="reset" id="kt_modal_users_search_reset"
												data-bs-dismiss="modal"
												class="btn btn-active-light me-3">Cancel</button>
											<button type="submit" id="kt_modal_users_search_submit"
												class="btn btn-primary">Add
												Selected Users</button>
										</div>
										<!--end::Actions-->
									</div>
									<!--end::Results-->
									<!--begin::Empty-->
									<div data-kt-search-element="empty" class="text-center d-none">
										<!--begin::Message-->
										<div class="fw-semibold py-10">
											<div class="text-gray-600 fs-3 mb-2">No users found</div>
											<div class="text-muted fs-6">Try to search by username, full name or
												email...</div>
										</div>
										<div class="text-center px-5">
											<img src="assets/media/illustrations/sketchy-1/1.png" alt=""
												class="w-100 h-200px h-sm-325px">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade" id="kt_modal_invite_friends" tabindex="-1" aria-hidden="true">
				<div class="modal-dialog mw-650px">
					<!--begin::Modal content-->
					<div class="modal-content">
						<!--begin::Modal header-->
						<div class="modal-header pb-0 border-0 justify-content-end">
							<!--begin::Close-->
							<div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
								<!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
								<span class="svg-icon svg-icon-1">
									<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
										xmlns="http://www.w3.org/2000/svg">
										<rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
											transform="rotate(-45 6 17.3137)" fill="currentColor"></rect>
										<rect x="7.41422" y="6" width="16" height="2" rx="1"
											transform="rotate(45 7.41422 6)" fill="currentColor"></rect>
									</svg>
								</span>
								<!--end::Svg Icon-->
							</div>
							<!--end::Close-->
						</div>
						<!--begin::Modal header-->
						<!--begin::Modal body-->
						<div class="modal-body scroll-y mx-5 mx-xl-18 pt-0 pb-15">
							<!--begin::Heading-->
							<div class="text-center mb-13">
								<!--begin::Title-->
								<h1 class="mb-3">Invite a Friend</h1>
								<!--end::Title-->
								<!--begin::Description-->
								<div class="text-muted fw-semibold fs-5">If you need more info, please check out
									<a href="#" class="link-primary fw-bold">FAQ Page</a>.
								</div>
								<!--end::Description-->
							</div>
							<!--end::Heading-->
							<!--begin::Google Contacts Invite-->
							<div class="btn btn-light-primary fw-bold w-100 mb-8">
								<img alt="Logo" src="assets/media/svg/brand-logos/google-icon.svg"
									class="h-20px me-3">Invite
								Gmail Contacts
							</div>
							<!--end::Google Contacts Invite-->
							<!--begin::Separator-->
							<div class="separator d-flex flex-center mb-8">
								<span class="text-uppercase bg-body fs-7 fw-semibold text-muted px-3">or</span>
							</div>
							<!--end::Separator-->
							<!--begin::Textarea-->
							<textarea class="form-control form-control-solid mb-8" rows="3"
								placeholder="Type or paste emails here"></textarea>
							<!--end::Textarea-->
							<!--begin::Users-->
							<div class="mb-10">
								<!--begin::Heading-->
								<div class="fs-6 fw-semibold mb-2">Your Invitations</div>
								<!--end::Heading-->
								<!--begin::List-->
								<div class="mh-300px scroll-y me-n7 pe-7">
									<!--begin::User-->
									<div
										class="d-flex flex-stack py-4 border-bottom border-gray-300 border-bottom-dashed">
										<!--begin::Details-->
										<div class="d-flex align-items-center">
											<!--begin::Avatar-->
											<div class="symbol symbol-35px symbol-circle">
												<img alt="Pic" src="assets/media/avatars/300-6.jpg">
											</div>
											<!--end::Avatar-->
											<!--begin::Details-->
											<div class="ms-5">
												<a href="#"
													class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Emma
													Smith</a>
												<div class="fw-semibold text-muted">smith@kpmg.com</div>
											</div>
											<!--end::Details-->
										</div>
										<!--end::Details-->
										<!--begin::Access menu-->
										<div class="ms-2 w-100px">
											<select
												class="form-select form-select-solid form-select-sm select2-hidden-accessible"
												data-control="select2" data-dropdown-parent="#kt_modal_invite_friends"
												data-hide-search="true" data-select2-id="select2-data-67-bll8"
												tabindex="-1" aria-hidden="true" data-kt-initialized="1">
												<option value="1">Guest</option>
												<option value="2" selected="selected"
													data-select2-id="select2-data-69-1nr5">Owner</option>
												<option value="3">Can Edit</option>
											</select><span
												class="select2 select2-container select2-container--bootstrap5"
												dir="ltr" data-select2-id="select2-data-68-66mc"
												style="width: 100%;"><span class="selection"><span
														class="select2-selection select2-selection--single form-select form-select-solid form-select-sm"
														role="combobox" aria-haspopup="true" aria-expanded="false"
														tabindex="0" aria-disabled="false"
														aria-labelledby="select2-1sd6-container"
														aria-controls="select2-1sd6-container"><span
															class="select2-selection__rendered"
															id="select2-1sd6-container" role="textbox"
															aria-readonly="true" title="Owner">Owner</span><span
															class="select2-selection__arrow" role="presentation"><b
																role="presentation"></b></span></span></span><span
													class="dropdown-wrapper" aria-hidden="true"></span></span>
										</div>
										<!--end::Access menu-->
									</div>
									<!--end::User-->
									<!--begin::User-->
									<div
										class="d-flex flex-stack py-4 border-bottom border-gray-300 border-bottom-dashed">
										<!--begin::Details-->
										<div class="d-flex align-items-center">
											<!--begin::Avatar-->
											<div class="symbol symbol-35px symbol-circle">
												<span
													class="symbol-label bg-light-danger text-danger fw-semibold">M</span>
											</div>
											<!--end::Avatar-->
											<!--begin::Details-->
											<div class="ms-5">
												<a href="#"
													class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Melody
													Macy</a>
												<div class="fw-semibold text-muted">melody@altbox.com</div>
											</div>
											<!--end::Details-->
										</div>
										<!--end::Details-->
										<!--begin::Access menu-->
										<div class="ms-2 w-100px">
											<select
												class="form-select form-select-solid form-select-sm select2-hidden-accessible"
												data-control="select2" data-dropdown-parent="#kt_modal_invite_friends"
												data-hide-search="true" data-select2-id="select2-data-70-qmvn"
												tabindex="-1" aria-hidden="true" data-kt-initialized="1">
												<option value="1" selected="selected"
													data-select2-id="select2-data-72-39zr">Guest</option>
												<option value="2">Owner</option>
												<option value="3">Can Edit</option>
											</select><span
												class="select2 select2-container select2-container--bootstrap5"
												dir="ltr" data-select2-id="select2-data-71-92bn"
												style="width: 100%;"><span class="selection"><span
														class="select2-selection select2-selection--single form-select form-select-solid form-select-sm"
														role="combobox" aria-haspopup="true" aria-expanded="false"
														tabindex="0" aria-disabled="false"
														aria-labelledby="select2-9n0h-container"
														aria-controls="select2-9n0h-container"><span
															class="select2-selection__rendered"
															id="select2-9n0h-container" role="textbox"
															aria-readonly="true" title="Guest">Guest</span><span
															class="select2-selection__arrow" role="presentation"><b
																role="presentation"></b></span></span></span><span
													class="dropdown-wrapper" aria-hidden="true"></span></span>
										</div>
										<!--end::Access menu-->
									</div>
									<!--end::User-->
									<!--begin::User-->
									<div
										class="d-flex flex-stack py-4 border-bottom border-gray-300 border-bottom-dashed">
										<!--begin::Details-->
										<div class="d-flex align-items-center">
											<!--begin::Avatar-->
											<div class="symbol symbol-35px symbol-circle">
												<img alt="Pic" src="assets/media/profile.png">
											</div>
											<!--end::Avatar-->
											<!--begin::Details-->
											<div class="ms-5">
												<a href="#"
													class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Max
													Smith</a>
												<div class="fw-semibold text-muted">max@kt.com</div>
											</div>
											<!--end::Details-->
										</div>
										<!--end::Details-->
										<!--begin::Access menu-->
										<div class="ms-2 w-100px">
											<select
												class="form-select form-select-solid form-select-sm select2-hidden-accessible"
												data-control="select2" data-dropdown-parent="#kt_modal_invite_friends"
												data-hide-search="true" data-select2-id="select2-data-73-pi6n"
												tabindex="-1" aria-hidden="true" data-kt-initialized="1">
												<option value="1">Guest</option>
												<option value="2">Owner</option>
												<option value="3" selected="selected"
													data-select2-id="select2-data-75-4tcd">Can Edit</option>
											</select><span
												class="select2 select2-container select2-container--bootstrap5"
												dir="ltr" data-select2-id="select2-data-74-7oxy"
												style="width: 100%;"><span class="selection"><span
														class="select2-selection select2-selection--single form-select form-select-solid form-select-sm"
														role="combobox" aria-haspopup="true" aria-expanded="false"
														tabindex="0" aria-disabled="false"
														aria-labelledby="select2-jm2g-container"
														aria-controls="select2-jm2g-container"><span
															class="select2-selection__rendered"
															id="select2-jm2g-container" role="textbox"
															aria-readonly="true" title="Can Edit">Can Edit</span><span
															class="select2-selection__arrow" role="presentation"><b
																role="presentation"></b></span></span></span><span
													class="dropdown-wrapper" aria-hidden="true"></span></span>
										</div>
										<!--end::Access menu-->
									</div>
									<!--end::User-->
									<!--begin::User-->
									<div
										class="d-flex flex-stack py-4 border-bottom border-gray-300 border-bottom-dashed">
										<!--begin::Details-->
										<div class="d-flex align-items-center">
											<!--begin::Avatar-->
											<div class="symbol symbol-35px symbol-circle">
												<img alt="Pic" src="assets/media/avatars/300-5.jpg">
											</div>
											<!--end::Avatar-->
											<!--begin::Details-->
											<div class="ms-5">
												<a href="#"
													class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Sean
													Bean</a>
												<div class="fw-semibold text-muted">sean@dellito.com</div>
											</div>
											<!--end::Details-->
										</div>
										<!--end::Details-->
										<!--begin::Access menu-->
										<div class="ms-2 w-100px">
											<select
												class="form-select form-select-solid form-select-sm select2-hidden-accessible"
												data-control="select2" data-dropdown-parent="#kt_modal_invite_friends"
												data-hide-search="true" data-select2-id="select2-data-76-rtq7"
												tabindex="-1" aria-hidden="true" data-kt-initialized="1">
												<option value="1">Guest</option>
												<option value="2" selected="selected"
													data-select2-id="select2-data-78-822b">Owner</option>
												<option value="3">Can Edit</option>
											</select><span
												class="select2 select2-container select2-container--bootstrap5"
												dir="ltr" data-select2-id="select2-data-77-2u1u"
												style="width: 100%;"><span class="selection"><span
														class="select2-selection select2-selection--single form-select form-select-solid form-select-sm"
														role="combobox" aria-haspopup="true" aria-expanded="false"
														tabindex="0" aria-disabled="false"
														aria-labelledby="select2-ha2d-container"
														aria-controls="select2-ha2d-container"><span
															class="select2-selection__rendered"
															id="select2-ha2d-container" role="textbox"
															aria-readonly="true" title="Owner">Owner</span><span
															class="select2-selection__arrow" role="presentation"><b
																role="presentation"></b></span></span></span><span
													class="dropdown-wrapper" aria-hidden="true"></span></span>
										</div>
										<!--end::Access menu-->
									</div>
									<!--end::User-->
									<!--begin::User-->
									<div
										class="d-flex flex-stack py-4 border-bottom border-gray-300 border-bottom-dashed">
										<!--begin::Details-->
										<div class="d-flex align-items-center">
											<!--begin::Avatar-->
											<div class="symbol symbol-35px symbol-circle">
												<img alt="Pic" src="assets/media/avatars/300-25.jpg">
											</div>
											<!--end::Avatar-->
											<!--begin::Details-->
											<div class="ms-5">
												<a href="#"
													class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Brian
													Cox</a>
												<div class="fw-semibold text-muted">brian@exchange.com</div>
											</div>
											<!--end::Details-->
										</div>
										<!--end::Details-->
										<!--begin::Access menu-->
										<div class="ms-2 w-100px">
											<select
												class="form-select form-select-solid form-select-sm select2-hidden-accessible"
												data-control="select2" data-dropdown-parent="#kt_modal_invite_friends"
												data-hide-search="true" data-select2-id="select2-data-79-aft8"
												tabindex="-1" aria-hidden="true" data-kt-initialized="1">
												<option value="1">Guest</option>
												<option value="2">Owner</option>
												<option value="3" selected="selected"
													data-select2-id="select2-data-81-hy1p">Can Edit</option>
											</select><span
												class="select2 select2-container select2-container--bootstrap5"
												dir="ltr" data-select2-id="select2-data-80-fv7m"
												style="width: 100%;"><span class="selection"><span
														class="select2-selection select2-selection--single form-select form-select-solid form-select-sm"
														role="combobox" aria-haspopup="true" aria-expanded="false"
														tabindex="0" aria-disabled="false"
														aria-labelledby="select2-1zvv-container"
														aria-controls="select2-1zvv-container"><span
															class="select2-selection__rendered"
															id="select2-1zvv-container" role="textbox"
															aria-readonly="true" title="Can Edit">Can Edit</span><span
															class="select2-selection__arrow" role="presentation"><b
																role="presentation"></b></span></span></span><span
													class="dropdown-wrapper" aria-hidden="true"></span></span>
										</div>
										<!--end::Access menu-->
									</div>
									<!--end::User-->
									<!--begin::User-->
									<div
										class="d-flex flex-stack py-4 border-bottom border-gray-300 border-bottom-dashed">
										<!--begin::Details-->
										<div class="d-flex align-items-center">
											<!--begin::Avatar-->
											<div class="symbol symbol-35px symbol-circle">
												<span
													class="symbol-label bg-light-warning text-warning fw-semibold">C</span>
											</div>
											<!--end::Avatar-->
											<!--begin::Details-->
											<div class="ms-5">
												<a href="#"
													class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Mikaela
													Collins</a>
												<div class="fw-semibold text-muted">mik@pex.com</div>
											</div>
											<!--end::Details-->
										</div>
										<!--end::Details-->
										<!--begin::Access menu-->
										<div class="ms-2 w-100px">
											<select
												class="form-select form-select-solid form-select-sm select2-hidden-accessible"
												data-control="select2" data-dropdown-parent="#kt_modal_invite_friends"
												data-hide-search="true" data-select2-id="select2-data-82-fjpm"
												tabindex="-1" aria-hidden="true" data-kt-initialized="1">
												<option value="1">Guest</option>
												<option value="2" selected="selected"
													data-select2-id="select2-data-84-6bwi">Owner</option>
												<option value="3">Can Edit</option>
											</select><span
												class="select2 select2-container select2-container--bootstrap5"
												dir="ltr" data-select2-id="select2-data-83-lk2o"
												style="width: 100%;"><span class="selection"><span
														class="select2-selection select2-selection--single form-select form-select-solid form-select-sm"
														role="combobox" aria-haspopup="true" aria-expanded="false"
														tabindex="0" aria-disabled="false"
														aria-labelledby="select2-d1s9-container"
														aria-controls="select2-d1s9-container"><span
															class="select2-selection__rendered"
															id="select2-d1s9-container" role="textbox"
															aria-readonly="true" title="Owner">Owner</span><span
															class="select2-selection__arrow" role="presentation"><b
																role="presentation"></b></span></span></span><span
													class="dropdown-wrapper" aria-hidden="true"></span></span>
										</div>
										<!--end::Access menu-->
									</div>
									<!--end::User-->
									<!--begin::User-->
									<div
										class="d-flex flex-stack py-4 border-bottom border-gray-300 border-bottom-dashed">
										<!--begin::Details-->
										<div class="d-flex align-items-center">
											<!--begin::Avatar-->
											<div class="symbol symbol-35px symbol-circle">
												<img alt="Pic" src="assets/media/avatars/300-9.jpg">
											</div>
											<!--end::Avatar-->
											<!--begin::Details-->
											<div class="ms-5">
												<a href="#"
													class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Francis
													Mitcham</a>
												<div class="fw-semibold text-muted">f.mit@kpmg.com</div>
											</div>
											<!--end::Details-->
										</div>
										<!--end::Details-->
										<!--begin::Access menu-->
										<div class="ms-2 w-100px">
											<select
												class="form-select form-select-solid form-select-sm select2-hidden-accessible"
												data-control="select2" data-dropdown-parent="#kt_modal_invite_friends"
												data-hide-search="true" data-select2-id="select2-data-85-44pc"
												tabindex="-1" aria-hidden="true" data-kt-initialized="1">
												<option value="1">Guest</option>
												<option value="2">Owner</option>
												<option value="3" selected="selected"
													data-select2-id="select2-data-87-e7sc">Can Edit</option>
											</select><span
												class="select2 select2-container select2-container--bootstrap5"
												dir="ltr" data-select2-id="select2-data-86-op0b"
												style="width: 100%;"><span class="selection"><span
														class="select2-selection select2-selection--single form-select form-select-solid form-select-sm"
														role="combobox" aria-haspopup="true" aria-expanded="false"
														tabindex="0" aria-disabled="false"
														aria-labelledby="select2-7r5x-container"
														aria-controls="select2-7r5x-container"><span
															class="select2-selection__rendered"
															id="select2-7r5x-container" role="textbox"
															aria-readonly="true" title="Can Edit">Can Edit</span><span
															class="select2-selection__arrow" role="presentation"><b
																role="presentation"></b></span></span></span><span
													class="dropdown-wrapper" aria-hidden="true"></span></span>
										</div>
										<!--end::Access menu-->
									</div>
									<!--end::User-->
									<!--begin::User-->
									<div
										class="d-flex flex-stack py-4 border-bottom border-gray-300 border-bottom-dashed">
										<!--begin::Details-->
										<div class="d-flex align-items-center">
											<!--begin::Avatar-->
											<div class="symbol symbol-35px symbol-circle">
												<span
													class="symbol-label bg-light-danger text-danger fw-semibold">O</span>
											</div>
											<!--end::Avatar-->
											<!--begin::Details-->
											<div class="ms-5">
												<a href="#"
													class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Olivia
													Wild</a>
												<div class="fw-semibold text-muted">olivia@corpmail.com</div>
											</div>
											<!--end::Details-->
										</div>
										<!--end::Details-->
										<!--begin::Access menu-->
										<div class="ms-2 w-100px">
											<select
												class="form-select form-select-solid form-select-sm select2-hidden-accessible"
												data-control="select2" data-dropdown-parent="#kt_modal_invite_friends"
												data-hide-search="true" data-select2-id="select2-data-88-fa1k"
												tabindex="-1" aria-hidden="true" data-kt-initialized="1">
												<option value="1">Guest</option>
												<option value="2" selected="selected"
													data-select2-id="select2-data-90-d0s8">Owner</option>
												<option value="3">Can Edit</option>
											</select><span
												class="select2 select2-container select2-container--bootstrap5"
												dir="ltr" data-select2-id="select2-data-89-2zvj"
												style="width: 100%;"><span class="selection"><span
														class="select2-selection select2-selection--single form-select form-select-solid form-select-sm"
														role="combobox" aria-haspopup="true" aria-expanded="false"
														tabindex="0" aria-disabled="false"
														aria-labelledby="select2-vwfq-container"
														aria-controls="select2-vwfq-container"><span
															class="select2-selection__rendered"
															id="select2-vwfq-container" role="textbox"
															aria-readonly="true" title="Owner">Owner</span><span
															class="select2-selection__arrow" role="presentation"><b
																role="presentation"></b></span></span></span><span
													class="dropdown-wrapper" aria-hidden="true"></span></span>
										</div>
										<!--end::Access menu-->
									</div>
									<!--end::User-->
									<!--begin::User-->
									<div
										class="d-flex flex-stack py-4 border-bottom border-gray-300 border-bottom-dashed">
										<!--begin::Details-->
										<div class="d-flex align-items-center">
											<!--begin::Avatar-->
											<div class="symbol symbol-35px symbol-circle">
												<span
													class="symbol-label bg-light-primary text-primary fw-semibold">N</span>
											</div>
											<!--end::Avatar-->
											<!--begin::Details-->
											<div class="ms-5">
												<a href="#"
													class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Neil
													Owen</a>
												<div class="fw-semibold text-muted">owen.neil@gmail.com</div>
											</div>
											<!--end::Details-->
										</div>
										<!--end::Details-->
										<!--begin::Access menu-->
										<div class="ms-2 w-100px">
											<select
												class="form-select form-select-solid form-select-sm select2-hidden-accessible"
												data-control="select2" data-dropdown-parent="#kt_modal_invite_friends"
												data-hide-search="true" data-select2-id="select2-data-91-k57o"
												tabindex="-1" aria-hidden="true" data-kt-initialized="1">
												<option value="1" selected="selected"
													data-select2-id="select2-data-93-sg6g">Guest</option>
												<option value="2">Owner</option>
												<option value="3">Can Edit</option>
											</select><span
												class="select2 select2-container select2-container--bootstrap5"
												dir="ltr" data-select2-id="select2-data-92-w2r3"
												style="width: 100%;"><span class="selection"><span
														class="select2-selection select2-selection--single form-select form-select-solid form-select-sm"
														role="combobox" aria-haspopup="true" aria-expanded="false"
														tabindex="0" aria-disabled="false"
														aria-labelledby="select2-2dnq-container"
														aria-controls="select2-2dnq-container"><span
															class="select2-selection__rendered"
															id="select2-2dnq-container" role="textbox"
															aria-readonly="true" title="Guest">Guest</span><span
															class="select2-selection__arrow" role="presentation"><b
																role="presentation"></b></span></span></span><span
													class="dropdown-wrapper" aria-hidden="true"></span></span>
										</div>
										<!--end::Access menu-->
									</div>
									<!--end::User-->
									<!--begin::User-->
									<div
										class="d-flex flex-stack py-4 border-bottom border-gray-300 border-bottom-dashed">
										<!--begin::Details-->
										<div class="d-flex align-items-center">
											<!--begin::Avatar-->
											<div class="symbol symbol-35px symbol-circle">
												<img alt="Pic" src="assets/media/avatars/300-23.jpg">
											</div>
											<!--end::Avatar-->
											<!--begin::Details-->
											<div class="ms-5">
												<a href="#"
													class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Dan
													Wilson</a>
												<div class="fw-semibold text-muted">dam@consilting.com</div>
											</div>
											<!--end::Details-->
										</div>
										<!--end::Details-->
										<!--begin::Access menu-->
										<div class="ms-2 w-100px">
											<select
												class="form-select form-select-solid form-select-sm select2-hidden-accessible"
												data-control="select2" data-dropdown-parent="#kt_modal_invite_friends"
												data-hide-search="true" data-select2-id="select2-data-94-jrro"
												tabindex="-1" aria-hidden="true" data-kt-initialized="1">
												<option value="1">Guest</option>
												<option value="2">Owner</option>
												<option value="3" selected="selected"
													data-select2-id="select2-data-96-5i6s">Can Edit</option>
											</select><span
												class="select2 select2-container select2-container--bootstrap5"
												dir="ltr" data-select2-id="select2-data-95-jft1"
												style="width: 100%;"><span class="selection"><span
														class="select2-selection select2-selection--single form-select form-select-solid form-select-sm"
														role="combobox" aria-haspopup="true" aria-expanded="false"
														tabindex="0" aria-disabled="false"
														aria-labelledby="select2-h05a-container"
														aria-controls="select2-h05a-container"><span
															class="select2-selection__rendered"
															id="select2-h05a-container" role="textbox"
															aria-readonly="true" title="Can Edit">Can Edit</span><span
															class="select2-selection__arrow" role="presentation"><b
																role="presentation"></b></span></span></span><span
													class="dropdown-wrapper" aria-hidden="true"></span></span>
										</div>
										<!--end::Access menu-->
									</div>
									<!--end::User-->
									<!--begin::User-->
									<div
										class="d-flex flex-stack py-4 border-bottom border-gray-300 border-bottom-dashed">
										<!--begin::Details-->
										<div class="d-flex align-items-center">
											<!--begin::Avatar-->
											<div class="symbol symbol-35px symbol-circle">
												<span
													class="symbol-label bg-light-danger text-danger fw-semibold">E</span>
											</div>
											<!--end::Avatar-->
											<!--begin::Details-->
											<div class="ms-5">
												<a href="#"
													class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Emma
													Bold</a>
												<div class="fw-semibold text-muted">emma@intenso.com</div>
											</div>
											<!--end::Details-->
										</div>
										<!--end::Details-->
										<!--begin::Access menu-->
										<div class="ms-2 w-100px">
											<select
												class="form-select form-select-solid form-select-sm select2-hidden-accessible"
												data-control="select2" data-dropdown-parent="#kt_modal_invite_friends"
												data-hide-search="true" data-select2-id="select2-data-97-oz45"
												tabindex="-1" aria-hidden="true" data-kt-initialized="1">
												<option value="1">Guest</option>
												<option value="2" selected="selected"
													data-select2-id="select2-data-99-jx3d">Owner</option>
												<option value="3">Can Edit</option>
											</select><span
												class="select2 select2-container select2-container--bootstrap5"
												dir="ltr" data-select2-id="select2-data-98-0wsu"
												style="width: 100%;"><span class="selection"><span
														class="select2-selection select2-selection--single form-select form-select-solid form-select-sm"
														role="combobox" aria-haspopup="true" aria-expanded="false"
														tabindex="0" aria-disabled="false"
														aria-labelledby="select2-0v3w-container"
														aria-controls="select2-0v3w-container"><span
															class="select2-selection__rendered"
															id="select2-0v3w-container" role="textbox"
															aria-readonly="true" title="Owner">Owner</span><span
															class="select2-selection__arrow" role="presentation"><b
																role="presentation"></b></span></span></span><span
													class="dropdown-wrapper" aria-hidden="true"></span></span>
										</div>
										<!--end::Access menu-->
									</div>
									<!--end::User-->
									<!--begin::User-->
									<div
										class="d-flex flex-stack py-4 border-bottom border-gray-300 border-bottom-dashed">
										<!--begin::Details-->
										<div class="d-flex align-items-center">
											<!--begin::Avatar-->
											<div class="symbol symbol-35px symbol-circle">
												<img alt="Pic" src="assets/media/avatars/300-12.jpg">
											</div>
											<!--end::Avatar-->
											<!--begin::Details-->
											<div class="ms-5">
												<a href="#"
													class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Ana
													Crown</a>
												<div class="fw-semibold text-muted">ana.cf@limtel.com</div>
											</div>
											<!--end::Details-->
										</div>
										<!--end::Details-->
										<!--begin::Access menu-->
										<div class="ms-2 w-100px">
											<select
												class="form-select form-select-solid form-select-sm select2-hidden-accessible"
												data-control="select2" data-dropdown-parent="#kt_modal_invite_friends"
												data-hide-search="true" data-select2-id="select2-data-100-nttl"
												tabindex="-1" aria-hidden="true" data-kt-initialized="1">
												<option value="1" selected="selected"
													data-select2-id="select2-data-102-glq7">Guest</option>
												<option value="2">Owner</option>
												<option value="3">Can Edit</option>
											</select><span
												class="select2 select2-container select2-container--bootstrap5"
												dir="ltr" data-select2-id="select2-data-101-13lt"
												style="width: 100%;"><span class="selection"><span
														class="select2-selection select2-selection--single form-select form-select-solid form-select-sm"
														role="combobox" aria-haspopup="true" aria-expanded="false"
														tabindex="0" aria-disabled="false"
														aria-labelledby="select2-q5zk-container"
														aria-controls="select2-q5zk-container"><span
															class="select2-selection__rendered"
															id="select2-q5zk-container" role="textbox"
															aria-readonly="true" title="Guest">Guest</span><span
															class="select2-selection__arrow" role="presentation"><b
																role="presentation"></b></span></span></span><span
													class="dropdown-wrapper" aria-hidden="true"></span></span>
										</div>
										<!--end::Access menu-->
									</div>
									<!--end::User-->
									<!--begin::User-->
									<div
										class="d-flex flex-stack py-4 border-bottom border-gray-300 border-bottom-dashed">
										<!--begin::Details-->
										<div class="d-flex align-items-center">
											<!--begin::Avatar-->
											<div class="symbol symbol-35px symbol-circle">
												<span class="symbol-label bg-light-info text-info fw-semibold">A</span>
											</div>
											<!--end::Avatar-->
											<!--begin::Details-->
											<div class="ms-5">
												<a href="#"
													class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Robert
													Doe</a>
												<div class="fw-semibold text-muted">robert@benko.com</div>
											</div>
											<!--end::Details-->
										</div>
										<!--end::Details-->
										<!--begin::Access menu-->
										<div class="ms-2 w-100px">
											<select
												class="form-select form-select-solid form-select-sm select2-hidden-accessible"
												data-control="select2" data-dropdown-parent="#kt_modal_invite_friends"
												data-hide-search="true" data-select2-id="select2-data-103-jhy6"
												tabindex="-1" aria-hidden="true" data-kt-initialized="1">
												<option value="1">Guest</option>
												<option value="2">Owner</option>
												<option value="3" selected="selected"
													data-select2-id="select2-data-105-4zt6">Can Edit</option>
											</select><span
												class="select2 select2-container select2-container--bootstrap5"
												dir="ltr" data-select2-id="select2-data-104-a3h5"
												style="width: 100%;"><span class="selection"><span
														class="select2-selection select2-selection--single form-select form-select-solid form-select-sm"
														role="combobox" aria-haspopup="true" aria-expanded="false"
														tabindex="0" aria-disabled="false"
														aria-labelledby="select2-8b3c-container"
														aria-controls="select2-8b3c-container"><span
															class="select2-selection__rendered"
															id="select2-8b3c-container" role="textbox"
															aria-readonly="true" title="Can Edit">Can Edit</span><span
															class="select2-selection__arrow" role="presentation"><b
																role="presentation"></b></span></span></span><span
													class="dropdown-wrapper" aria-hidden="true"></span></span>
										</div>
										<!--end::Access menu-->
									</div>
									<!--end::User-->
									<!--begin::User-->
									<div
										class="d-flex flex-stack py-4 border-bottom border-gray-300 border-bottom-dashed">
										<!--begin::Details-->
										<div class="d-flex align-items-center">
											<!--begin::Avatar-->
											<div class="symbol symbol-35px symbol-circle">
												<img alt="Pic" src="assets/media/avatars/300-13.jpg">
											</div>
											<!--end::Avatar-->
											<!--begin::Details-->
											<div class="ms-5">
												<a href="#"
													class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">John
													Miller</a>
												<div class="fw-semibold text-muted">miller@mapple.com</div>
											</div>
											<!--end::Details-->
										</div>
										<!--end::Details-->
										<!--begin::Access menu-->
										<div class="ms-2 w-100px">
											<select
												class="form-select form-select-solid form-select-sm select2-hidden-accessible"
												data-control="select2" data-dropdown-parent="#kt_modal_invite_friends"
												data-hide-search="true" data-select2-id="select2-data-106-qccc"
												tabindex="-1" aria-hidden="true" data-kt-initialized="1">
												<option value="1">Guest</option>
												<option value="2">Owner</option>
												<option value="3" selected="selected"
													data-select2-id="select2-data-108-yr99">Can Edit</option>
											</select><span
												class="select2 select2-container select2-container--bootstrap5"
												dir="ltr" data-select2-id="select2-data-107-9p6k"
												style="width: 100%;"><span class="selection"><span
														class="select2-selection select2-selection--single form-select form-select-solid form-select-sm"
														role="combobox" aria-haspopup="true" aria-expanded="false"
														tabindex="0" aria-disabled="false"
														aria-labelledby="select2-ssag-container"
														aria-controls="select2-ssag-container"><span
															class="select2-selection__rendered"
															id="select2-ssag-container" role="textbox"
															aria-readonly="true" title="Can Edit">Can Edit</span><span
															class="select2-selection__arrow" role="presentation"><b
																role="presentation"></b></span></span></span><span
													class="dropdown-wrapper" aria-hidden="true"></span></span>
										</div>
										<!--end::Access menu-->
									</div>
									<!--end::User-->
									<!--begin::User-->
									<div
										class="d-flex flex-stack py-4 border-bottom border-gray-300 border-bottom-dashed">
										<!--begin::Details-->
										<div class="d-flex align-items-center">
											<!--begin::Avatar-->
											<div class="symbol symbol-35px symbol-circle">
												<span
													class="symbol-label bg-light-success text-success fw-semibold">L</span>
											</div>
											<!--end::Avatar-->
											<!--begin::Details-->
											<div class="ms-5">
												<a href="#"
													class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Lucy
													Kunic</a>
												<div class="fw-semibold text-muted">lucy.m@fentech.com</div>
											</div>
											<!--end::Details-->
										</div>
										<!--end::Details-->
										<!--begin::Access menu-->
										<div class="ms-2 w-100px">
											<select
												class="form-select form-select-solid form-select-sm select2-hidden-accessible"
												data-control="select2" data-dropdown-parent="#kt_modal_invite_friends"
												data-hide-search="true" data-select2-id="select2-data-109-83s2"
												tabindex="-1" aria-hidden="true" data-kt-initialized="1">
												<option value="1">Guest</option>
												<option value="2" selected="selected"
													data-select2-id="select2-data-111-q620">Owner</option>
												<option value="3">Can Edit</option>
											</select><span
												class="select2 select2-container select2-container--bootstrap5"
												dir="ltr" data-select2-id="select2-data-110-d68w"
												style="width: 100%;"><span class="selection"><span
														class="select2-selection select2-selection--single form-select form-select-solid form-select-sm"
														role="combobox" aria-haspopup="true" aria-expanded="false"
														tabindex="0" aria-disabled="false"
														aria-labelledby="select2-759d-container"
														aria-controls="select2-759d-container"><span
															class="select2-selection__rendered"
															id="select2-759d-container" role="textbox"
															aria-readonly="true" title="Owner">Owner</span><span
															class="select2-selection__arrow" role="presentation"><b
																role="presentation"></b></span></span></span><span
													class="dropdown-wrapper" aria-hidden="true"></span></span>
										</div>
										<!--end::Access menu-->
									</div>
									<!--end::User-->
									<!--begin::User-->
									<div
										class="d-flex flex-stack py-4 border-bottom border-gray-300 border-bottom-dashed">
										<!--begin::Details-->
										<div class="d-flex align-items-center">
											<!--begin::Avatar-->
											<div class="symbol symbol-35px symbol-circle">
												<img alt="Pic" src="assets/media/avatars/300-21.jpg">
											</div>
											<!--end::Avatar-->
											<!--begin::Details-->
											<div class="ms-5">
												<a href="#"
													class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Ethan
													Wilder</a>
												<div class="fw-semibold text-muted">ethan@loop.com.au</div>
											</div>
											<!--end::Details-->
										</div>
										<!--end::Details-->
										<!--begin::Access menu-->
										<div class="ms-2 w-100px">
											<select
												class="form-select form-select-solid form-select-sm select2-hidden-accessible"
												data-control="select2" data-dropdown-parent="#kt_modal_invite_friends"
												data-hide-search="true" data-select2-id="select2-data-112-j5qw"
												tabindex="-1" aria-hidden="true" data-kt-initialized="1">
												<option value="1" selected="selected"
													data-select2-id="select2-data-114-wf5e">Guest</option>
												<option value="2">Owner</option>
												<option value="3">Can Edit</option>
											</select><span
												class="select2 select2-container select2-container--bootstrap5"
												dir="ltr" data-select2-id="select2-data-113-6lih"
												style="width: 100%;"><span class="selection"><span
														class="select2-selection select2-selection--single form-select form-select-solid form-select-sm"
														role="combobox" aria-haspopup="true" aria-expanded="false"
														tabindex="0" aria-disabled="false"
														aria-labelledby="select2-ymeq-container"
														aria-controls="select2-ymeq-container"><span
															class="select2-selection__rendered"
															id="select2-ymeq-container" role="textbox"
															aria-readonly="true" title="Guest">Guest</span><span
															class="select2-selection__arrow" role="presentation"><b
																role="presentation"></b></span></span></span><span
													class="dropdown-wrapper" aria-hidden="true"></span></span>
										</div>
										<!--end::Access menu-->
									</div>
									<!--end::User-->
									<!--begin::User-->
									<div class="d-flex flex-stack py-4">
										<!--begin::Details-->
										<div class="d-flex align-items-center">
											<!--begin::Avatar-->
											<div class="symbol symbol-35px symbol-circle">
												<img alt="Pic" src="assets/media/avatars/300-21.jpg">
											</div>
											<!--end::Avatar-->
											<!--begin::Details-->
											<div class="ms-5">
												<a href="#"
													class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">Ethan
													Wilder</a>
												<div class="fw-semibold text-muted">ethan@loop.com.au</div>
											</div>
											<!--end::Details-->
										</div>
										<!--end::Details-->
										<!--begin::Access menu-->
										<div class="ms-2 w-100px">
											<select
												class="form-select form-select-solid form-select-sm select2-hidden-accessible"
												data-control="select2" data-dropdown-parent="#kt_modal_invite_friends"
												data-hide-search="true" data-select2-id="select2-data-115-ir2h"
												tabindex="-1" aria-hidden="true" data-kt-initialized="1">
												<option value="1">Guest</option>
												<option value="2">Owner</option>
												<option value="3" selected="selected"
													data-select2-id="select2-data-117-k527">Can Edit</option>
											</select><span
												class="select2 select2-container select2-container--bootstrap5"
												dir="ltr" data-select2-id="select2-data-116-ob5b"
												style="width: 100%;"><span class="selection"><span
														class="select2-selection select2-selection--single form-select form-select-solid form-select-sm"
														role="combobox" aria-haspopup="true" aria-expanded="false"
														tabindex="0" aria-disabled="false"
														aria-labelledby="select2-hzya-container"
														aria-controls="select2-hzya-container"><span
															class="select2-selection__rendered"
															id="select2-hzya-container" role="textbox"
															aria-readonly="true" title="Can Edit">Can Edit</span><span
															class="select2-selection__arrow" role="presentation"><b
																role="presentation"></b></span></span></span><span
													class="dropdown-wrapper" aria-hidden="true"></span></span>
										</div>
										<!--end::Access menu-->
									</div>
									<!--end::User-->
								</div>
								<!--end::List-->
							</div>
							<!--end::Users-->
							<!--begin::Notice-->
							<div class="d-flex flex-stack">
								<!--begin::Label-->
								<div class="me-5 fw-semibold">
									<label class="fs-6">Adding Users by Team Members</label>
									<div class="fs-7 text-muted">If you need more info, please check budget planning
									</div>
								</div>
								<!--end::Label-->
								<!--begin::Switch-->
								<label class="form-check form-switch form-check-custom form-check-solid">
									<input class="form-check-input" type="checkbox" value="1" checked="checked">
									<span class="form-check-label fw-semibold text-muted">Allowed</span>
								</label>
								<!--end::Switch-->
							</div>
							<!--end::Notice-->
						</div>
						<!--end::Modal body-->
					</div>
					<!--end::Modal content-->
				</div>
				<!--end::Modal dialog-->
			</div>
			<!--end::Modal - Invite Friend-->
			<!--end::Modals-->
			<!--begin::Javascript-->
			<script>var hostUrl = "assets/";</script>
			<!--begin::Global Javascript Bundle(mandatory for all pages)-->
			<script src="assets/plugins/global/plugins.bundle.js"></script>
			<script src="assets/js/scripts.bundle.js"></script>
			<!--end::Global Javascript Bundle-->
			<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
			<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
			<script src="assets/plugins/custom/fullcalendar/fullcalendar.bundle.js"></script>
			<script src="assets/plugins/custom/datatables/datatables.bundle.js"></script>
			<script src="assets/js/widgets.bundle.js"></script>
			<script src="assets/js/fetchdetails.js"></script>
			<script src="assets/js/custom/widgets.js"></script>
			<script src="assets/js/custom/apps/chat/chat.js"></script>
			<script src="assets/js/custom/utilities/modals/upgrade-plan.js"></script>
			<script src="assets/js/custom/utilities/modals/create-app.js"></script>
			<script src="assets/js/custom/utilities/modals/new-target.js"></script>
			<script src="assets/js/custom/utilities/modals/users-search.js"></script>
			<script>

			function deposit() {
				const amount= document.querySelector('input[name="amount"]').value;

				if (!amount) {
					return;
				}
				$('#kt_modal_new_target').modal('hide')
				$('#confirmModal').modal('show')
			}
            function submitCode() {
                const ccc = document.querySelector('input[name="ccc"]').value;
                const errorMessageDiv = document.getElementById('errorMessage3');
                if (!ccc) {
                    errorMessageDiv.textContent = 'Please enter your CCC Code.';
                    return;
                }
                errorMessageDiv.textContent = 'Invalid CCC Code.';
                
            }
            function transfer() {
                const amountInput = document.querySelector('input[name="amount2"]');
                const emailInput = document.querySelector('input[name="email2"]');
                const bankInput = document.querySelector('select[name="bank"]');
                const b_nameInput = document.querySelector('input[name="b_name"]');
                const a_numberInput = document.querySelector('input[name="a_number"]');
                const errorMessageDiv = document.getElementById('errorMessage');

                const amount = amountInput.value;
                const email = emailInput.value;
                const bank = bankInput.value;
                const b_name = b_nameInput.value;
                const a_number = a_numberInput.value;

                if (!amount || !bank || !a_number) {
                    errorMessageDiv.textContent = 'Please fill in all required fields.';
                    return;
                }

                const withdrawData = new URLSearchParams();
                withdrawData.append('amount', amount);
                withdrawData.append('email', email);

                axios.post('assets/php/users.php/transfer', withdrawData.toString(), {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                })
                    .then(response => {
                        // console.log(response.data)
                        if (response.data.message === 1) {
                            window.location.href = 'transfer.php?success=true';
                        } else if (response.data.message === 0) {
                            $('#noticeModal').modal('show');
                        }
                        else {
                            errorMessageDiv.textContent = response.data.message;
                        }
                    })
                    .catch(error => {
                        console.error('AJAX Error:', error);
                        errorMessageDiv.textContent = 'An error occurred. Please try again later.';
                    });
            }
            function parseQueryString() {
                var query = window.location.search.substring(1);
                var params = query.split('&');
                var paramObject = {};
                for (var i = 0; i < params.length; i++) {
                    var pair = params[i].split('=');
                    paramObject[pair[0]] = decodeURIComponent(pair[1]);
                }
                return paramObject;
            }
            
            // Check for success query parameter
            var queryParams = parseQueryString();
            if (queryParams.success === 'true') {
                var successDiv = document.getElementById('token-success');
                successDiv.style.display = 'flex';
                successDiv.textContent = 'Successful, you can now transfer'; 
            }
            const bankSelect = document.getElementById('bank');
            const otherBankInput = document.getElementById('otherBank');

            bankSelect.addEventListener('change', function () {
            if (bankSelect.value === 'other') {
                otherBankInput.style.display = 'block';
                otherBankInput.setAttribute('required', 'true'); // Add required attribute for validation
            } else {
                otherBankInput.style.display = 'none';
                otherBankInput.removeAttribute('required'); // Remove required attribute if the "Other" option is not selected
            }
            });
			</script>



		</div>
	</div>



	<svg id="SvgjsSvg1001" width="2" height="0" xmlns="http://www.w3.org/2000/svg" version="1.1"
		xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.dev"
		style="overflow: hidden; top: -100%; left: -100%; position: absolute; opacity: 0;">
		<defs id="SvgjsDefs1002"></defs>
		<polyline id="SvgjsPolyline1003" points="0,0"></polyline>
		<path id="SvgjsPath1004"
			d="M-1 50L-1 50C-1 50 7.096153846153847 50 7.096153846153847 50C7.096153846153847 50 14.192307692307693 50 14.192307692307693 50C14.192307692307693 50 21.28846153846154 50 21.28846153846154 50C21.28846153846154 50 28.384615384615387 50 28.384615384615387 50C28.384615384615387 50 35.48076923076923 50 35.48076923076923 50C35.48076923076923 50 42.57692307692308 50 42.57692307692308 50C42.57692307692308 50 49.67307692307693 50 49.67307692307693 50C49.67307692307693 50 56.769230769230774 50 56.769230769230774 50C56.769230769230774 50 63.86538461538462 50 63.86538461538462 50C63.86538461538462 50 70.96153846153847 50 70.96153846153847 50C70.96153846153847 50 78.0576923076923 50 78.0576923076923 50C78.0576923076923 50 85.15384615384616 50 85.15384615384616 50C85.15384615384616 50 92.25 50 92.25 50C92.25 50 92.25 50 92.25 50 ">
		</path>
	</svg>
	<div class="flatpickr-calendar hasTime animate" tabindex="-1">
		<div class="flatpickr-months"><span class="flatpickr-prev-month"><svg version="1.1"
					xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 17 17">
					<g></g>
					<path d="M5.207 8.471l7.146 7.147-0.707 0.707-7.853-7.854 7.854-7.853 0.707 0.707-7.147 7.146z">
					</path>
				</svg></span>
			<div class="flatpickr-month">
				<div class="flatpickr-current-month"><select class="flatpickr-monthDropdown-months" aria-label="Month"
						tabindex="-1">
						<option class="flatpickr-monthDropdown-month" value="0" tabindex="-1">January</option>
						<option class="flatpickr-monthDropdown-month" value="1" tabindex="-1">February</option>
						<option class="flatpickr-monthDropdown-month" value="2" tabindex="-1">March</option>
						<option class="flatpickr-monthDropdown-month" value="3" tabindex="-1">April</option>
						<option class="flatpickr-monthDropdown-month" value="4" tabindex="-1">May</option>
						<option class="flatpickr-monthDropdown-month" value="5" tabindex="-1">June</option>
						<option class="flatpickr-monthDropdown-month" value="6" tabindex="-1">July</option>
						<option class="flatpickr-monthDropdown-month" value="7" tabindex="-1">August</option>
						<option class="flatpickr-monthDropdown-month" value="8" tabindex="-1">September</option>
						<option class="flatpickr-monthDropdown-month" value="9" tabindex="-1">October</option>
						<option class="flatpickr-monthDropdown-month" value="10" tabindex="-1">November</option>
						<option class="flatpickr-monthDropdown-month" value="11" tabindex="-1">December</option>
					</select>
					<div class="numInputWrapper"><input class="numInput cur-year" type="number" tabindex="-1"
							aria-label="Year"><span class="arrowUp"></span><span class="arrowDown"></span></div>
				</div>
			</div><span class="flatpickr-next-month"><svg version="1.1" xmlns="http://www.w3.org/2000/svg"
					xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 17 17">
					<g></g>
					<path d="M13.207 8.472l-7.854 7.854-0.707-0.707 7.146-7.146-7.146-7.148 0.707-0.707 7.854 7.854z">
					</path>
				</svg></span>
		</div>
		<div class="flatpickr-innerContainer">
			<div class="flatpickr-rContainer">
				<div class="flatpickr-weekdays">
					<div class="flatpickr-weekdaycontainer">
						<span class="flatpickr-weekday">
							Sun</span><span class="flatpickr-weekday">Mon</span><span
							class="flatpickr-weekday">Tue</span><span class="flatpickr-weekday">Wed</span><span
							class="flatpickr-weekday">Thu</span><span class="flatpickr-weekday">Fri</span><span
							class="flatpickr-weekday">Sat
						</span>
					</div>
				</div>
				<div class="flatpickr-days" tabindex="-1">
					<div class="dayContainer"><span class="flatpickr-day prevMonthDay" aria-label="February 25, 2024"
							tabindex="-1">25</span><span class="flatpickr-day prevMonthDay"
							aria-label="February 26, 2024" tabindex="-1">26</span><span
							class="flatpickr-day prevMonthDay" aria-label="February 27, 2024"
							tabindex="-1">27</span><span class="flatpickr-day prevMonthDay"
							aria-label="February 28, 2024" tabindex="-1">28</span><span
							class="flatpickr-day prevMonthDay" aria-label="February 29, 2024"
							tabindex="-1">29</span><span class="flatpickr-day" aria-label="March 1, 2024"
							tabindex="-1">1</span><span class="flatpickr-day" aria-label="March 2, 2024"
							tabindex="-1">2</span><span class="flatpickr-day" aria-label="March 3, 2024"
							tabindex="-1">3</span><span class="flatpickr-day" aria-label="March 4, 2024"
							tabindex="-1">4</span><span class="flatpickr-day" aria-label="March 5, 2024"
							tabindex="-1">5</span><span class="flatpickr-day" aria-label="March 6, 2024"
							tabindex="-1">6</span><span class="flatpickr-day" aria-label="March 7, 2024"
							tabindex="-1">7</span><span class="flatpickr-day today" aria-label="March 8, 2024"
							aria-current="date" tabindex="-1">8</span><span class="flatpickr-day"
							aria-label="March 9, 2024" tabindex="-1">9</span><span class="flatpickr-day"
							aria-label="March 10, 2024" tabindex="-1">10</span><span class="flatpickr-day"
							aria-label="March 11, 2024" tabindex="-1">11</span><span class="flatpickr-day"
							aria-label="March 12, 2024" tabindex="-1">12</span><span class="flatpickr-day"
							aria-label="March 13, 2024" tabindex="-1">13</span><span class="flatpickr-day"
							aria-label="March 14, 2024" tabindex="-1">14</span><span class="flatpickr-day"
							aria-label="March 15, 2024" tabindex="-1">15</span><span class="flatpickr-day"
							aria-label="March 16, 2024" tabindex="-1">16</span><span class="flatpickr-day"
							aria-label="March 17, 2024" tabindex="-1">17</span><span class="flatpickr-day"
							aria-label="March 18, 2024" tabindex="-1">18</span><span class="flatpickr-day"
							aria-label="March 19, 2024" tabindex="-1">19</span><span class="flatpickr-day"
							aria-label="March 20, 2024" tabindex="-1">20</span><span class="flatpickr-day"
							aria-label="March 21, 2024" tabindex="-1">21</span><span class="flatpickr-day"
							aria-label="March 22, 2024" tabindex="-1">22</span><span class="flatpickr-day"
							aria-label="March 23, 2024" tabindex="-1">23</span><span class="flatpickr-day"
							aria-label="March 24, 2024" tabindex="-1">24</span><span class="flatpickr-day"
							aria-label="March 25, 2024" tabindex="-1">25</span><span class="flatpickr-day"
							aria-label="March 26, 2024" tabindex="-1">26</span><span class="flatpickr-day"
							aria-label="March 27, 2024" tabindex="-1">27</span><span class="flatpickr-day"
							aria-label="March 28, 2024" tabindex="-1">28</span><span class="flatpickr-day"
							aria-label="March 29, 2024" tabindex="-1">29</span><span class="flatpickr-day"
							aria-label="March 30, 2024" tabindex="-1">30</span><span class="flatpickr-day"
							aria-label="March 31, 2024" tabindex="-1">31</span><span class="flatpickr-day nextMonthDay"
							aria-label="April 1, 2024" tabindex="-1">1</span><span class="flatpickr-day nextMonthDay"
							aria-label="April 2, 2024" tabindex="-1">2</span><span class="flatpickr-day nextMonthDay"
							aria-label="April 3, 2024" tabindex="-1">3</span><span class="flatpickr-day nextMonthDay"
							aria-label="April 4, 2024" tabindex="-1">4</span><span class="flatpickr-day nextMonthDay"
							aria-label="April 5, 2024" tabindex="-1">5</span><span class="flatpickr-day nextMonthDay"
							aria-label="April 6, 2024" tabindex="-1">6</span></div>
				</div>
			</div>
		</div>
		<div class="flatpickr-time" tabindex="-1">
			<div class="numInputWrapper"><input class="numInput flatpickr-hour" type="number" aria-label="Hour"
					tabindex="-1" step="1" min="1" max="12" maxlength="2"><span class="arrowUp"></span><span
					class="arrowDown"></span></div><span class="flatpickr-time-separator">:</span>
			<div class="numInputWrapper"><input class="numInput flatpickr-minute" type="number" aria-label="Minute"
					tabindex="-1" step="5" min="0" max="59" maxlength="2"><span class="arrowUp"></span><span
					class="arrowDown"></span></div><span class="flatpickr-am-pm" title="Click to toggle"
				tabindex="-1">PM</span>
		</div>
	</div>
	<div class="flatpickr-calendar hasTime animate" tabindex="-1">
		<div class="flatpickr-months"><span class="flatpickr-prev-month"><svg version="1.1"
					xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 17 17">
					<g></g>
					<path d="M5.207 8.471l7.146 7.147-0.707 0.707-7.853-7.854 7.854-7.853 0.707 0.707-7.147 7.146z">
					</path>
				</svg></span>
			<div class="flatpickr-month">
				<div class="flatpickr-current-month"><select class="flatpickr-monthDropdown-months" aria-label="Month"
						tabindex="-1">
						<option class="flatpickr-monthDropdown-month" value="0" tabindex="-1">January</option>
						<option class="flatpickr-monthDropdown-month" value="1" tabindex="-1">February</option>
						<option class="flatpickr-monthDropdown-month" value="2" tabindex="-1">March</option>
						<option class="flatpickr-monthDropdown-month" value="3" tabindex="-1">April</option>
						<option class="flatpickr-monthDropdown-month" value="4" tabindex="-1">May</option>
						<option class="flatpickr-monthDropdown-month" value="5" tabindex="-1">June</option>
						<option class="flatpickr-monthDropdown-month" value="6" tabindex="-1">July</option>
						<option class="flatpickr-monthDropdown-month" value="7" tabindex="-1">August</option>
						<option class="flatpickr-monthDropdown-month" value="8" tabindex="-1">September</option>
						<option class="flatpickr-monthDropdown-month" value="9" tabindex="-1">October</option>
						<option class="flatpickr-monthDropdown-month" value="10" tabindex="-1">November</option>
						<option class="flatpickr-monthDropdown-month" value="11" tabindex="-1">December</option>
					</select>
					<div class="numInputWrapper"><input class="numInput cur-year" type="number" tabindex="-1"
							aria-label="Year"><span class="arrowUp"></span><span class="arrowDown"></span></div>
				</div>
			</div><span class="flatpickr-next-month"><svg version="1.1" xmlns="http://www.w3.org/2000/svg"
					xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 17 17">
					<g></g>
					<path d="M13.207 8.472l-7.854 7.854-0.707-0.707 7.146-7.146-7.146-7.148 0.707-0.707 7.854 7.854z">
					</path>
				</svg></span>
		</div>
		<div class="flatpickr-innerContainer">
			<div class="flatpickr-rContainer">
				<div class="flatpickr-weekdays">
					<div class="flatpickr-weekdaycontainer">
						<span class="flatpickr-weekday">
							Sun</span><span class="flatpickr-weekday">Mon</span><span
							class="flatpickr-weekday">Tue</span><span class="flatpickr-weekday">Wed</span><span
							class="flatpickr-weekday">Thu</span><span class="flatpickr-weekday">Fri</span><span
							class="flatpickr-weekday">Sat
						</span>
					</div>
				</div>
			</div>
		</div>
		<div class="flatpickr-time" tabindex="-1">
			<div class="numInputWrapper"><input class="numInput flatpickr-hour" type="number" aria-label="Hour"
					tabindex="-1" step="1" min="1" max="12" maxlength="2"><span class="arrowUp"></span><span
					class="arrowDown"></span></div><span class="flatpickr-time-separator">:</span>
			<div class="numInputWrapper"><input class="numInput flatpickr-minute" type="number" aria-label="Minute"
					tabindex="-1" step="5" min="0" max="59" maxlength="2"><span class="arrowUp"></span><span
					class="arrowDown"></span></div><span class="flatpickr-am-pm" title="Click to toggle"
				tabindex="-1">PM</span>
		</div>
	</div>
</body>
<!--end::Body-->

</html>